{% macro format_date(src_date) %}
-- Transfrome le format de date du SNDS : 01/02/2020 (1er février 2020) au format Postgresql 2020-02-01
        (
                SUBSTR({{src_date}}, 7,4) --année
        || '-' || SUBSTR({{src_date}}, 4,2) --mois
        || '-' || SUBSTR({{src_date}}, 1, 2) --jour 
        )::date
    
    
    
{% endmacro %}