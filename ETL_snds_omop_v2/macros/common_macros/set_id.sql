{% macro set_id(columns) %}

-- Cette fonction esr utilisée pour créer un identifiant unique pour chaque enregistrement dans une table, en se basant
-- sur une ou plusieurs colonnes de la table.
-- Ces colonnes sont hashées par la macro dbt_utilis.surrogate_key(), le résultat est casté en BIGINT

('x'||{{dbt_utils.generate_surrogate_key(columns)}})::bit(63)::bigint
--@('x' || encode(sha256(({{column_name}})::bytea), 'hex'))::bit(32)::integer

{% endmacro %}