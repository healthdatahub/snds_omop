{% macro filter_concepts(source_vocabs) %}

-- This macro adds a column corresponding to the domain of target concept. 

-- source_vocab      : name of source vocabulary. Ex : 'NABM'

-- source_code       : code as it is in source data
-- source_concept_id : concept corresponding to source code
-- target_concept_id : concept corresponding to taget code
-- domain_id         : domain corresponding to target concept code


SELECT 
    src.source_code::text AS source_value,
    src.source_concept_id,
    src.target_concept_id,
    
    c.domain_id
    
    FROM 
        {{ source('vocabularies','source_to_concept_map') }} src
        
    JOIN {{ source('vocabularies', 'concept') }} c
        ON concept_id = target_concept_id 
    
    WHERE source_vocabulary_id IN ( {% for sv in source_vocabs %} {{sv}} {% if not loop.last %} , {% endif %} {% endfor %} ) 
    
    
    
{% endmacro %}