{% macro procedure_pmsi_source_value(src_source, src_table, source_value, src_base, src_finess_nb, src_visit_nb, src_sej_idt, phase, delai, quantity=1) %}

-- This macro contains repetitive code that cannot be reduced. It queries the different PMSI tables which will feed the OMOP-CDM PROCEDURE_OCCURENCE table. 
-- This macro calls the visit_source_value macro 
-- src_base is the part of the PMSI (MCO, HAD, SSR, RIP) 
-- src_table is the table where info is stored 
-- quantity is the variable where the number of procedures is stored in the src_table,  default is 1  
-- delai is the number of days that pass during the visit before drug administration, if the columns does not exist, set delai = 0
-- phase is a french CCAM notion : one procedure can be divided into different phases. 
-- src_finess_nb is the variable where the juridical identifier of a care site is stored in the src_table
-- src_visit_nb is the unique identifier of the visit where the condition was found in the src_table 

SELECT
    {{source_value}}               AS source_value,
    {{quantity}}                   AS quantity,
    {{src_base}}                   AS base,
    {{src_finess_nb}}              AS finess_nb,
    {{src_visit_nb}}               AS visit_nb,
    {{src_sej_idt}}                AS sej_idt,
    coalesce({{delai}}::integer,0) AS delai
    
FROM {{ source(src_source, src_table) }}

{% if phase is not none %}
    WHERE {{phase}}::integer IN (0,1) -- If the procedure has multiple phases, keep only the first one (phase = 1) to avoid duplicates. 0 is for procedure with a unique phase 
{% endif %}

{% endmacro %}




{% macro procedure_visit(src_table, exe_soi_dtd=0) %}


SELECT 
    DISTINCT 
    source_value      AS source_value,
    quantity::integer AS quantity,
    delai,
    {{visit_occurrence_source_value('base', 
                                    'finess_nb', 
                                    'visit_nb',
                                    exe_soi_dtd)}} AS visit_occurrence_source_value 
FROM 
    {{src_table}}
    
{% endmacro %}