{% macro clean_table_c( source_name,table_c, base, finess_j, visit_nb ) %}

-- This macro contains repetitive code that cannot be reduces. It queries the different PMSI tables which will feed the OMOP-CDM VISIT_OCCURRENCE table where visit_concept_id is 9201 ( Inpatient) 
-- base is the part of the PMSI (MCO, HAD, SSR, RIP)
-- table_c is the table where info is stored 
-- finess_j is the variable where the juridical identifier is stored in the table_c
-- visit_nb is the unique identifier of the visit in table_c


SELECT
    c.num_enq::varchar(100)          AS num_enq,
    {{ format_date('exe_soi_dtd') }} AS exe_soi_dtd,
    {{ format_date('exe_soi_dtf') }} AS exe_soi_dtf,
    {{ base }}                       AS base,
    c.{{ finess_j }}::varchar(50)    AS finess_j,
    c.{{ visit_nb }}::varchar(50)    AS visit_nb


FROM
    {{ source(source_name, table_c) }} c
    
-- In this macro, we only select the valid visits, that is to say the 'codes retours' set to 0 
WHERE 
    c.nir_ret = '0' 
    AND c.nai_ret = '0' 
    AND c.sex_ret = '0' 
    AND c.sej_ret = '0'
    AND c.fho_ret = '0' 
    AND c.pms_ret = '0' 
    AND c.dat_ret = '0'
    AND c.coh_nai_ret = '0' 
    AND c.coh_sex_ret = '0'
    
    
    
{% endmacro %}