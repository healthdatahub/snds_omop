{% macro visit_pmsi_inpatient( src_table, src_base, src_finess_nb, src_visit_nb ) %}

-- This macro contains repetitive code that cannot be reduces. It queries the different PMSI tables which will feed the OMOP-CDM VISIT_OCCURRENCE table where visit_concept_id is 9201 ( Inpatient) 
-- src_base is the part of the PMSI (MCO, HAD, SSR, RIP)
-- src_table is the table where info is stored 
-- src_finess_nb is the variable where the juridical identifier of a care site is stored in the src_table
-- src_visit_nb is the unique identifier of the visit in src_table


SELECT
    a.num_enq::varchar(50)                               AS num_enq,
    {{ format_date('exe_soi_dtd') }}                     AS exe_soi_dtd,
    {{ format_date('exe_soi_dtf') }}                     AS exe_soi_dtf,
    {{ src_base }}                                       AS src_base,
    a.{{ src_finess_nb }}::varchar(50)                   AS finess_j,
    a.{{ src_visit_nb }}::varchar(50)                    AS visit_nb,
    9201                                                 AS visit_concept_id,
    
    {{ visit_source_value(src_base=src_base, 
                          src_finess_nb=src_finess_nb, 
                          src_visit_nb=src_visit_nb )}}  AS visit_source_value

FROM
    {{ src_table }} a
    
-- In this macro, we only select the valid visits, that is to say the 'codes retours' set to 0 
WHERE 
    a.nir_ret = '0' 
    AND a.nai_ret = '0' 
    AND a.sex_ret = '0' 
    AND a.sej_ret = '0'
    AND a.fho_ret = '0' 
    AND a.pms_ret = '0' 
    AND a.dat_ret = '0'
    AND a.coh_nai_ret = '0' 
    AND a.coh_sex_ret = '0'

{% endmacro %}