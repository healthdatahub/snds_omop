{% macro visit_occurrence_source_value( src_base, src_finess_nb, src_visit_nb, src_start_date=0) %}

-- This macro computes a unique identifier of a visit from the SNDS 
-- src_base is the part of the PMSI (MCO, HAD, SSR, RIP)
-- src_finess_nb is the variable where the juridical identifier of a care site is stored in the src_table
-- src_visit_nb is the unique identifier of the visit in the source table
-- src_start_date is the start date of visit in the source_table

CASE
    WHEN {{ src_base }} = 'ssr' OR SUBSTR({{ src_base }},1,3) = 'rip'
    THEN {{ src_base }} ||'_'|| COALESCE({{src_finess_nb }}, '0') ||'_'|| {{ src_visit_nb }} || '_' || {{ src_start_date }}
    
    ELSE {{ src_base }} ||'_'|| COALESCE({{src_finess_nb }}, '0') ||'_'|| {{ src_visit_nb }} 

END 

{% endmacro %}


{% macro visit_detail_id_source_value( src_base, src_visit_occurrence_id, src_visit_detail_nb) %}

{{ src_base }} || '_' || {{ src_visit_occurrence_id }} ||'_'||  {{ src_visit_detail_nb }} 

{% endmacro %}