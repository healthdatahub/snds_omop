{%macro visit_detail_pmsi(src_table, src_base, src_finess_nb, src_visit_nb, src_detail_nb, nb_jrs, um_typ) %}

-- This macro contains repetitive code that cannot be reduces. It queries the different PMSI tables which will feed the OMOP-CDM VISIT_DETAIL table 
-- It calls the visit_source_value macro 
-- src_base is the part of the PMSI (MCO, HAD, SSR, RIP)
-- src_table is the table where info is stored 
-- um_typ is the variable where the medical unit code is stored in the src_table 
-- nbr_jrs is the number of days spent in the Medical Unit 
-- src_finess_nb is the variable where the juridical identifier of a care site is stored in the src_table
-- src_visit_nb is the unique identifier of the visit in the src_table
-- src_detail_nb is the identifier of the visit_detail in the src_table

SELECT 
    {{ visit_occurrence_source_value(src_base, src_finess_nb, src_visit_nb) }} AS visit_occurrence_source_value,
    {{ visit_occurrence_source_value(src_base, src_finess_nb, src_visit_nb) }} ||'_'|| {{ src_detail_nb }}::text AS visit_detail_source_value,
    {{ src_detail_nb }}::text AS detail_num,
    coalesce({{ nb_jrs }}, 0)        AS jours,
    {{ um_typ }}        AS um_typ
    
FROM {{src_table}} t1
WHERE {{um_typ}} IS NOT NULL



{% endmacro %}