{% macro visit_detail_dates(start_date, date_time) %}

-- Computes start_date and end_date of a visit_detail, given the visit_start_date, the duration of the visit_detail (jours) and the time passed between the visit_start_date and the visit_detail_start_date (delai)
-- start_date and date_time are boolean 


{% if start_date %}

        {% if date_time %}

                ((visit_start_date::date + (delai::integer - jours::integer))||' 00:00:00')::timestamp

        {% else %}

                (visit_start_date::date + (delai::integer - jours::integer))

        {% endif %}

{% else %}

        {% if date_time %}

                ((visit_start_date::date + delai::integer)||' 00:00:00')::timestamp

        {% else %}


                (visit_start_date::date + delai::integer)::date

        {%endif%}


{% endif %}

{% endmacro %}