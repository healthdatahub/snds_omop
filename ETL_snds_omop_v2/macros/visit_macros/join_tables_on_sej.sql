{% macro join_tables_on_sej(table_a, table_b) %}


-- table_b contains finess_geo

SELECT DISTINCT
    a.*,
    COALESCE(b.finess_geo, b.finess_j) AS finess_geo

FROM {{ table_a }} a
    JOIN {{ table_b }} b
        ON (a.finess_j = b.finess_j
        AND a.visit_nb = b.visit_nb)
        
        
        
        
{% endmacro %}