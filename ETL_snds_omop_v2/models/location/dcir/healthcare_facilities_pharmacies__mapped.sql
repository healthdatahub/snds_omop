{{ config(materialized='view') }}

-- Pour les établissements et les pharmacies, ce model sélectionne :
-- * l'adresse,
-- * la raison sociale,
-- * le code commune,
-- * le code région

-- grâce aux fichiers de correspondance à partir du numéro finess sur 8 caractères.



WITH 
    finess_dcir AS(
            {{ dbt_utils.union_relations(
                relations=[
                    ref('finess_pharmacies__selected'),
                    ref('finess_healthcare_facilities__selected')
                ] ) }} ),
                
                
    mapping_finess_location AS(
        SELECT *
        FROM {{ ref('bse__mapping_finess_location') }}), 
        
    mapping_region_departement AS(
        SELECT *
        FROM {{ ref('bse__mapping_region_departement') }} ),    
    
            
            
    dcir_finess_mapped AS(
        SELECT DISTINCT
            mfl.adresse,                             
            mfl.lib_acheminement,
            mfl.com_code,
            mfl.rs,
            mrp.code_region,
            fd.finess8
            
            
        FROM finess_dcir fd
        
        LEFT JOIN mapping_finess_location mfl
            ON fd.finess8 = mfl.finess8
            
            
        LEFT JOIN mapping_region_departement mrp
            ON mfl.com_code = mrp.com_code)
            
            
SELECT * FROM dcir_finess_mapped