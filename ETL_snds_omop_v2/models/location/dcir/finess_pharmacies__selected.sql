{{ config(materialized='view') }}

-- Ce model sélectionne les numéros finess des pharmacies provenant de la table er_pha_f

SELECT DISTINCT
    etb_pre_fin::numeric::bigint::varchar(50) AS finess8 --réparation provisoire : lié à une problème d'import de la base de données, à supprimer lors de l'export des scripts 
FROM {{ ref('pha_prs__joined') }}      
            

