{{ config(materialized='view') }}

-- Ce modèle sélectionne les numéros finess juridiques et géographiques des établissements du champs SSR contenus 
-- dans les tables t_SSRaaB

WITH union_ssr_b_years AS(

    {% for year in var('years_pmsi') %}
    SELECT DISTINCT
    eta_num::varchar(50)     AS finess_j, 
    eta_num_geo::varchar(50) AS finess_geo,
    aut_typ_um               AS type_um,
    'ssr'                    AS pmsi_product

    FROM {{ source('SSR', 't_ssr' ~ year ~ 'b') }} b

    {% if not loop.last -%}
    UNION ALL 
    {%- endif -%}

    {%- endfor -%})

SELECT * FROM union_ssr_b_years
