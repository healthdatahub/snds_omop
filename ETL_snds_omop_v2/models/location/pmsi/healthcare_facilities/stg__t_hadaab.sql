{{ config(materialized='view') }}

-- Ce modèle sélectionne les numéros finess juridiques et géographiques des établissements du champs HAD contenus 
-- dans les tables t_HADaaB

WITH union_had_b_years AS(

    {% for year in var('years_pmsi') %}
    SELECT DISTINCT
        eta_num_epmsi::varchar(50) AS finess_j, 
        'had'                      AS pmsi_product,

            {% if year | int > 16 %}    
        eta_num_geo::varchar(50)   AS finess_geo,
        {{year}} AS year 

            {% else %}
        eta_num_two::varchar(50)   AS finess_geo,
        {{year}} AS year 

            {% endif %}

    FROM {{ source('HAD', 't_had' ~ year ~ 'b') }} b 

    {% if not loop.last -%}
    UNION ALL 
    {%- endif -%}

    {%- endfor -%})


SELECT * FROM union_had_b_years
