{{ config(materialized='table') }}

-- Ce modèle référence un fichier de correspondance qui permet de sélectionner des informations concernant l'adresse 
-- d'un établissement et sa raison sociale à partir de son numéro finess juridique ou géographique 
        

WITH 
    -- Exception pour le code commune de la Corse
    correct_code_commune_corse AS(
        SELECT 
            ej_finess::varchar(50),
            et_finess::varchar(50),
            finess8::varchar(50),
            rs::varchar(255),
            ej_rs::varchar(255),
            code_postal::varchar(9),
            {{ correct_code_commune(code = 'com_code') }}::varchar(9) AS com_code, -- Correction du code commune pour les communes corses (2A et 2B --> 20)
            lib_acheminement::varchar(50),
            adresse::varchar(50),
            com_code AS com_code_init

        FROM {{ source('correspondance', 'correspondance_finess_codecom' ) }} ),
        
        
    -- Choix d'une seule raison sociale par établissement
    last_ej_rs AS( 
        SELECT
            ej_finess,
            et_finess,
            COALESCE(et_finess, ej_finess) AS finess_geo, -- Variable de jointure intermédiaire
            finess8,
            rs,

            last_value(ej_rs) OVER ( 
                PARTITION BY ej_finess
                ROWS BETWEEN unbounded preceding and unbounded following 
            ) AS ej_rs, -- Certaines entités juridiques sont représentées sous des noms différents, on choisit un seul nom parmis les possibles

            code_postal,
            com_code,
            com_code_init, -- Ajustement numéro du département sur 3 chiffres
            lib_acheminement,
            adresse

        FROM correct_code_commune_corse),
        
    -- Exception des codes de certaines communes des DOM
    correct_dpt_code AS(
        SELECT
            ej_finess,
            et_finess,
            finess_geo,
            finess8,
            rs,
            ej_rs,
            code_postal,
            com_code,
            lib_acheminement,
            adresse,
            {{ numero_departement(code_commune = 'com_code_init') }}::varchar(3) AS dpt_code
        FROM last_ej_rs
        
        
    )
    
    
SELECT * FROM correct_dpt_code
    
    



        
