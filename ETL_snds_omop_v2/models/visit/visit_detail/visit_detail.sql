{{ config(materialized='incremental',
         schema='omop', unique_key='visit_detail_id') }}

WITH 
    all_um AS (

    {{ dbt_utils.union_relations(relations=[
            ref('visit_detail_ssr'),
            ref('visit_detail_mco') ] ) }} )
            
   
SELECT
    DISTINCT
    {{set_id('visit_detail_id_source_value')}}::bigint      AS visit_detail_id,
    visit_detail_id_source_value                            AS visit_detail_id_source_value,
    person_id::bigint                                       AS person_id,
    0                                                       AS visit_detail_concept_id, -- map codes um_typ avec omop
    visit_detail_start_date                                 AS visit_detail_start_date,
    (visit_detail_start_date::text||' 00:00:00')::timestamp AS visit_detail_start_datetime,
    visit_detail_end_date                                   AS visit_detail_end_date,
    (visit_detail_end_date::text||' 00:00:00')::timestamp   AS visit_detail_end_datetime,
    32810                                                   AS visit_detail_type_concept_id,
    0::bigint                                               AS provider_id,
    care_site_id ::bigint                                   AS care_site_id,
    0                                                       AS admitting_source_concept_id,
    0                                                       AS discharge_to_concept_id,
    NULL::bigint                                            AS preceding_visit_detail_id,
    NULL::bigint                                            AS visit_detail_parent_id,
    visit_detail_source_value::varchar(50)                  AS visit_detail_source_value,
    0                                                       AS visit_detail_source_concept_id,
    NULL::varchar(50)                                       AS admitting_source_value,
    NULL::varchar(50)                                       AS discharge_to_source_value,
    visit_occurrence_id::bigint                             AS visit_occurrence_id


    FROM all_um