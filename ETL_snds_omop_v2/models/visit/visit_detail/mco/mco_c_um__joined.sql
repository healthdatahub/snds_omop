{{ config(materialized='view') }}

WITH
    mco_um AS(
        SELECT DISTINCT 
            base, 
            finess_j,
            visit_nb,
            um_ord_num,
            aut_typ1_um,
            par_dur_sej,
            eta_num_geo
        
        FROM {{ ref('stg__t_mcoaaum_visit') }}),
        
        
    mco_c AS(
        SELECT DISTINCT
            base, 
            finess_j, 
            visit_nb, 
            exe_soi_dtd, 
            exe_soi_dtf
        
        FROM {{ ref('stg__t_mcoaac_visit') }}),
        
        
        
    mco_c_um__joined AS(
        SELECT
            um.base,
            um.finess_j,
            um.visit_nb,
            um.um_ord_num,
            um.aut_typ1_um,
            um.par_dur_sej,
            um.eta_num_geo,
        
            c.exe_soi_dtd,
            c.exe_soi_dtf,
              
            exe_soi_dtd + (SUM(um.par_dur_sej) OVER 
                (PARTITION BY um.base, um.finess_j, um.visit_nb 
                 ORDER BY um.um_ord_num))::integer  AS visit_detail_end_date -- Date de sortie de l'UM calculée avec la date de début du séjour et le nombre de jours passés dans l'UM
        
        
        FROM mco_um um
        JOIN mco_c c USING(base, finess_j, visit_nb) ),
        
        
    calcul_start_date AS(
        SELECT 
            base, 
            finess_j,
            visit_nb,
            um_ord_num,
            aut_typ1_um,
            par_dur_sej,
            eta_num_geo,
            visit_detail_end_date,
        
            CASE 
                WHEN um_ord_num = 1 
                THEN exe_soi_dtd
        
        
                ELSE 
                    LAG(visit_detail_end_date) OVER
                        (PARTITION BY base, finess_j, visit_nb
                                     ORDER BY um_ord_num) 
        
            END visit_detail_start_date -- Date d'entrée dans l'UM calculée avec la date d'entrée et la date de sortie de l'UM précédente
        
        FROM mco_c_um__joined
    )
        
SELECT * FROM calcul_start_date