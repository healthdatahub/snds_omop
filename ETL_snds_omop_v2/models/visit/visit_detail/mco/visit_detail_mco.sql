{{ config(materialized='view') }}


WITH
    
    mco_um AS(
        SELECT 
            base, 
            finess_j,
            visit_nb,
            eta_num_geo || '_' || aut_typ1_um AS care_site_source_value,
            um_ord_num                        AS visit_detail_nb,
            aut_typ1_um                       AS visit_detail_source_value,
            visit_detail_end_date,
            visit_detail_start_date, -- Date d'entrée dans l'UM calculée avec la date d'entrée et la date de sortie de l'UM précédente)
        
            {{ visit_occurrence_source_value(src_base = 'base',
                                            src_finess_nb = 'finess_j',
                                            src_visit_nb = 'visit_nb',
                                            src_start_date = 'visit_detail_start_date')}} AS visit_occurrence_source_value
    
        FROM {{ ref('mco_c_um__joined') }} ),
        
    
    
    visit_occurrence_pmsi AS(
        SELECT 
            visit_occurrence_source_value,
            visit_occurrence_id,
            person_id

        FROM  {{ ref('visit_occurrence_pmsi') }}),
        
        
    care_site AS(
        SELECT
            care_site_source_value,
            care_site_id
    
        FROM {{ ref('care_site') }}),
    
    
    
    visit_detail_mco AS(
        SELECT 
            v.person_id,
            v.visit_occurrence_id,
        
            m.visit_detail_start_date,
            m.visit_detail_end_date,
            m.visit_detail_source_value,
        
            {{ visit_detail_id_source_value(src_base = 'base',
                                            src_visit_occurrence_id = 'visit_occurrence_id',
                                            src_visit_detail_nb = 'visit_detail_nb') }} AS visit_detail_id_source_value,
        
            c.care_site_id
        
        
            
        FROM mco_um m
        JOIN visit_occurrence_pmsi v USING(visit_occurrence_source_value)
        LEFT JOIN care_site c USING(care_site_source_value) )
    
    
SELECT * FROM visit_detail_mco