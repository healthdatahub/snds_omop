{{ config(materialized='view') }}

WITH 
    mco_um AS(

    {% for year in var('years_pmsi') %}

        {% set source_name = 'MCO' %}
        {% set table_name = 't_mco' ~ year ~ 'um'%}
        {% set base = ("'" ~ 'mco' ~ year ~ "'") | string %}
        
        {% if year < 20 %}

        SELECT
            {{ base }}           AS base,
            eta_num              AS finess_j, 
            rsa_num              AS visit_nb,
            um_ord_num::integer  AS um_ord_num,
            par_dur_sej::integer AS par_dur_sej, 
            substring(aut_typ1_um from '[^ ]+'::text) AS aut_typ1_um, --Suprression de l'espace entre le nombre et le chiffre dans le code
            eta_num_geo
        
        FROM {{ source(source_name, table_name) }}
        
        
        {% else %}
        
         SELECT
            {{ base }}           AS base,
            eta_num              AS finess_j, 
            rsa_num              AS visit_nb,
            rum_ord_num::integer AS um_ord_num,
            par_dur_sej::integer AS par_dur_sej, 
            substring(aut_typ1_um from '[^ ]+'::text) AS aut_typ1_um, --Suprression de l'espace entre le nombre et le chiffre dans le code
            eta_num_geo
        
        FROM {{ source(source_name, table_name) }}
        
        {% endif %}
            

        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%} )
        



SELECT * FROM mco_um

