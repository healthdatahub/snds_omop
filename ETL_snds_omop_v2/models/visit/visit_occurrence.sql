{{ config(materialized='incremental', schema='omop', unique_key='visit_occurrence_id') }}
    
    
    
WITH 

    dcir_pmsi__unioned AS(
        {{ dbt_utils.union_relations(
            relations=[
                ref('visit_occurrence_dcir'),
                ref('visit_occurrence_pmsi')
            ] ) }} )
            
            
            
            
SELECT 

        visit_occurrence_id,
        person_id,
        visit_concept_id,
        visit_start_date,
        visit_start_datetime,
        visit_end_date,
        visit_end_datetime,
        visit_type_concept_id,
        care_site_id,
        visit_source_value,
        visit_occurrence_source_value,
        provider_id,
        visit_source_concept_id,
        admitting_source_concept_id,
        admitting_source_value,
        discharge_to_concept_id,
        discharge_to_source_value,
        preceding_visit_occurrence_id
        
        
FROM dcir_pmsi__unioned


        
        
