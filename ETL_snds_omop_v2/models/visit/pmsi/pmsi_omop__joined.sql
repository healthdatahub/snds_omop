{{ config(materialized='view') }}


WITH 
    
    pmsi AS(
        SELECT 
            person_source_value,
            care_site_source_value,
            visit_start_date,
            visit_end_date,
            visit_concept_id,
            visit_source_value,
            visit_occurrence_source_value,
            admitting_source_value,
            discharge_to_source_value,
            provider_source_value
        
    FROM {{ ref('visit_pmsi_variables__selected') }}),
            
    person AS(
        SELECT 
            person_source_value,
            person_id
        FROM {{ ref('person') }} ),
        
        
    care_site AS(
        SELECT 
            care_site_source_value,
            care_site_id
        FROM {{ ref('care_site') }}),
        
    provider AS (
        SELECT 
            provider_source_value,
            provider_id
        FROM {{ref('provider')}}
    ),  
    ms_ent_v AS (
        SELECT 
            source_code,
            target_concept_id
        FROM {{source('vocabularies', 'source_to_concept_map')}}
        WHERE source_vocabulary_id = 'MS_ENT_V'
    ),
    ms_sor_v AS (
        SELECT 
            source_code,
            target_concept_id
        FROM {{source('vocabularies', 'source_to_concept_map')}}
        WHERE source_vocabulary_id = 'MS_SOR_V'    
    ),  
    
    pmsi_person_care_site_joined AS(
        SELECT 
            pers.person_id,
        
            cs.care_site_id,
        
            p.provider_id,
            
            admitting_source_value,
            ent.target_concept_id AS admitting_source_concept_id,
            discharge_to_source_value,
            sor.target_concept_id AS discharge_to_concept_id,
            
        
            pmsi.visit_start_date,
            pmsi.visit_end_date,
            pmsi.visit_concept_id,
            pmsi.visit_source_value,
            pmsi.visit_occurrence_source_value
            
            
        FROM pmsi 
            JOIN person pers USING(person_source_value)
            JOIN care_site cs USING(care_site_source_value) 
            LEFT JOIN provider p USING(provider_source_value)
            LEFT JOIN ms_ent_v ent ON admitting_source_value = ent.source_code
            LEFT JOIN ms_sor_v sor ON discharge_to_source_value = sor.source_code
)     
                    
                    
SELECT * FROM pmsi_person_care_site_joined