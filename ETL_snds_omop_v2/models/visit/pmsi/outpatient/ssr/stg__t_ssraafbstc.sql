{{ config(materialized='view') }}

WITH union_ssr_fbstc_years AS(

    {%- for year in var('years_pmsi') -%}
        {% set source_name = 'SSR' %}
        {% set table_name = 't_ssr' ~ year ~ 'fbstc'%}
        {% set base = ("'" ~ 'ssr' ~ year ~ 'ace' ~ "'") | string %}
        {% if year | int > 16 %}
            SELECT 
                eta_num AS finess_j,
                seq_num AS visit_nb,
                eta_num_geo AS care_site_source_value,
                CASE 
                    WHEN SUBSTR(exe_spe, 1, 1) = '0' THEN SUBSTR(exe_spe, 2)
                    ELSE exe_spe
                END exe_spe,
                {{base}} AS base
            FROM {{ source(source_name, table_name) }} fb
            {% if not loop.last -%}
            UNION ALL 
            {%- endif -%}
        {% else %}
            SELECT 
                eta_num AS finess_j,
                seq_num AS visit_nb,
                eta_num AS care_site_source_value,
                CASE 
                    WHEN SUBSTR(exe_spe, 1, 1) = '0' THEN SUBSTR(exe_spe, 2)
                    ELSE exe_spe
                END exe_spe,
                {{base}} AS base
            FROM {{ source(source_name, table_name) }} fb
            {% if not loop.last -%}
            UNION ALL 
            {%- endif -%}
        {%-endif-%}
        
    {%- endfor -%})

SELECT * FROM union_ssr_fbstc_years
