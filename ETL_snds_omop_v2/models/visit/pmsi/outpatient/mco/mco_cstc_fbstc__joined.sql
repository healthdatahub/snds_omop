{{ config(materialized='view') }}

WITH join_cstc_fbstc AS (
    SELECT 
        cstc.num_enq,
        cstc.exe_soi_dtd,
        cstc.exe_soi_dtf,
        cstc.base,
        cstc.finess_j,
        cstc.visit_nb,
        
        NULL      AS visit_source_value,
        NULL      AS admitting_source_value,
        NULL      AS discharge_to_source_value,
    
        MAX(fbstc.exe_spe) AS provider_source_value,
    
    CASE 
        WHEN fbstc.emergency_count > 0  THEN 9203
        ELSE 9202
    END visit_concept_id
        
    FROM 
        {{ref('stg__t_mcoaacstc_visit')}} cstc
        JOIN {{ref('stg__t_mcoaafbstc')}} fbstc
        USING(finess_j, visit_nb, base)
    
    GROUP BY 1,2,3,4,5,6,7,8,9,11
)

SELECT * FROM join_cstc_fbstc

