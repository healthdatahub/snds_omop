{{ config(materialized='view') }}

WITH union_ssr_b_years AS(

    {% for year in var('years_pmsi') %}
        {% set source_name = 'SSR' %}
        {% set table_name = 't_ssr' ~ year ~ 'b'%}
        {% set base = ("'" ~ 'ssr' ~ year ~"'") | string %}
    
    SELECT DISTINCT
        eta_num::varchar(50)       AS finess_j, 
        eta_num_geo::varchar(50)   AS finess_geo,
        rha_num::varchar(50)       AS visit_nb,
        rhs_num                    AS rhs_num,     -- Utile pour visit_detail
        jp_hwe || jp_we            AS jp_week,     -- Utile pour visit_detail
        rhs_ant_sej_ent::integer   AS rhs_ant_sej_ent, -- Utile pour visit_detail
        aut_typ_um                 AS aut_typ_um,  -- Utile pour visit_detail
    
        first_value(ent_prv) OVER (
            PARTITION BY eta_num, rha_num
            ORDER BY rhs_num 
            ROWS BETWEEN unbounded preceding and unbounded following ) AS ent_prv,
        hos_typ_um                 AS hos_typ_um,
    
        {{base}}                   AS base,
    
        last_value(sor_des) OVER (
            PARTITION BY eta_num, rha_num
            ORDER BY rhs_num 
            ROWS BETWEEN unbounded preceding and unbounded following ) AS sor_des
        
    FROM {{ source(source_name, table_name) }} b 

    {% if not loop.last -%}
    UNION ALL 
    {%- endif -%}

    {%- endfor -%})


SELECT * FROM union_ssr_b_years