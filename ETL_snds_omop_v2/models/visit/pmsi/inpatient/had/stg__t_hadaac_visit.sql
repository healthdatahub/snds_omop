{{ config(materialized='view') }}

-- This models uses clean_table_c macro 
-- Removes non-valid lines 
-- Fixes dates format
-- Selects relevant variables to compute visit_source_value 


WITH union_had_c_years AS(

    {%- for year in var('years_pmsi') -%}

        {% set source_name = 'HAD' %}
        {% set table_name = 't_had' ~ year ~ 'c'%}
        {% set base = ("'" ~ 'had' ~ year ~ "'") | string %}

        {{ clean_table_c(
                source_name = source_name,
                table_c     = table_name,
                 base       = base,
                 finess_j   = 'eta_num_epmsi',
                 visit_nb   = 'rhad_num') }}

        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%})


SELECT * FROM union_had_c_years






                 




