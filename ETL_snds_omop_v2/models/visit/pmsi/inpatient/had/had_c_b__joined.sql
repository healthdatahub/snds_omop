{{ config(materialized='view') }}



WITH 
    join_c_b AS(
        SELECT
            c.num_enq,
            c.exe_soi_dtd,
            c.exe_soi_dtf,
            c.base,
            c.finess_j,
            c.visit_nb,
        
            'Hospitalisation à domicile'  AS visit_source_value,
            MAX(b.ent_prv)                AS admitting_to_source_value,
            MAX(b.sor_des)                AS discharge_to_source_value,
        
            NULL                          AS provider_source_value,
            9201                          AS visit_concept_id
    
    
        FROM {{ ref('stg__t_hadaab_visit') }} b 
        JOIN {{ ref('stg__t_hadaac_visit') }} c USING(finess_j, visit_nb, base)
        GROUP BY 1,2,3,4,5,6,7,10,11)
        
        
SELECT * FROM join_c_b
                              
                              
                              
