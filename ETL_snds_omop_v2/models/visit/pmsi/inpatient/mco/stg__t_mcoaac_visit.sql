{{ config(materialized='view') }}

-- This models uses clean_table_c macro 
-- Removes non-valid lines 
-- Fixes dates format
-- Selects relevant variables to compute visit_source_value 

WITH union_mco_c_years AS(

    {%- for year in var('years_pmsi') -%}

        {% set source_name = 'MCO' %}
        {% set table_name = 't_mco' ~ year ~ 'c'%}
        {% set base = ("'" ~ 'mco' ~ year ~ "'") | string %}

        {{ clean_table_c(source_name = source_name,
                table_c = table_name,
                 base = base,
                 finess_j = 'eta_num',
                 visit_nb = 'rsa_num') }}

        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%})


SELECT * FROM union_mco_c_years