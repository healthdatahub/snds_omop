{{ config(materialized='table') }}

WITH join_c_b AS (
    SELECT 
        c.num_enq,
        c.exe_soi_dtd,
        c.exe_soi_dtf,
        c.base,
        c.finess_j,
        c.visit_nb,
        
        NULL      AS visit_source_value,
        b.ent_prv AS admitting_source_value,
        b.sor_des AS discharge_to_source_value,
    
        NULL      AS provider_source_value,
        
    CASE 
        WHEN b.ent_prv = '5' THEN 262
        ELSE 9201
    END visit_concept_id
        
    FROM 
        {{ref('stg__t_mcoaac_visit')}} c
        JOIN {{ref('stg__t_mcoaab_visit')}} b
        USING(finess_j, visit_nb, base)
)

SELECT * FROM join_c_b



