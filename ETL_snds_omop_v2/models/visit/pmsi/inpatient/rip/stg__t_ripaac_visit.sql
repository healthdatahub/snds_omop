{{ config(materialized='view') }}

-- This models uses clean_table_c macro 
-- Removes non-valid lines 
-- Fixes dates format
-- Selects relevant variables to compute visit_source_value 

WITH union_rip_c_years AS(

    {%- for year in var('years_pmsi') -%}

        {% set source_name = 'RIP' %}
        {% set table_name = 't_rip' ~ year ~ 'c'%}
        {% set base = ("'" ~ 'rip' ~ year ~"'") | string %}

        {{ clean_table_c(source_name = source_name,
                 table_c = table_name,
                 base = base,
                 finess_j = 'eta_num_epmsi',
                 visit_nb = 'rip_num') }}

        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%})


SELECT 
    num_enq,
    exe_soi_dtd,
    exe_soi_dtf,
    finess_j,
    visit_nb,
    base
    
FROM union_rip_c_years


                 

