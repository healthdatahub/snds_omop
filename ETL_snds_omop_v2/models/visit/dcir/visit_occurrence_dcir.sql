{{config(materialized='table', indexes = [{'columns' : ['visit_occurrence_id'], 'unique' : True} ])}}

-- Ce modèle complète les visites préalablement récupérées dans er_ete_f et er_prs_f en ajoutant le type de visit. Il représente la partie de la table VISIT_OCCURRENCE concernant ke DCIR. 

WITH 
visit_concept_mapping AS
(
    SELECT  
        dcir.visit_occurrence_source_value,
        dcir.visit_start_date, 
        dcir.visit_end_date,
        dcir.person_id,
        dcir.care_site_id,
        dcir.provider_id,
    
        stc.target_concept_id AS visit_concept_id 
    FROM 
        {{ref('dcir_omop_fk__mapped')}} dcir
        LEFT JOIN {{ source('vocabularies', 'source_to_concept_map') }}  stc 
            ON stc.source_code = dcir.prs_nat_ref
    
    WHERE stc.source_vocabulary_id = 'IR_NAT_V'
        
),

dcir_visits AS 
(
    SELECT
        {{set_id(['visit_occurrence_source_value'])}}::bigint AS visit_occurrence_id,
        person_id::bigint                                     AS person_id,
        visit_concept_id::integer                             AS visit_concept_id,
        visit_start_date::date                                AS visit_start_date,
        (visit_start_date::text||' 00:00:00')::timestamp      AS visit_start_datetime,
        visit_end_date::date                                  AS visit_end_date,
        (visit_end_date::text||' 00:00:00')::timestamp        AS visit_end_datetime,
        32810                                                 AS visit_type_concept_id,
        care_site_id::bigint                                  AS care_site_id,
        visit_occurrence_source_value::text                   AS visit_occurrence_source_value,
        provider_id::bigint                                   AS provider_id,
        0                                                     AS visit_source_concept_id,
        0                                                     AS admitting_source_concept_id,
        NULL::varchar(50)                                     AS admitting_source_value,
        0                                                     AS discharge_to_concept_id,
        NULL::varchar(50)                                     AS discharge_to_source_value,
        NULL::bigint                                          AS preceding_visit_occurrence_id
FROM
        visit_concept_mapping
)

SELECT * FROM dcir_visits