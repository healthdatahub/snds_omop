{{config(materialized = 'view')}}

-- Ce model sélectionne les visites associées à la table er_ete_f dont les données ne remontent pas dans le PMSI.

WITH 

provider_pse_spe_cod AS (
    SELECT 
        num_enq                              AS person_source_value, 
        {{
            visit_occurrence_source_value(
                src_base= "'dcir'", 
                src_finess_nb= 'etb_pre_fin', 
                src_visit_nb ='dcir_visit_id'
            )
        }}                                   AS visit_occurrence_source_value,
        etb_pre_fin                          AS care_site_source_value,
        pse_spe_cod                          AS provider_source_value,
        exe_soi_dtd                          AS visit_start_date, 
        COALESCE(exe_soi_dtf, exe_soi_dtd)   AS visit_end_date,
    
        prs_nat_ref,
        etb_cat_rg1,
        ete_cat_cod
    FROM 
        {{ref('ete_quantity__grouped')}}
    WHERE pse_spe_cod NOT IN ('0', '99')

),

provider_pse_act_nat AS (
    SELECT 
        num_enq                              AS person_source_value, 
        {{
            visit_occurrence_source_value(
                src_base= "'dcir'", 
                src_finess_nb= 'etb_pre_fin', 
                src_visit_nb ='dcir_visit_id'
            )
        }}                                   AS visit_occurrence_source_value,
        etb_pre_fin                          AS care_site_source_value,
        pse_act_nat                          AS provider_source_value,
        exe_soi_dtd                          AS visit_start_date, 
        COALESCE(exe_soi_dtf, exe_soi_dtd)   AS visit_end_date,
    
        prs_nat_ref,
        etb_cat_rg1,
        ete_cat_cod
    FROM 
        {{ref('ete_quantity__grouped')}}
    WHERE pse_act_nat NOT IN ('0', '99')

),

no_provider AS (
    SELECT 
        num_enq                              AS person_source_value, 
        {{
            visit_occurrence_source_value(
                src_base= "'dcir'", 
                src_finess_nb= 'etb_pre_fin', 
                src_visit_nb ='dcir_visit_id'
            )
        }}                                   AS visit_occurrence_source_value,
        etb_pre_fin                          AS care_site_source_value,
        '0'                                  AS provider_source_value,
        exe_soi_dtd                          AS visit_start_date, 
        COALESCE(exe_soi_dtf, exe_soi_dtd)   AS visit_end_date,
    
        prs_nat_ref,
        etb_cat_rg1,
        ete_cat_cod
    FROM 
        {{ref('ete_quantity__grouped')}}
    WHERE pse_act_nat IN ('0', '99') AND pse_spe_cod IN ('0', '99')

),

all_provider AS (
SELECT * FROM provider_pse_spe_cod
UNION ALL 
SELECT * FROM provider_pse_act_nat
UNION ALL
SELECT * FROM no_provider
),

drop_duplicated_data_from_pmsi AS (
    SELECT 
        person_source_value, 
        visit_occurrence_source_value,
        care_site_source_value,
        provider_source_value,
        visit_start_date, 
        visit_end_date,
    
        prs_nat_ref
    FROM 
        all_provider
    {{ categorize_facilities() }} -- Filtre qui enlève les visites associées à des établissements dont les données remontent dans le PMSI
)

SELECT * FROM drop_duplicated_data_from_pmsi WHERE care_site_source_value IS NOT NULL 

