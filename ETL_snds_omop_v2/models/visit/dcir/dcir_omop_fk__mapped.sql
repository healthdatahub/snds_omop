{{config(materialized = 'table', indexes = [{'columns' : ['visit_occurrence_source_value'], 'unique' : True} ])}}

-- Ce model sélectionne les visites provenant de er_ete_f et celles provenant de er_prs_f mais pas er_ete_f, et les joint aux tables PERSON, CARE_SITE et PROVIDER pour récuperer respectivement les variables person_id, care_site_id et provider_id.

WITH 
foreign_key_mapping AS
(
    SELECT  
        prs_ete.visit_occurrence_source_value,
        prs_ete.visit_start_date, 
        prs_ete.visit_end_date,
    
        pers.person_id,
    
        cs.care_site_id,
    
        pro.provider_id,
    
        prs_ete.prs_nat_ref
    FROM 
        {{ref('prs_ete_not_pmsi__unioned')}} prs_ete
        JOIN {{ref('person')}} pers USING(person_source_value) -- On garde uniquement les prestations qui correspondent à des patients enregistrés dans PERSON
        LEFT JOIN {{ref('care_site')}} cs USING(care_site_source_value)
        LEFT JOIN {{ref('provider')}} pro USING(provider_source_value)
        
)

SELECT * FROM foreign_key_mapping