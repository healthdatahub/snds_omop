{{config(materialized = 'table', indexes = [{'columns' : ['visit_occurrence_source_value'], 'unique' : True} ])}}

-- Ce modèle réalise l'union des visites provenant de er_ete_f et de celles qui proviennent de er_prs_f mais ne sont pas dans er_ete_f.

WITH er_ete_f_not_pmsi AS 
(
    SELECT 
        person_source_value, 
        visit_occurrence_source_value,
        care_site_source_value,
        provider_source_value,
        visit_start_date, 
        visit_end_date,
    
        prs_nat_ref
    FROM 
        {{ref('er_ete_f_not_pmsi__selected')}}
),
er_prs_f_not_pmsi AS
(
    SELECT 
        person_source_value, 
        visit_occurrence_source_value,
        care_site_source_value,
        provider_source_value,
        visit_start_date, 
        visit_end_date,
    
        prs_nat_ref
    FROM 
        {{ref('er_prs_f_not_pmsi__selected')}}
),
ete_prs__unioned AS
(
    SELECT * FROM er_ete_f_not_pmsi
    UNION ALL 
    SELECT * FROM er_prs_f_not_pmsi
)

SELECT DISTINCT * FROM ete_prs__unioned
