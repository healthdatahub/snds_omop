{{config(materialized = 'view')}}

-- Ce modèle sélectionne les visites provenant de la table er_ete_f. Il ne garde que les visites utilisées dans des établissement dont les données ne remontent pas dans le PMSI.

WITH 
provider_pse_spe_cod AS (
    SELECT 
        num_enq                                  AS person_source_value, 
        {{
            visit_occurrence_source_value(
                src_base= "'dcir'", 
                src_finess_nb= 'etb_pre_fin', 
                src_visit_nb ='dcir_visit_id'
            )
        }}                                       AS visit_occurrence_source_value,
        etb_pre_fin                              AS care_site_source_value,
        pse_spe_cod                              AS provider_source_value,
        exe_soi_dtd                              AS visit_start_date, 
        COALESCE(exe_soi_dtf, exe_soi_dtd)       AS visit_end_date,
        prs_nat_ref, 
        dcir_key_id
    FROM {{ref('bse_er_prs_f')}}
    WHERE exe_soi_dtd IS NOT NULL 
        AND pse_spe_cod NOT IN ('0', '99')
),
provider_pse_act_nat AS (
    SELECT 
        num_enq                                  AS person_source_value, 
        {{
            visit_occurrence_source_value(
                src_base= "'dcir'", 
                src_finess_nb= 'etb_pre_fin', 
                src_visit_nb ='dcir_visit_id'
            )
        }}                                       AS visit_occurrence_source_value,
        etb_pre_fin                              AS care_site_source_value,
        pse_act_nat                              AS provider_source_value,
        exe_soi_dtd                              AS visit_start_date, 
        COALESCE(exe_soi_dtf, exe_soi_dtd)       AS visit_end_date,
        prs_nat_ref, 
        dcir_key_id
    FROM {{ref('bse_er_prs_f')}}
    WHERE exe_soi_dtd IS NOT NULL 
        AND pse_act_nat NOT IN ('0', '99')
),
no_provider AS (
    SELECT 
        num_enq                                  AS person_source_value, 
        {{
            visit_occurrence_source_value(
                src_base= "'dcir'", 
                src_finess_nb= 'etb_pre_fin', 
                src_visit_nb ='dcir_visit_id'
            )
        }}                                       AS visit_occurrence_source_value,
        etb_pre_fin                              AS care_site_source_value,
        '0'                                      AS provider_source_value,
        exe_soi_dtd                              AS visit_start_date, 
        COALESCE(exe_soi_dtf, exe_soi_dtd)       AS visit_end_date,
        prs_nat_ref, 
        dcir_key_id
    FROM {{ref('bse_er_prs_f')}}
    WHERE exe_soi_dtd IS NOT NULL 
        AND pse_act_nat IN ('0', '99') 
        AND pse_spe_cod IN ('0', '99')
),
all_provider AS (
SELECT * FROM provider_pse_spe_cod
UNION ALL 
SELECT * FROM provider_pse_act_nat
UNION ALL
SELECT * FROM no_provider
),

relevant_columns_er_ete_f AS
(
    SELECT 
        dcir_key_id
    FROM
        {{ref('ete_prs__joined')}}
),

rows_not_in_er_ete_f AS (
    SELECT 
        DISTINCT
        person_source_value, 
        visit_occurrence_source_value,
        care_site_source_value,
        provider_source_value,
        visit_start_date, 
        visit_end_date,
    
        prs_nat_ref 
    
    FROM all_provider prs
    LEFT JOIN relevant_columns_er_ete_f ete
    USING(dcir_key_id)
    WHERE 
        ete.dcir_key_id IS NULL -- On sélectionne uniquement les prestations qui sont dans er_prs_f_, mais pas dans er_ete_f
        
)

SELECT * FROM rows_not_in_er_ete_f

