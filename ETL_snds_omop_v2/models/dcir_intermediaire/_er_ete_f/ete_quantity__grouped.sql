{{config(materialized='table')}}

-- Ce model sert à réaliser les régularisations dans la table er_ete_f. 
-- On somme les quantités en groupant par identifiant de visit dcir_visit_id. 
-- On supprime ainsi les lignes en double dues à des corrections dans er_prs_f. 

WITH fix_ete_quantity 
 
AS
(
    SELECT 
        etb_exe_fin,
        ete_cat_cod,
        etb_pre_fin,
        pse_spe_cod,
        pse_act_nat,
        num_enq,
        exe_soi_dtd,
        exe_soi_dtf,
        dcir_visit_id, 
        etb_cat_rg1,
        prs_nat_ref,
    
        SUM(prs_act_qte) AS quantity -- on somme les quantités associées à une même visite 
    FROM 
        {{ref('ete_prs__joined')}}
    GROUP BY 1,2,3,4,5,6,7, 8, 9, 10,11  
)


SELECT * from fix_ete_quantity 