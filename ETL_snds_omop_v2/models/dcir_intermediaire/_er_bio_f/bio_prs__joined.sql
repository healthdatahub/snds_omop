{{ config(materialized='table') }}
    
-- Ce modèle joint la table ER_BIO_F à la table ER_PRS_F   


WITH 
    bio_join_prs AS (
        SELECT 
            a.bio_prs_ide,
            a.bio_act_qsn, 
            b.etb_pre_fin,
            b.num_enq,
            b.exe_soi_dtd,
            b.dcir_visit_id
        FROM {{ref('stg_er_bio_f')}} a
        JOIN {{ ref('bse_er_prs_f')}} b USING(dcir_key_id) )

SELECT * FROM bio_join_prs

