{{config(materialized='table')}}
    
    

WITH       
    fix_bio_quantity AS (
        SELECT 
            a.bio_prs_ide,
            a.etb_pre_fin,
            a.num_enq,
            a.exe_soi_dtd,
            a.dcir_visit_id,

            SUM(bio_act_qsn) AS quantity
        FROM 
            {{ref('bio_prs__joined')}} a
              
        
        
        GROUP BY 1,2,3,4,5 
    )


SELECT * from fix_bio_quantity 