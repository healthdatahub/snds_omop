# Tables du DCIR - Documentation des modèles


Pour chaque table du DCIR : 
1. On sélectionne les variables nécessaires et on unit toutes les années dans le fichier *stg_er_xxx_f.sql*
2. On joint cette table à la table ER_PRS_F dans le modèle *xxx_prs__joined.sql*
3. On somme les quantités pour avoir la quantité totale dans le modème *xxx_quantity__grouped.sql*