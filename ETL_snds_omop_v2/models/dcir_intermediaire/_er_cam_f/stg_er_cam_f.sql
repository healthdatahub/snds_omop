{{config(materialized='view')}}

WITH union_er_cam_f_years
AS
(
    {% for year in var('years') %}

    SELECT
        cam_prs_ide::varchar(50),
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
    FROM {{ source('ER_CAM_F', 'er_cam_f_' ~  year  ) }}
    
    {% if not loop.last -%}
            UNION ALL 
            {%- endif -%}

        {%- endfor -%})
    


SELECT 
    *
FROM 
    union_er_cam_f_years 
WHERE dcir_key_id IN (SELECT dcir_key_id FROM {{ ref('bse_er_prs_f') }})


    