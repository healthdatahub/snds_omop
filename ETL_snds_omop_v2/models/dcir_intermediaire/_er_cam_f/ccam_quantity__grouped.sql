{{config(materialized='table')}}

WITH fix_ccam_quantity 
 
AS
(
    SELECT 
        cam_prs_ide,
        etb_pre_fin,
        num_enq,
        exe_soi_dtd,
        dcir_visit_id,
    
        SUM(prs_act_qte) AS quantity
    FROM 
        {{ref('cam_prs__joined')}}
    GROUP BY 1,2,3,4,5
)


SELECT * from fix_ccam_quantity 