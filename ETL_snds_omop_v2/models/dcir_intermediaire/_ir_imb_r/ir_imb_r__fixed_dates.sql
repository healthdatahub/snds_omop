{{config(materialized='table')}}


WITH 
fix_ir_imb_r_dates AS
(
    SELECT 
        num_enq,
        med_mtf_cod, 
        imb_etm_nat,
    
        MIN(imb_ald_dtd) AS imb_ald_dtd, 
        MAX(imb_ald_dtf) AS imb_ald_dtf, 
        MAX(ins_dte) AS ins_dte, 
        MAX(upd_dte) AS upd_dte
    FROM {{ref('stg_ir_imb_r')}} 
    GROUP BY 1, 2, 3
    
)

SELECT * FROM fix_ir_imb_r_dates


   
    
    