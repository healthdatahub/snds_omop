{{ config(materialized='table') }}
         
/* SELECT on ER_PRS_F */ 

-- Ce model sélectionne l'ensemble des variables et attribue les types attendus dans le format OMOP-CDM de ER_PRS_F utilisées dans l'ETL 
-- On fait d'abord l'union de toutes les années (ex: ER_PRS_F_2018, ER_PRS_F_2019, etc). 
-- On supprime également les lignes transmises pour information. 

WITH 
    union_tables AS(
        {% for year in var('years') %} -- Union de toutes les années 

        SELECT
            num_enq::varchar(100),
            CASE 
                WHEN etb_pre_fin LIKE '%.%'
                THEN SUBSTR(etb_pre_fin, 1, CHAR_LENGTH(etb_pre_fin) - 2)::varchar(100)
            ELSE etb_pre_fin::varchar(100) -- gestion du problème de type dans pandas : finess sur 7 caractères et .0 à la fin 
            END etb_pre_fin,
            {{ format_date('exe_soi_dtd') }}    AS exe_soi_dtd, -- modification date format postgres
            {{ format_date('exe_soi_dtf') }}    AS exe_soi_dtf, -- modification date format postgres
            prs_nat_ref::varchar(50),
            pfs_exe_num::varchar(100),
            pse_spe_cod::varchar(50),
            psp_spe_cod::varchar(50),
            pse_act_nat::varchar(50),
            psp_act_nat::varchar(50),
            {{ format_date('pre_pre_dtd') }}    AS pre_pre_dtd, -- modification date format postgres
            prs_act_qte::integer,
            cpl_maj_top::integer,
            {{ set_id(var('dcir_key')) }}       AS dcir_key_id, -- crée un id à partir des 9 clés de jointure du DCIR
            {{ set_id(var('dcir_visit_key')) }} AS dcir_visit_id, 
            prs_pai_mnt::numeric,
            bse_rem_mnt::numeric,
            cpl_rem_mnt::numeric,

            dpn_qlf::varchar(100), -- utilisée pour filtrer
            prs_dpn_qlp::varchar(100), -- utilisée pour filtrer
            {{ year }}                          AS created_year

        FROM {{source('ER_PRS_F', 'er_prs_f_' ~ year )}}
        WHERE exe_soi_dtd IS NOT NULL -- suppression des lignes où la date de visite n'est pas remplie 
        

    
            {% if not loop.last -%}
            UNION ALL 
            {%- endif -%}

        {%- endfor -%}),



    filter_on_expenses AS(
        SELECT DISTINCT
            num_enq,
            etb_pre_fin,
            exe_soi_dtd,
            exe_soi_dtf,
            prs_nat_ref,
            pfs_exe_num,
            pse_spe_cod,
            psp_spe_cod,
            pse_act_nat,
            psp_act_nat,
            pre_pre_dtd,
            prs_act_qte,
            cpl_maj_top,
            dcir_key_id,
            dcir_visit_id,
            prs_pai_mnt,
            bse_rem_mnt,
            cpl_rem_mnt,
            created_year
        FROM union_tables
        WHERE dpn_qlf != '71' AND prs_dpn_qlp != '71') -- Suppression des lignes transmises en double 


SELECT *
FROM filter_on_expenses
