{{config(materialized='view')}}

-- Ce model sert à récupérer les variables d'intérêt d'er_pha_f et à l'union de toutes les années

WITH 
union_er_pha_f_years
AS
(
    {% for year in var('years') %}
    SELECT
        pha_prs_c13::varchar(50),
        COALESCE(pha_act_qsn::integer, 0) AS pha_act_qsn,
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
    FROM {{ source('ER_PHA_F', 'er_pha_f_' ~  year  ) }}
    WHERE pha_act_qsn != 'pha_act_qsn'
    
        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%}
)

SELECT * 
FROM union_er_pha_f_years
WHERE pha_prs_c13 != '0' -- On supprime les lignes pour lesquelles le code cip 13 est inconnu 
AND dcir_key_id IN (SELECT dcir_key_id FROM {{ ref('bse_er_prs_f') }})