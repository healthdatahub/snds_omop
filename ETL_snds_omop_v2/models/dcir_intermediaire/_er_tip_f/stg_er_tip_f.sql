{{config(materialized='view')}}


WITH 
union_er_tip_f_years
AS
(
    {% for year in var('years') %}
    SELECT
        tip_prs_ide::varchar(50),
        {{ format_date('tip_acl_dtd') }} AS tip_acl_dtd,
        {{ format_date('tip_acl_dtf') }} AS tip_acl_dtf,
        tip_act_qsn::integer,
        tip_prs_typ::varchar(50),
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
        
    
    FROM {{ source('ER_TIP_F', 'er_tip_f_' ~  year  ) }}
    WHERE tip_acl_dtd != 'tip_acl_dtd'
    AND tip_acl_dtf != 'tip_acl_dtf'

        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%}
)
 
SELECT * 
FROM union_er_tip_f_years
WHERE dcir_key_id IN (SELECT dcir_key_id FROM {{ ref('bse_er_prs_f') }})
