{{config(materialized='table')}}
    
WITH 
fix_tip_quantity_dates
AS 
(
    SELECT 
        dcir_visit_id,
        tip_prs_ide, 
        etb_pre_fin,
        exe_soi_dtd,
        tip_prs_typ,
    
        SUM(tip_act_qsn) AS quantity ,
        MAX(tip_acl_dtd) AS tip_acl_dtd,  
        MAX(tip_acl_dtf) AS tip_acl_dtf
    
    FROM {{ref('tip_prs__joined')}}  
    GROUP BY 1,2,3,4,5
)

SELECT 
    DISTINCT 
    dcir_visit_id,
    tip_prs_ide, 
    etb_pre_fin,
    tip_prs_typ,
    
    quantity ,
    COALESCE(tip_acl_dtd, exe_soi_dtd) AS tip_acl_dtd,  
    tip_acl_dtf 
FROM fix_tip_quantity_dates