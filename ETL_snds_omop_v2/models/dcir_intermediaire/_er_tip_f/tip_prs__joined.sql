{{config(materialized='table')}}
    
WITH 
tip_prs_joined
AS 
(
    SELECT 
        a.tip_prs_ide,
        a.tip_acl_dtd,
        a.tip_acl_dtf,
        a.tip_act_qsn,
        a.tip_prs_typ,
    
        b.exe_soi_dtd,
        b.etb_pre_fin,
        b.dcir_visit_id
    FROM 
        {{ref('stg_er_tip_f')}} a
        JOIN {{ref('bse_er_prs_f')}} b USING(dcir_key_id)
)

 
SELECT * FROM tip_prs_joined