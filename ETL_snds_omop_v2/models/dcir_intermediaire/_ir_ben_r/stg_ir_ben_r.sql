{{config(materialized='view')}}


SELECT 
    DISTINCT
   num_enq::varchar(100), -- correspond à ben_nir_psa
   num_enq_idt::varchar(100), --ajouté grâce à la table de cohorte
   ben_sex_cod::varchar(50),
   ben_nai_ann::integer,
   ben_nai_moi::integer,
   org_aff_ben::varchar(50),
   RIGHT('000' || ben_res_dpt::varchar(5), 3)::varchar(5) AS ben_res_dpt,
   RIGHT('000' || ben_res_com::varchar(5), 3)::varchar(5) AS ben_res_com,
   {{format_date('ben_dte_maj')}} AS ben_dte_maj,
   {{format_date('ben_dcd_dte')}} AS ben_dcd_dte
   
FROM {{source('beneficiaire','ir_ben_r')}}
WHERE ben_nai_ann::integer > 1600  
