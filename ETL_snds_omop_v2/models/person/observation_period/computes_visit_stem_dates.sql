{{ config(materialized='table', indexes = [{'columns' : ['person_id']}]) }}

WITH dates AS
(
    SELECT
        person_id,
        visit_start_date AS start_date,
        visit_end_date   AS end_date
    FROM 
        {{ref('visit_occurrence')}}
    UNION ALL
    SELECT 
        person_id,
        condition_start_date AS start_date,
        coalesce(condition_end_date, condition_start_date) AS end_date
    FROM 
        {{ref('condition_occurrence')}}
    UNION ALL
    SELECT 
        person_id,
        procedure_date AS start_date,
        procedure_date AS end_date
    FROM 
        {{ref('procedure_occurrence')}}
    UNION ALL
    SELECT 
        person_id,
        drug_exposure_start_date AS start_date,
        coalesce(drug_exposure_end_date, drug_exposure_start_date) AS end_date
    FROM 
        {{ref('drug_exposure')}}
    UNION ALL
    SELECT 
        person_id,
        device_exposure_start_date AS start_date,
        coalesce(device_exposure_end_date, device_exposure_start_date) AS end_date
    FROM 
        {{ref('device_exposure')}}
    UNION ALL
    SELECT 
        person_id,
        measurement_date AS start_date,
        measurement_date AS end_date
    FROM 
        {{ref('measurement')}}
    UNION ALL
    SELECT 
        person_id,
        observation_date AS start_date,
        observation_date AS end_date
    FROM 
        {{ref('observation')}}
        
)

SELECT * FROM dates 
