{{ config(materialized='view') }}


WITH
    add_num_enq AS( 
        SELECT  
            ki.dcd_idt_enc,
            ki.num_enq_idt,
            COALESCE(ki.ben_dcd_dte, ir.ben_dcd_dte) AS ben_dcd_dte,
            ki.dcd_cim_cod,

            ir.num_enq
    
        FROM {{ ref('ir_ben_r__deduplicated') }} ir

        LEFT JOIN {{ ref('stg__ki_cci_r') }} ki 
            ON ir.num_enq_idt = ki.num_enq_idt
        )

SELECT 
    * 
FROM add_num_enq
WHERE ben_dcd_dte > '1600-01-01'::date


