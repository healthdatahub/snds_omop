{{ config(materialized='view') }}

SELECT
    dcd_idt_enc::varchar(100),
    num_enq_idt::varchar(100) AS num_enq_idt,
    {{ format_date('ben_dcd_dte') }} AS ben_dcd_dte,
    dcd_cim_cod::varchar(50)

FROM {{source('KI', 'ki_cci_r')}}