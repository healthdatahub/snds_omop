{{ config(materialized='view') }}

-- Ce model sélectionne le sexe, la date de naissance, l'identifiant et le location_id d'une personne.

WITH join_location
AS
(
        SELECT
                ben_sex_cod,
                ben_nai_ann,
                ben_nai_moi,
                num_enq, -- ben_nir_psa
                location_id
        FROM
                {{ ref('ir_ben_r__deduplicated') }} a
                LEFT JOIN {{ ref('location') }} d ON a.code_commune = d.location_source_value
)

SELECT * FROM join_location 
