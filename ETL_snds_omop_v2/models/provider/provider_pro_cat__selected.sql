{{ config(materialized='view', schema='provider') }}

-- Ce modèle sélectionne les différentes catégories professionnelles des professionnels de santé executants  


SELECT 
    source_code             AS specialty_source_value,
    source_code             AS provider_source_value,
    target_concept_id       AS specialty_concept_id
FROM {{ source('vocabularies', 'source_to_concept_map')}} 
WHERE source_vocabulary_id = 'PRO_CAT'