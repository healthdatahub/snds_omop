{{ config(materialized='view', schema='provider') }}

-- Ce modèle sélectionne les différentes natures d'activités des professionnels de santé exécutants 


SELECT 
    source_code             AS specialty_source_value,
    source_code             AS provider_source_value,
    target_concept_id       AS specialty_concept_id
FROM {{ source('vocabularies', 'source_to_concept_map')}} 
WHERE source_vocabulary_id = 'IR_ACT_V'
