{{ config(materialized='table') }}

{%- set table_exist = namespace(value=TRUE) -%}
    {%- for year in var('years_pmsi') -%}
        {%- if year | int > 19 -%}
            {%- set table_exist.value = TRUE -%}
        {%-endif-%}
    {%-endfor-%}

{%- if table_exist.value -%}
    WITH 
        measurement_ssr_flstc AS(
    
            {% for year in var('years_pmsi') %}

                {% if year | int > 19 %}

                    {% set source_name = 'MCO' %}
                    {% set table_name = 't_ssr' ~ year ~ 'flstc' %}
                    {% set base = "'ssr'" | string %}

            {{ measurement_pmsi_source_value( 
                                            src_source    = source_name,
                                            src_table     = table_name,
                                            source_value  = 'nabm_cod',
                                            src_base      = base,
                                            src_finess_nb = 'eta_num',
                                            src_visit_nb  = 'seq_num') }}


                    {% if not loop.last -%}
                    UNION ALL 
                    {%- endif -%}
                {%-endif-%}
            {%- endfor -%} 

        )

SELECT * FROM measurement_ssr_flstc
{% else %}

    SELECT
        null          AS source_value,
        null          AS visit_occurrence_source_value

{% endif %}

    


