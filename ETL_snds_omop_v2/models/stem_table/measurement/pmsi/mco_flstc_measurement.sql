{{ config(materialized='view') }}

WITH measurement_mco_flstc AS(

    {% for year in var('years_pmsi') %}
        
        {% set source_name = 'MCO' %}
        {% set table_name = 't_mco' ~ year ~ 'flstc'%}
        {% set base = ("'" ~ 'mco' ~ year ~ 'ace' ~ "'") | string %}

        {{ measurement_pmsi_source_value( 
                                        src_source    = source_name,
                                        src_table     = table_name,
                                        source_value  = 'nabm_cod',
                                        src_base      = base,
                                        src_finess_nb = 'eta_num',
                                        src_visit_nb  = 'seq_num') }}


        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%} )


SELECT * FROM measurement_mco_flstc  
    