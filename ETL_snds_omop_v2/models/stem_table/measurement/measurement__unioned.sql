{{ config(materialized='table', schema='stem', alias = 'stem_table_measurement') }}

WITH device AS
(
    {{ dbt_utils.union_relations(relations=[
    ref('measurement_dcir'),
    ref('measurement_pmsi') ] ) }}
)

    
SELECT
    DISTINCT
    person_id      AS person_id,
    visit_occurrence_id     AS visit_occurrence_id,
    NULL::bigint            AS visit_detail_id,
    provider_id             AS provider_id,
    concept_id              AS concept_id,          -- CIM10 concept to complete 
    source_value            AS source_value,
    source_concept_id       AS source_concept_id,   -- CIM10 code, see source to source query
    32817                   AS type_concept_id,     -- updated using the new standard types
    start_date              AS start_date,
    (start_date||' 00:00:00')::timestamp                     AS start_datetime,
    GREATEST(end_date, start_date)                           AS end_date,
    (GREATEST(end_date, start_date)||' 00:00:00')::timestamp AS end_datetime,
    domain_id::varchar(20)  AS domain_id,       -- faux à voir une fois le mapping des codes CIM10 faits     
    NULL                    AS verbatim_end_date,
    NULL                    AS days_supply,
    NULL                    AS dose_unit_source_value,
    NULL                    AS lot_number,
    0                       AS modifier_concept_id,
    0                       AS modifier_source_value,       -- If any additional information
    0                       AS operator_concept_id,
    quantity                AS quantity,            
    NULL                    AS range_high,
    NULL                    AS range_low,
    NULL                    AS refills,
    0                       AS route_concept_id,
    NULL                    AS route_source_value,
    NULL                    AS sig,
    NULL                    AS stop_reason,
    NULL                    AS unique_device_id,
    0                       AS unit_concept_id,
    NULL                    AS unit_source_value,
    0                       AS value_as_concept_id,
    NULL                    AS value_as_number,
    NULL                    AS value_as_string,
    NULL                    AS value_source_value,
    0                       AS anatomic_site_concept_id,
    0                       AS disease_status_concept_id,
    NULL                    AS specimen_source_id,
    NULL                    AS anatomic_site_source_value,
    0                       AS status_concept_id,
    NULL                    AS status_source_value,
    NULL                    AS qualifier_concept_id,
    NULL                    AS qualifier_source_value
FROM
    device