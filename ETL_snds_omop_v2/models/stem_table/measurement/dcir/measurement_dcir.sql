{{ config(materialized='table') }}

WITH 
    dcir_measurement AS (
        SELECT *
        FROM {{ ref('dcir_measurements__selected') }} ),


    nabm_concepts AS (
        SELECT *
        FROM {{ ref('nabm_concepts') }} ),


    visit_occurrence_dcir AS (
        SELECT *
        FROM {{ ref('visit_occurrence_dcir') }}),


    dcir_join_visit_concept AS (   
        SELECT
            v.person_id::bigint                                AS person_id,
            v.visit_occurrence_id::bigint                      AS visit_occurrence_id,
            v.provider_id::bigint                              AS provider_id,
            v.visit_start_date::date                           AS start_date,
            v.visit_start_datetime::timestamp                  AS start_datetime,
            v.visit_end_date::date                             AS end_date,
            v.visit_end_datetime::timestamp                    AS end_datetime,
        
            dm.source_value::text                              AS source_value,
            dm.quantity::float                                 AS quantity, 

            COALESCE(nc.source_concept_id, 0)::integer         AS source_concept_id, -- voir dans fichier des concepts 
            COALESCE(nc.target_concept_id, 0)::integer         AS concept_id,
            COALESCE(nc.domain_id, 'Measurement')::varchar(20) AS domain_id

        
        FROM  dcir_measurement dm
        
            JOIN visit_occurrence_dcir v USING(visit_occurrence_source_value) 
        
            LEFT JOIN nabm_concepts nc USING(source_value))
                
                
                




SELECT * FROM dcir_join_visit_concept 
