{{ config(materialized='view') }} 
 
 
SELECT 
    SUBSTR(ucd_ucd_cod::text,7)                               AS source_value,
    {{ provider_source_value('psp_spe_cod', 'psp_act_nat') }} AS provider_source_value,
    pre_pre_dtd                                               AS start_date,
    (pre_pre_dtd + 29 )::date                                 AS end_date,    
    quantity                                                  AS quantity,
    
    {{ visit_occurrence_source_value( 
        src_base      = "'dcir'", 
        src_finess_nb = 'etb_pre_fin', 
        src_visit_nb  = 'dcir_visit_id') }} AS visit_occurrence_source_value
    
FROM {{ ref('ucd_quantity__grouped') }} 