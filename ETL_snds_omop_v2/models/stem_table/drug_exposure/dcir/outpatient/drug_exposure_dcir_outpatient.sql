{{ config(materialized='view') }}

WITH 
    dcir_outpatient_drug AS(
        SELECT *
        FROM {{ ref('dcir_outpatient_drugs__selected') }} ),

    cip7 AS (
        SELECT *
        FROM {{ ref('cip13_cip7') }}),
    
    visit_occurrence_dcir AS(
        SELECT *
        FROM {{ ref('visit_occurrence_dcir') }} ),
        
    provider AS(
        SELECT *
        FROM {{ ref('provider') }} ),

    join_visit AS (
        SELECT 
            v.person_id                        AS person_id, 
            v.visit_occurrence_id              AS visit_occurrence_id,
        
            p.provider_id                      AS provider_id,
        
            COALESCE(cip.concept_id, 0)        AS concept_id,
            COALESCE(cip.source_concept_id, 0) AS source_concept_id,
            COALESCE(cip.domain_id, 'Drug')    AS domain_id,
        
            dod.source_value                   AS source_value,
            dod.start_date                     AS start_date,
            dod.end_date                       AS end_date,
            dod.quantity                       AS quantity,
            dod.dose_unit_source_value         AS dose_unit_source_value,
        
            30                                 AS days_supply
        
        FROM dcir_outpatient_drug dod
        
            JOIN visit_occurrence_dcir v USING(visit_occurrence_source_value)

            LEFT JOIN provider p USING(provider_source_value)

            LEFT JOIN cip7 cip USING(source_value) )
                
SELECT * FROM join_visit                
