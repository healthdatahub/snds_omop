{{ config(materialized='view') }}


SELECT 
    cu.codeucd13::text AS source_value,

    ath.concept_code,
    ath.concept_id_1    AS source_concept_id,
    ath.concept_id_2    AS concept_id, 
    ath.domain_id

FROM 
    {{ source('correspondance', 'cip_ucd') }} cu

LEFT JOIN {{ ref('athena_mapping') }} ath 
    ON TRIM(codecip::text) = concept_code 
    
    