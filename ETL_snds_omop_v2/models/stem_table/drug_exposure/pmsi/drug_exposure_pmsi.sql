WITH 
    drug_pmsi__unioned AS(
        {{ dbt_utils.union_relations(
            relations=[
                ref('mco_fhstc_drug'),
                ref('mco_med_drug'),
                ref('mco_medatu_drug'),
                ref('mco_medthrombo_drug'),
                
                ref('had_med_drug'),
                ref('had_medatu_drug'),
                ref('had_medchl_drug'),
                
                ref('ssr_med_c__joined_drug'),
                ref('ssr_medatu_c__joined_drug') ] ) }} ),
                
                
    drug_pmsi_visit_occurrence_source_value AS(
        SELECT 
            source_value,
            quantity,
            delai,
            {{ visit_occurrence_source_value(src_base = 'base',
                                            src_finess_nb = 'finess_nb',
                                            src_visit_nb = 'visit_nb',
                                            src_start_date = 'exe_soi_dtd') }} AS visit_occurrence_source_value
                                            
        FROM drug_pmsi__unioned
         ),
                

    cip7 AS (
        {{ dbt_utils.union_relations(
            relations=[
                ref('ucd7_cip7'),
                ref('ucd13_cip7') ] ) }} ),       
        
    visit_occurrence_pmsi AS (
        SELECT *
        FROM {{ ref('visit_occurrence_pmsi') }} ),


    join_visit AS (
        SELECT           
            v.person_id                                            AS person_id, 
            v.visit_occurrence_id                                  AS visit_occurrence_id,
            NULL                                                   AS provider_id,
            (v.visit_start_date::date + dp.delai::integer)         AS end_date,
        
            COALESCE(cip.concept_id,0)                             AS concept_id,
            COALESCE(cip.source_concept_id)                        AS source_concept_id,
            COALESCE(cip.domain_id, 'Drug')                        AS domain_id,
            dp.visit_occurrence_source_value                       AS visit_occurrence_source_value,
        
            dp.source_value                                        AS source_value,
            dp.quantity                                            AS quantity,

            (v.visit_start_date::date + dp.delai::integer)         AS start_date, 
        
            {{ date_diff(
                start_date = 'v.visit_start_date', 
                end_date   = 'v.visit_end_date') }} + 1 - dp.delai AS days_supply,
            
            NULL                                                   AS dose_unit_source_value
        
        
        FROM  drug_pmsi_visit_occurrence_source_value dp
        
            JOIN visit_occurrence_pmsi v USING(visit_occurrence_source_value)
            
            LEFT JOIN cip7 cip USING(source_value) ),
            
            

    total_quantity AS (
        SELECT
            person_id,
            visit_occurrence_id, 
            concept_id,
            source_concept_id,
            domain_id,
            provider_id,
            source_value,
            start_date,
            SUM(quantity) AS quantity,
            days_supply,
            end_date,
            dose_unit_source_value,
            visit_occurrence_source_value

        FROM 
            join_visit

        GROUP BY 1,2,3,4,5,6,7,8,10,11,12,13 )
        
SELECT * FROM total_quantity




