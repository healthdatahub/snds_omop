{{ config(materialized='view') }}

WITH drug_exposure_mco_medatu AS(

    {% for year in var('years_pmsi') %}

        {% set source_name = 'MCO' %}
        {% set table_name = 't_mco' ~ year ~ 'medatu'%}
        {% set base = ("'" ~ 'mco' ~ year ~ "'") | string %}

        {{ drug_pmsi(
                src_source    = source_name,
                src_table     = table_name,
                src_base      = base, 
                src_finess_nb = 'eta_num', 
                src_visit_nb  = 'rsa_num', 
                dat_delai     = 'dat_delai' ) }} -- Changé (avnat : délai)


        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%} )


SELECT * FROM drug_exposure_mco_medatu


