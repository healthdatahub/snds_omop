{{ config(materialized='view') }}

WITH 
   union_years_ssr_med AS(

        {% for year in var('years_pmsi') %}
                {% set source_name = 'SSR' %}
                {% set table_name = 't_ssr' ~ year ~ 'med'%}
                {% set base = ("'" ~ 'ssr' ~ year ~ "'") | string %}

                {{ drug_pmsi(
                        src_source    = source_name,
                        src_table     = table_name , 
                        src_base      = base, 
                        src_finess_nb = 'eta_num', 
                        src_visit_nb  = 'rha_num') }}


                {% if not loop.last -%}
                UNION ALL 
                {%- endif -%}

        {%- endfor -%} ),
        
        
    ssr_c AS (
        SELECT
            base,
            finess_j AS finess_nb,
            visit_nb,
            exe_soi_dtd
        FROM {{ ref('stg__t_ssraac_visit') }}),
        
        
    ssr_c_med AS (
        SELECT 
            med.*,
            c.exe_soi_dtd
    
        FROM union_years_ssr_med med 
        JOIN ssr_c c USING(base, finess_nb, visit_nb))

SELECT * FROM ssr_c_med
