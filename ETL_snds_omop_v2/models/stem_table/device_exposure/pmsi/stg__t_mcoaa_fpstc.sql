{{ config(materialized='view') }}

WITH device_mco_fpstc AS (

    {% for year in var('years_pmsi') %}
    
        {% set source_name = 'MCO' %}
        {% set table_name = 't_mco' ~ year ~ 'fpstc'%}
        {% set base = ("'" ~ 'mco' ~ year ~ 'ace' ~ "'") | string %}

        {{ device_exposure_pmsi(
            source_name   = source_name,
            source_value  = 'lpp_cod', 
            quantity      = 'lpp_qua', 
            delai         = 'del_dat_ent', 
            src_base      = base, 
            src_finess_nb = 'eta_num', 
            src_visit_nb  = 'seq_num', 
            src_table     = table_name) }}

        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%}   )

SELECT * FROM device_mco_fpstc
