{{ config(materialized='view') }}



SELECT 
    quantity,
    tip_acl_dtd       AS start_date,  
    
    CASE
        WHEN tip_prs_typ = '1' --purchase
            THEN NULL::date --end date of data in the database
        ELSE tip_acl_dtf 
    END  end_date, 
    
    tip_prs_ide::text AS source_value, 

    {{ visit_occurrence_source_value(
            src_base="'dcir'", 
            src_finess_nb='etb_pre_fin', 
            src_visit_nb='dcir_visit_id' ) }} AS visit_occurrence_source_value

FROM {{ ref('tip_quantity_dates__grouped') }} 