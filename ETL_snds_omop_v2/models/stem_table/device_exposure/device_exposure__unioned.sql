{{ config(materialized='table', schema='stem', alias = 'stem_table_device') }}

WITH 
    all_device AS (
        {{ dbt_utils.union_relations(
            relations=[
                ref('device_dcir'),
                ref('device_pmsi') ] ) }} )

SELECT   
    DISTINCT 
    domain_id::varchar(20)               AS domain_id,           
    person_id::bigint                    AS person_id,
    visit_occurrence_id::bigint          AS visit_occurrence_id,
    NULL::bigint                         AS visit_detail_id,
    provider_id::bigint                  AS provider_id,
    concept_id::integer                  AS concept_id,          -- LPP concept to complete 
    source_value::text                   AS source_value,
    source_concept_id::integer           AS source_concept_id,   -- LPP code, see source to source query
    32810::integer                       AS type_concept_id,     
    start_date::date                     AS start_date,
    (start_date||' 00:00:00')::timestamp AS start_datetime,
    end_date::date                       AS end_date,
    (end_date||' 00:00:00')::timestamp   AS end_datetime,
    NULL::date                           AS verbatim_end_date,
    NULL::integer                        AS days_supply,
    NULL::varchar(50)                    AS dose_unit_source_value,
    NULL::varchar(50)                    AS lot_number,
    0::integer                           AS modifier_concept_id,
    0::varchar(50)                       AS modifier_source_value,       -- If any additional information
    0::integer                           AS operator_concept_id,
    quantity::float                      AS quantity,            
    NULL::float                          AS range_high,
    NULL::float                          AS range_low,
    NULL::integer                        AS refills,
    0::integer                           AS route_concept_id,
    NULL::varchar(50)                    AS route_source_value,
    NULL::varchar(1000)                  AS sig,
    NULL::varchar(20)                    AS stop_reason,
    NULL::varchar(50)                    AS unique_device_id,
    0::integer                           AS unit_concept_id,
    NULL::varchar(50)                    AS unit_source_value,
    0::integer                           AS value_as_concept_id,
    NULL::float                          AS value_as_number,
    NULL::varchar(60)                    AS value_as_string,
    NULL::varchar(50)                    AS value_source_value,
    0::integer                           AS anatomic_site_concept_id,
    0::integer                           AS disease_status_concept_id,
    NULL::integer                        AS specimen_source_id,
    NULL::varchar(50)                    AS anatomic_site_source_value,
    0::integer                           AS status_concept_id,
    NULL::varchar(50)                    AS status_source_value,
    0::integer                           AS qualifier_concept_id,
    NULL::varchar(50)                    AS qualifier_source_value  

FROM
    all_device