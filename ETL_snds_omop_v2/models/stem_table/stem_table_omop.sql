{{config(materialized='table', schema='stem_table', indexes = [{'columns' : ['domain_id']}])}}

WITH stem_table AS
(
{{dbt_utils.union_relations(
    relations=[
        ref('condition_occurrence_diag__unioned'), 
        ref('ir_imb_r_fk_omop__joined'),
        ref('device_exposure__unioned'), 
        ref('drug_exposure__unioned'),
        ref('measurement__unioned'),
        ref('procedure_occurrence__unioned')
    ]
)
}}
)

SELECT 
    DISTINCT
    {{set_id(var('stem_table_key'))}} AS id,
    *
FROM stem_table 


