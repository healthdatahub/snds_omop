{{ config(materialized='view') }}


WITH 
    union_years_condition_dgn_ass_had AS (

        {% for year in var('years_pmsi') %}
        
            {% set source_name = 'HAD' %}
            {% set table_name = 't_had' ~ year ~ 'd'%}
            {% set base = ("'" ~ 'had' ~ year ~ "'") | string %}

                {{ condition_source_values(
                    src_source = source_name,
                    src_table = table_name,
                    source_values = ['dgn_ass'],
                    src_base="'had19_09'",
                    src_finess_nb='eta_num_epmsi',
                    src_visit_nb='rhad_num',
                    src_sej_idt='rhad_num'
                )
                }}

            {% if not loop.last -%}
            UNION ALL 
            {%- endif -%}

        {%- endfor -%}      )


SELECT * FROM union_years_condition_dgn_ass_had