WITH
visits AS
(
    SELECT
        person_id,
        visit_occurrence_id,
        provider_id,
        visit_start_date,
        visit_end_date,
        visit_occurrence_source_value
    FROM {{ref('visit_occurrence_dcir')}}
),

join_visit_concept AS
(   
    SELECT
        DISTINCT
        person_id                AS person_id,
        visit_occurrence_id      AS visit_occurrence_id,
        provider_id              AS provider_id,
        source_value             AS source_value,
        coalesce(source_concept_id,0)::integer AS source_concept_id, -- voir dans fichier des concepts 
        coalesce(target_concept_id,0)::integer AS concept_id,
        coalesce(domain_id, 'Procedure')       AS domain_id,   
        visit_start_date        AS start_date,
        visit_end_date          AS end_date,
        quantity                AS quantity 
    FROM 
        {{ref('procedure_dcir_quantity__selected')}} pro
        JOIN visits USING(visit_occurrence_source_value) -- On prend uniquement les prestations qui sont remontée dans visit_occurrence via le DCIR
        LEFT JOIN {{ref('stg_source_to_concept_map__ccam_csarr')}} USING(source_value)
)



SELECT * FROM join_visit_concept 