WITH total_quantity AS

(
    SELECT 
        person_id,
        visit_occurrence_id,
        visit_detail_id,
        provider_id,
        source_value,
        source_concept_id, 
        concept_id,
        domain_id, 
        start_date,
        end_date,
    
        SUM(quantity)         AS quantity  
    FROM {{ref('procedure_pmsi_visit_concept__joined')}}
    GROUP BY 1,2,3,4,5,6,7,8,9,10

)

SELECT * FROM total_quantity