{{ config(materialized='view') }}

WITH procedure_t_mcoaafmstc AS(

    {% for year in var('years_pmsi') %}

        {% set source_name = 'MCO' %}
        {% set table_name = 't_mco' ~ year ~ 'fmstc'%}
        {% set base = ("'" ~ 'mco' ~ year ~ 'ace' ~ "'") | string %}
        
        {{ procedure_pmsi_source_value(
            src_source = source_name,
            src_table = table_name, 
            source_value = 'ccam_cod', 
            src_base = base, 
            src_finess_nb = 'eta_num', 
            src_visit_nb = 'seq_num',
            src_sej_idt = 'seq_num',
            phase = 'pha_act',
            delai = 'del_dat_ent') }}

        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%}
)

SELECT * FROM procedure_t_mcoaafmstc