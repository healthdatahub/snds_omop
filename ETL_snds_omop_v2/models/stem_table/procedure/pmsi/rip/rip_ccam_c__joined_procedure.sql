{{ config(materialized='table') }}

{%- set table_exist = namespace(value=TRUE) -%}
    {%- for year in var('years_pmsi') -%}
        {%- if year | int > 16 -%}
            {%- set table_exist.value = TRUE -%}
        {%-endif-%}
    {%-endfor-%}

{%- if table_exist.value -%}

    WITH 
    
        union_years_rip_ccam AS (

        {%- for year in var('years_pmsi') -%}
            {%- if year | int > 16 -%}
            
                {%- set source_name = 'RIP' -%}
                {%- set table_name = 't_rip' ~ year ~ 'ccam' -%}
                {%- set base = ("'"~'rip' ~ year ~ "'") -%}
        
            {{ procedure_pmsi_source_value(
                src_source = source_name,
                src_table = table_name, 
                source_value = 'cdc_act', 
                src_base = base, 
                src_finess_nb = 'eta_num_epmsi', 
                src_visit_nb = 'rip_num',
                src_sej_idt = 'sej_idt',
                phase = 'pha_act',
                quantity = 'nbr_exe_act',
                delai = 'ent_dat_del' ) }}
        
                {% if not loop.last %}
                UNION ALL 
                {%- endif -%}
    
            {%-endif-%}   
        {%endfor%}
    ),
 
     rip_c AS (
        SELECT
            base,
            finess_j AS finess_nb,
            visit_nb,
            exe_soi_dtd
        FROM {{ ref('stg__t_ripaac_visit') }}),
        
        
    rip_ccam_c AS (
        SELECT 
            ccam.*,
            c.exe_soi_dtd
    
        FROM union_years_rip_ccam ccam
        JOIN rip_c c USING(base, finess_nb, visit_nb))
        
SELECT * FROM rip_ccam_c
    
    
    {% else %} -- La table n'existe pas 
    
    SELECT 
        0::text    AS source_value,
        0::integer AS quantity,
        0::text    AS base,
        0          AS finess_nb,
        0          AS visit_nb,
        0::integer AS delai,
        0          AS exe_soi_dtd
 

        
       
    {% endif %}
    
