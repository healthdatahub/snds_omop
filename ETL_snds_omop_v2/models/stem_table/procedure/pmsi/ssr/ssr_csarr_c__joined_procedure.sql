{{ config(materialized='view') }}

WITH 
    union_years_ssr_csarr AS (

    {% for year in var('years_pmsi') %}

        {% set source_name = 'SSR' %}
        {% set table_name = 't_ssr' ~ year ~ 'csarr'%}
        {% set base = "'ssr'" | string %}
        
        {% if year < 20 %}
        
        {% set delai = 'ent_dat_del_um' %}
        
        {% else %}
        
        {% set delai = 'del_dat_ent' %}
        
        {% endif %}

        {{ procedure_pmsi_source_value(
            src_source = source_name,
            src_table = table_name, 
            source_value = 'csarr_cod', 
            src_base = base, 
            src_finess_nb = 'eta_num', 
            src_visit_nb = 'rha_num', 
            src_sej_idt = 'rha_num',
            phase = None,
            delai = delai) }}


        {% if not loop.last -%}
        UNION ALL 
        {%- endif -%}

    {%- endfor -%}
),

    ssr_c AS (
        SELECT
            base,
            finess_j AS finess_nb,
            visit_nb,
            exe_soi_dtd
        FROM {{ ref('stg__t_ssraac_visit') }}),
        
        
    ssr_csarr_c AS (
        SELECT 
            csarr.*,
            c.exe_soi_dtd
    
        FROM union_years_ssr_csarr csarr
        JOIN ssr_c c USING(base, finess_nb, visit_nb))
        
SELECT * FROM ssr_csarr_c
