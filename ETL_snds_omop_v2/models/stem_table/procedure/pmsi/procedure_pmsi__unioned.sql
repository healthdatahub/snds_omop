WITH 
    pmsi_procedure AS (
        {{ procedure_visit(src_table = ref('mco_a_procedure') ) }}
        
        UNION ALL

        {{ procedure_visit(src_table = ref('ssr_ccam_c__joined_procedure'),
                           exe_soi_dtd = 'exe_soi_dtd' ) }}
        
         UNION ALL

        {{ procedure_visit(src_table = ref('ssr_csarr_c__joined_procedure'),
                           exe_soi_dtd = 'exe_soi_dtd' ) }}
        
         UNION ALL

        {{ procedure_visit(src_table = ref('ssr_fmstc_procedure')) }}
        
         UNION ALL

        {{ procedure_visit(src_table = ref('had_a_procedure') ) }}
        
         UNION ALL

        {{ procedure_visit(src_table = ref('rip_ccam_c__joined_procedure'),
                           exe_soi_dtd = 'exe_soi_dtd' ) }}
        
        )

SELECT * FROM pmsi_procedure
WHERE source_value <> '0' -- Dans le cas où la table n'existe pas, toutes les variables valent 0



