{{ config(materialized='view') }}

WITH 
    visits AS (
        SELECT 
            person_id,
            visit_occurrence_id,
            provider_id,
            visit_start_date,
            visit_end_date,
            visit_occurrence_source_value
        FROM {{ ref('visit_occurrence_pmsi') }} ),
        
        
    visit_detail AS(
        SELECT
            visit_occurrence_id,
            visit_detail_id,
            visit_detail_start_date,
            visit_detail_end_date
        FROM {{ ref('visit_detail') }} ),
        

    join_visit_concept AS (   
        SELECT
        
            pro.quantity                              AS quantity,
            pro.source_value                          AS source_value,
        
            vis.person_id                             AS person_id,
            vis.visit_occurrence_id                   AS visit_occurrence_id,
            vis.provider_id                           AS provider_id,
            vis.visit_start_date + pro.delai::integer AS start_date,
            GREATEST(vis.visit_end_date, vis.visit_start_date + pro.delai::integer) AS end_date,
               
            COALESCE(con.source_concept_id,0)         AS source_concept_id, 
            COALESCE(con.target_concept_id,0)         AS concept_id,
            COALESCE(con.domain_id, 'Procedure')      AS domain_id 
                        
        FROM 
            {{ ref('procedure_pmsi__unioned') }} pro
            JOIN visits vis USING(visit_occurrence_source_value)
            LEFT JOIN {{ ref('stg_source_to_concept_map__ccam_csarr') }} con USING(source_value) ),
            
            
    join_visit_detail AS(
        SELECT DISTINCT
            pro.quantity,
            pro.source_value,
            pro.person_id,
            pro.visit_occurrence_id,
            pro.provider_id,
            pro.start_date,
            pro.end_date,
            pro.source_concept_id,
            pro.concept_id,
            pro.domain_id,
    
            vd.visit_detail_id
    
        FROM join_visit_concept pro
        LEFT JOIN visit_detail vd
        ON pro.visit_occurrence_id = vd.visit_occurrence_id
        AND pro.start_date >= vd.visit_detail_start_date
        AND pro.end_date <= vd.visit_detail_end_date)


SELECT * FROM join_visit_detail