{{ config(materialized='table') }}

-- Ce modèle sélectionne tous les concepts concept_id_1 (concept_id associés aux codes sources SNDS) et concept_id_2 (concept_id associés aux codes cibles OMOP) de la table concept_relation_ship. 
-- Il récupère le domain associé au concept_id_2 (domaine cible) en joignant la table concept_relation_ship à la table concept. (concept_id = concept_id_2)
-- Il récupère le code source (SNDS) associé au concept_id_1 en joignant la table concept_relation_ship à la table concept. (concept_id = concept_id_1), ainsi que le vocabulaire du code source


WITH 
concept_mapping AS
-- on récupère la correspondance et le domain_id du code cible 
(
    SELECT 
        c1.concept_id_1, --source
        c1.concept_id_2, --target
        c.domain_id --target
    FROM {{ source('vocabularies', 'concept_relationship'  ) }} c1
    INNER JOIN {{ source('vocabularies', 'concept'  ) }} c 
        ON c.concept_id = c1.concept_id_2
    WHERE relationship_id = 'Maps to' --AND concept_id_1 != concept_id_2 
),

-- on récupère le code original  

concepts AS 
(
    SELECT
        c1.concept_code , --source
        c.concept_id_1, --source
        c.concept_id_2, --target
        c.domain_id, --target domain
        c1.vocabulary_id
    FROM concept_mapping c
    INNER JOIN {{ source('vocabularies', 'concept'  ) }} c1 ON concept_id = c.concept_id_1            
)

SELECT * FROM concepts 