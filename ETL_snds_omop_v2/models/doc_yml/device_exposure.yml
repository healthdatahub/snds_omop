version: 2


# └── device_exposure
#     ├── pmsi
#     │   ├── stg__t_mcoaa_dmip_device.sql
#     │   ├── stg__t_mcoaa_fpstc_device.sql
#     │   └── device_pmsi.sql
#     ├── dcir
#     │   ├── dcir_device__selected.sql
#     │   └── device_dcir.sql
#     ├── lpp_concepts.sql
#     └── device_exposure.sql

models:

### DCIR ###
  - name: dcir_device__selected
    description: Ce modèle corrige la date de fin dans le cas d'un achat, et calcule la variable visit_source_value à partir des variables du modèle tip_quantity_dates__grouped. 
    columns:
      - name: quantity
        description: Nombre de dispositifs médicaux achetés, loués ou réparés
      - name: start_date
        description: Date d'achat, de location ou de réparation
      - name: end_date
        description: Date de fin de location
      - name: source_value
        description: Code LPP du dispositif médical
      - name: visit_source_value
        description: Identifiant d'une visite dans OMOP

  - name: device_dcir
    description: Ce modèle joint le modèle dcir_device__selected (dispositifs médicaux) au modèle lpp_concept (correspondances entre les codes lpp et les codes SNOMED) et au modèle visit_occurrence_dcir (visites issues du SNDS) et sélectionne les variables pertienentes de la table DEVICE_EXPOSURE
    columns: 
      - name: person_id
        description: Identifiant unique d'une personne
        tests:
          - not_null
          - relationships:
              to: ref('person')
              field: person_id

      - name: visit_occurrence_id
        description: Identifiant unique d'une visite
        tests:
          - not_null
          - relationships:
              to: ref('visit_occurrence')
              field: visit_occurrence_id

      - name: provider_id
        description: Identifiant unique du professionnel de santé
        tests:
          - relationships:
              to: ref('provider')
              field: provider_id

      - name: start_date
        description: Date d'achat, de location ou de réparation
        tests:
          - not_null

      - name: end_date 
        description: Date de fin de location

      - name: source_value
        description: Code LPP du dispositif médical

      - name: quantity
        description: Nombre de dispositifs médicaux

      - name: source_concept_id
        description: Concept associé au code source LPP

      - name: concept_id
        description: Concept standard du dispositif médical
        tests:
          - not_null  
          - relationships:
              to: ref('concept')
              field: concept_id

      - name: domain_id
        description: Domaine du concept standard


### PMSI ###
  - name: stg__t_mcoaa_dmip
    description: Ce modèle sélectionne les variables de la table T_MCOaaDMIP qui alimenteront la table DEVICE_EXPOSURE et calcule le visit_source_value associé à la pose ou l'achat du dispositif médical
    columns: 
      - name: source_value
        description: Code LPP du dispositif médical
      - name: delai
        description: Nombre de jours écoulés entre le début de la visite et la pose ou l'achat du dispositif
      - name: quantity
        description: Nombre de dispositifs
      - name: visit_source_value
        description: Identifiant de la visite pendant laquelle à eu lieu la pose ou l'achat du dispositif

  - name: stg__t_mcoaa_fpstc
    description: Ce modèle sélectionne les variables de la table T_MCOaaFPSTC qui alimenteront la table DEVICE_EXPOSURE et calcule le visit_source_value associé à la pose ou l'achat du dispositif médical
    columns: 
      - name: source_value
        description: Code LPP du dispositif médical
      - name: delai
        description: Nombre de jours écoulés entre le début de la visite et la pose ou l'achat du dispositif
      - name: quantity
        description: Nombre de dispositifs
      - name: visit_source_value
        description: Identifiant de la visite pendant laquelle à eu lieu la pose ou l'achat du dispositif


  - name: device_pmsi
    description: Ce modèle unit les modèles stg__t_mcoaa_dmip et stg__t_mcoaafpstc (dispositifs médicaux). Il effectue ensuite une jointure sur le modèle lpp_concepts pour récupérer le concept standard correspondant au code source LPP, il effectue également une jointure avec la table VISIT_OCCURRENCE pour récupérer les informations relatives à la visite associée à la pose du dispositif médical. 
    columns: 
      - name: person_id
        description: Identifiant unique d'une personne
        tests:
          - not_null
          - relationships:
              to: ref('person')
              field: person_id

      - name: visit_occurrence_id
        description: Identifiant unique d'une visite
        tests:
          - not_null
          - relationships:
              to: ref('visit_occurrence')
              field: visit_occurrence_id

      - name: provider_id
        description: Identifiant unique du professionnel de santé
        tests:
          - relationships:
              to: ref('provider')
              field: provider_id

      - name: start_date
        description: Date d'achat, de location ou de réparation
        tests:
          - not_null

      - name: end_date 

      - name: source_value
        description: Code LPP du dispositif médical

      - name: quantity
        description: Nombre de dispositifs médicaux

      - name: source_concept_id
        description: Concept associé au code source LPP

      - name: concept_id
        description: Concept standard du dispositif médical
        tests:
          - not_null  
          - relationships:
              to: ref('concept')
              field: concept_id

      - name: domain_id
        description: Domaine du concept standard

### Commun ###
  - name: lpp_concepts
    description: Ce modèle sélectionne les lignes de la table source_to_concept_map relatives à des codes LPP et effectue une joiture sur la table CONCEPT pour récupérer le domaine associé au code SNOMED. 
    columns: 
      - name: source_value
        description: Code LPP du dispositif médical
      - name: source_concept_id
        description: Concept associé au code LPP
      - name: target_concept_id
        description: Concept standard SNOMED du dispositif médical
      - name: domain_id
        description: Domaine du concept standard


  - name: device_exposure__unioned
    description: Ce modèle unit les modèles device_dcir et device_pmsi (modèles qui rescenssent les dispositifs médicaux provenant du PMSI et DCIR) et sélectionne les variables de DEVICE_EXPOSURE.
    columns: 
      - name: person_id
        description: Identifiant unique d'une personne
        tests:
          - not_null
          - relationships:
              to: ref('person')
              field: person_id

      - name: visit_occurrence_id
        description: Identifiant unique d'une visite
        tests:
          - not_null
          - relationships:
              to: ref('visit_occurrence')
              field: visit_occurrence_id

      - name: provider_id
        description: Identifiant unique du professionnel de santé
        tests:
          - relationships:
              to: ref('provider')
              field: provider_id

      - name: start_date
        description: Date d'achat, de location ou de réparation
        tests:
          - not_null

      - name: end_date 
        description: Date de fin de location

      - name: source_value
        description: Code LPP du dispositif médical

      - name: quantity
        description: Nombre de dispositifs médicaux

      - name: source_concept_id
        description: Concept associé au code source LPP

      - name: concept_id
        description: Concept standard du dispositif médical
        tests:
          - not_null  
          - relationships:
              to: ref('concept')
              field: concept_id

      - name: domain_id
        description: Domaine du concept standard 

      - name: type_concept_id
        description: Type d'enregistrement



                