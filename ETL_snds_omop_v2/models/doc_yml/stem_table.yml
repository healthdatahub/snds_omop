version: 2

models:
### CONDITION ###      
    - name: condition_occurrence
      description: Table OMOP CONDITION_OCCURRENCE
      columns:
          - name: condition_occurrence_id
            description: Identifiant unique d'une condition pour un patient
            tests:
                - unique
                - not_null

          - name: person_id
            description: Identifiant unique d'une personne
            tests:
                - not_null
                - relationships :
                    to : ref('person')
                    field : person_id

          - name: condition_concept_id
            description: Concept standard d'une condition
            tests:
                - not_null
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id
                    
          - name: condition_start_date
            description: Date de début d'une condition
            tests:
                - not_null
                
          - name: condition_end_date
            description: Date de fin d'une condition
            tests:
                - not_null

          - name: condition_type_concept_id
            description: Type de provenance des données
            tests:
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id

          - name: condition_status_concept_id
            description: Moment de la visite où le diagnostic a été posé
            tests:
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id

          - name: provider_id
            description: Praticien qui a enregistré le diagnostic
            tests:
                - relationships :
                    to : ref('provider')
                    field : provider_id
                    
          - name: visit_occurrence_id
            description: Identifiant unique de la visite pendant laquelle a été posé le diagnostic
            tests:
                - relationships :
                    to : ref('visit_occurrence')
                    field : visit_occurrence_id

          - name: visit_detail_id
            description: Identifiant unique de la visite détaillée pendant laquelle a été posé le diagnostic
            tests:
                - relationships :
                    to : ref('visit_detail')
                    field : visit_detail_id

          - name: condition_source_value
            description: Code d'un diagnostic dans le SNDS

          - name: consition_source_concept_id
            description: Concept associé au code source du diagnostic

          - name: condition_status_source_value
            description: Type de diagnostic dans le SNDS (principal, relié, associé)


### DEVICE ###                     
    - name: device_exposure
      description: Table OMOP DEVICE_EXPOSURE
      columns:
          - name: device_exposure_id
            description: Identifiant unique d'une exposition à un dispositif pour un patient
            tests:
                - unique
                - not_null

          - name: person_id
            description: Identifiant unique d'une personne
            tests:
                - not_null
                - relationships :
                    to : ref('person')
                    field : person_id

          - name: device_concept_id
            description: Concept standard d'un dispositif médical
            tests:
                - not_null
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id
                    
          - name: device_exposure_start_date
            description: Date de début d'exposition à un dispositif médical
            tests:
                - not_null

          - name: device_type_concept_id
            description: Type de provenance des données
            tests:
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id

          - name: provider_id
            description: Praticien qui a prescrit ou mis en place le dispositif
            tests:
                - relationships :
                    to : ref('provider')
                    field : provider_id
                    
          - name: visit_occurrence_id
            description: Identifiant de la visite pendant laquelle le dispositif a été prescrit ou mis en place
            tests:
                - relationships :
                    to : ref('visit_occurrence')
                    field : visit_occurrence_id

          - name: visit_detail_id
            description: Identifiant de la visite détaillée pendant laquelle le dispositif a été prescrit ou mis en place
            tests:
                - relationships :
                    to : ref('visit_detail')
                    field : visit_detail_id

          - name: quantity
            description: Nombre de dispositifs prescrits ou mis en place

          - name: device_source_value
            description: Code source LPP du dispositif

          - name: device_source_concept_id
            description: Concept associé au code source du dispositif

          - name: device_exposure_end_date
            description: Date de fin de location du dispositif (si loué)

### DRUG ###
    - name: drug_exposure
      description: "Table OMOP DRUG_EXPOSURE"
      columns:
          - name: drug_exposure_id
            description: Identifiant unique d'une prise de médicament pour un patient
            tests:
                - unique
                - not_null

          - name: person_id
            description: Identifiant unique d'une personne
            tests:
                - not_null
                - relationships :
                    to : ref('person')
                    field : person_id

          - name: drug_concept_id
            description: Concept standard d'un médicament
            tests:
                - not_null
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id
                    
          - name: drug_exposure_start_date
            description: Date de début de prise d'un médicament
            tests:
                - not_null
                
          - name: drug_exposure_end_date
            description: Date de fin de prise d'un médicament
            tests:
                - not_null

          - name: drug_type_concept_id
            description: Type de provenance d'un médicament
            tests:
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id

          - name: provider_id
            description: Praticien qui a prescrit ou administré le médicament
            tests:
                - relationships :
                    to : ref('provider')
                    field : provider_id
                    
          - name: visit_occurrence_id
            description: Identifiant de la visite pendant laquelle le praticien a prescrit ou administré le médicament
            tests:
                - relationships :
                    to : ref('visit_occurrence')
                    field : visit_occurrence_id

          - name: visit_detail_id
            description: Identifiant de la visite détaillée pendant laquelle le praticien a prescrit ou administré le médicament
            tests:
                - relationships :
                    to : ref('visit_detail')
                    field : visit_detail_id
                    

### PROCEDURE ###
    - name: procedure_occurrence
      description: Table OMOP PROCEDURE_OCCURRENCE
      columns:
          - name: procedure_occurrence_id
            description: Identifiant unique d'une procedure pour un patient
            tests:
                - unique
                - not_null

          - name: person_id
            description: Identifiant d'une personne
            tests:
                - not_null
                - relationships :
                    to : ref('person')
                    field : person_id

          - name: procedure_concept_id
            description: Concept standard d'une procédure
            tests:
                - not_null
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id
                    
          - name: procedure_date
            description: Date de la procédure
            tests:
                - not_null

          - name: procedure_type_concept_id
            description: Type de provenance des données
            tests:
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id


          - name: provider_id
            description: Praticien qui a effectué la procédure
            tests:
                - relationships :
                    to : ref('provider')
                    field : provider_id
                    
          - name: visit_occurrence_id
            description: Identifiant de la visite pendant laquelle la procédure a été effectuée
            tests:
                - relationships :
                    to : ref('visit_occurrence')
                    field : visit_occurrence_id

          - name: visit_detail_id
            description: Identifiant de la visité détaillée pendant laquelle la procédure a été effectuée
            tests:
                - relationships :
                    to : ref('visit_detail')
                    field : visit_detail_id  

          - name: procedure_source_concept_id
            description: Concept associé à la procédure
            tests:
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id                    
### MEASUREMENT ###                    
    - name: measurement
      description: Table OMOP MEASUREMENT
      columns:
          - name: measurement_id
            description: Identifiant unique d'une mesure pour un patient
            tests:
                - unique
                - not_null

          - name: person_id
            description: Identifiant unique d'une personne
            tests:
                - not_null
                - relationships :
                    to : ref('person')
                    field : person_id

          - name: measurement_concept_id
            description: Concept standard d'une mesure
            tests:
                - not_null
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id
                    
          - name: measurement_date
            description: Date à laquelle la mesure a été effectée
            tests:
                - not_null

          - name: measurement_type_concept_id
            description: Type de provenance des données
            tests:
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id

          - name: provider_id
            description: Praticien qui a enregistré ou effectué la mesure
            tests:
                - relationships :
                    to : ref('provider')
                    field : provider_id
                    
          - name: visit_occurrence_id
            description: Identifiant de la visite pendant laquelle la mesure a été effectuée
            tests:
                - relationships :
                    to : ref('visit_occurrence')
                    field : visit_occurrence_id

          - name: visit_detail_id
            description: Identifiant de la visite détaillée pendant laquelle la mesure a été effectuée
            tests:
                - relationships :
                    to : ref('visit_detail')
                    field : visit_detail_id 
                    
          - name: measurement_source_concept_id
            description: Concept associé à la mesure
            tests:
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id 
### OBSERVATION ###                    
    - name: observation
      description: Table OMOP OBSERVATION
      columns:
          - name: observation_id
            description: Identifiant unique d'une observation pour un patient
            tests:
                - unique
                - not_null

          - name: person_id
            description: Identifiant unique d'une personne
            tests:
                - not_null
                - relationships :
                    to : ref('person')
                    field : person_id

          - name: observation_concept_id
            description: Concept standard d'une observation
            tests:
                - not_null
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id
                    
          - name: observation_date
            description: Date de l'observation
            tests:
                - not_null

          - name: observation_type_concept_id
            description: Type de provenance des données
            tests:
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id

          - name: provider_id
            description: Praticien qui a effectué un test ou enregistré l'observation
            tests:
                - relationships :
                    to : ref('provider')
                    field : provider_id
                    
          - name: visit_occurrence_id
            description: Identifiant de la visite pendant laquelle une observation a été faite
            tests:
                - relationships :
                    to : ref('visit_occurrence')
                    field : visit_occurrence_id

          - name: visit_detail_id
            description: Identifiant de la visite détaillée pendant laquelle une obseravtion a été faite
            tests:
                - relationships :
                    to : ref('visit_detail')
                    field : visit_detail_id 
                    
          - name: observation_source_concept_id
            description: Concept associé au code source de l'observation
            tests:
                - relationships :
                    to : source('vocabularies','concept')
                    field : concept_id 
                    