{{config(materialized='incremental',
         schema='omop',
         unique_key='observation_id',
         alias='observation')}}

SELECT 
    DISTINCT
    id::bigint                            AS observation_id, 
    person_id::bigint                     AS person_id, 
    concept_id::bigint                    AS observation_concept_id, 
    start_date::date                      AS observation_date,
    start_datetime::timestamp             AS observation_datetime,
    type_concept_id::bigint               AS observation_type_concept_id,
    value_as_number::numeric              AS value_as_number, 
    value_as_string::varchar(60)          AS value_as_string,
    value_as_concept_id::bigint           AS value_as_concept_id, 
    qualifier_concept_id::bigint          AS qualifier_concept_id, 
    unit_concept_id::bigint               AS unit_concept_id, 
    provider_id::bigint                   AS provider_id, 
    visit_occurrence_id::bigint           AS visit_occurrence_id, 
    visit_detail_id::bigint               AS visit_detail_id, 
    source_value::varchar(50)             AS observation_source_value,
    source_concept_id::bigint             AS observation_source_concept_id, 
    unit_source_value::varchar(50)        AS unit_source_value,
    qualifier_source_value::varchar(50)   AS qualifier_source_value 
FROM 
    {{ref('stem_table_omop')}}
    WHERE domain_id = 'Observation'
    
