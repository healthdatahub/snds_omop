{{config(materialized='incremental',
         schema='omop',
         unique_key='drug_exposure_id',
         alias='drug_exposure')}}
SELECT
    DISTINCT
    id::bigint                          AS drug_exposure_id,
    person_id::bigint                   AS person_id,
    concept_id::integer                 AS drug_concept_id,
    start_date::date                    AS drug_exposure_start_date,
    start_datetime::timestamp           AS drug_exposure_start_datetime,
    end_date::date                      AS drug_exposure_end_date,
    end_datetime::timestamp             AS drug_exposure_end_datetime,
    verbatim_end_date::date             AS verbatim_end_date,
    type_concept_id::integer            AS drug_type_concept_id,
    stop_reason::varchar(20)            AS stop_reason,
    refills::integer                    AS refills,
    quantity::integer                   AS quantity,
    days_supply::integer                AS days_supply,
    sig::text                           AS sig,
    lot_number::varchar(50)             AS lot_number,
    route_concept_id::integer           AS route_concept_id,
    provider_id::bigint                 AS provider_id,
    visit_occurrence_id::bigint         AS visit_occurrence_id,
    visit_detail_id::bigint             AS visit_detail_id,
    source_value::varchar(50)           AS drug_source_value,
    source_concept_id::integer          AS drug_source_concept_id,
    route_source_value::varchar(50)     AS route_source_value,
    dose_unit_source_value::varchar(50) AS dose_unit_source_value
FROM 
    {{ref('stem_table_omop')}}
WHERE domain_id='Drug'

