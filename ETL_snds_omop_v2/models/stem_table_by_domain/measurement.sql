{{config(materialized='incremental', 
         schema='omop',
         unique_key='measurement_id',
         alias='measurement')}}

SELECT 
    DISTINCT  
    id::bigint                      AS measurement_id, 
    person_id::bigint               AS person_id, 
    concept_id::integer             AS measurement_concept_id, 
    start_date::date                AS measurement_date,
    start_datetime::timestamp       AS measurement_datetime,
    '00:00:0000'::varchar(10)       AS measurement_time,
    type_concept_id::integer        AS measurement_type_concept_id, 
    operator_concept_id::integer    AS operator_concept_id, 
    value_as_number::float          AS value_as_number, 
    value_as_concept_id::integer    AS value_as_concept_id,
    unit_concept_id::integer        AS unit_concept_id,
    range_low::float                AS range_low,
    range_high::float               AS range_high,
    provider_id::bigint             AS provider_id,
    visit_occurrence_id::bigint     AS visit_occurrence_id,
    visit_detail_id::bigint         AS visit_detail_id,
    source_value::varchar(50)       AS measurement_source_value,
    source_concept_id::integer      AS measurement_source_concept_id,
    unit_source_value::varchar(50)  AS unit_source_value,
    value_source_value::varchar(50) AS value_source_value
FROM {{ref('stem_table_omop')}}
WHERE domain_id = 'Measurement'

