{{ config(materialized='view') }}

-- Ce model joint les finess juridiques au fichier de correspondance pour récupérer les raisons sociales correspondantes

WITH 
    mapping_finess_location AS(
        SELECT 
            finess_geo AS finess_j,
            ej_rs,
            dpt_code
        
        FROM {{ ref('bse__mapping_finess_location') }} ),
        

    finess_j__unioned AS(
        SELECT DISTINCT
            finess_j
        FROM {{ ref('finess__unioned')}}),
        
    location AS(
        SELECT *
        FROM {{ ref('location') }}),
    
    
    finess_j_mapped AS(
        SELECT DISTINCT
            maf.ej_rs                 AS care_site_name,        
            fin.finess_j              AS care_site_source_value,
            maf.dpt_code              AS location_source_value,
            maf.finess_j
                    
        FROM finess_j__unioned fin
        
        LEFT JOIN mapping_finess_location maf USING(finess_j) )           
            
SELECT * FROM finess_j_mapped