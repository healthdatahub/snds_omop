{{ config(materialized='view') }}

-- Ce modèle réalise la jointure entre la table T_MCOaaUM et la liste des unités médicales du MCO. 
-- Il supprime également les numéros finess à supprimer pour éviter les doublons

WITH 

    mco_finess AS(
        SELECT *
        FROM {{ ref('finess__unioned') }}
        WHERE pmsi_product = 'mco'   --Lignes qui concernent le MCO
        AND   type_um IS NOT NULL    --Lignes qui concernent les UM
        
    ),
        
    join_mco_list_um_finess AS(
        SELECT 
            mco.*,
            um.classification_um_fonctionnelle,
            fin.rs

        FROM mco_finess AS mco

        LEFT JOIN {{ source('correspondance', 'correspondance_um_uf_mco')}} um        
            ON SUBSTR(mco.type_um, 1,3) = um.code_um_actuel

        LEFT JOIN {{ source('correspondance', 'correspondance_finess_codecom') }} fin 
            ON mco.finess_geo = fin.et_finess
        
    )                
                
SELECT * FROM join_mco_list_um_finess