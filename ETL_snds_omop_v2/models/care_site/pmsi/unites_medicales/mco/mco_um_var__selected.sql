{{ config(materialized='view') }}

-- Ce modèle sélectionne et renomme les variables du modèle mco_finess_geo_um__joined qui seront utilisées dans la table care_site

SELECT 
    rs || '_' || classification_um_fonctionnelle AS care_site_name,
    finess_geo || '_' || type_um                 AS care_site_source_value,
    type_um                                      AS place_of_service_source_value,
    finess_geo                                   AS location_source_value
FROM {{ ref('mco_um_finess_geo__joined') }} mco        
        
    