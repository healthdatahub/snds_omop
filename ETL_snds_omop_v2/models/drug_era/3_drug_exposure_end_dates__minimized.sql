{{ config(materialized='table') }}

WITH 
normalized_dates_overlapping__joined AS
(
    SELECT 
        edn.person_id, 
        edn.ingredient_concept_id, 
        edn.drug_exposure_start_date, 
        oes.end_date
    FROM {{ref('1_drug_exposure_end_dates__normalized')}} edn
    JOIN {{ref('2_overlapping_exposures__sorted')}} oes 
        ON edn.person_id = oes.person_id 
            AND edn.ingredient_concept_id = oes.ingredient_concept_id 
            AND oes.end_date >= edn.drug_exposure_start_date
),

cteDrugExposureEnds (person_id, drug_concept_id, drug_exposure_start_date, drug_sub_exposure_end_date) AS
(
SELECT
    person_id, 
    ingredient_concept_id, 
    drug_exposure_start_date, 
    MIN(end_date) AS drug_sub_exposure_end_date
FROM normalized_dates_overlapping__joined 
GROUP BY 
    person_id, 
    ingredient_concept_id, 
    drug_exposure_start_date
) 

SELECT * FROM cteDrugExposureEnds
