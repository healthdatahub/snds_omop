{{ config(materialized='table') }}

WITH 
start_dates AS
-- select the start dates, assigning a row number to each
(
    SELECT 
        person_id, 
        ingredient_concept_id, 
        drug_sub_exposure_start_date AS event_date,
        -1 AS event_type,
        ROW_NUMBER() OVER (
            PARTITION BY person_id, ingredient_concept_id
            ORDER BY drug_sub_exposure_start_date
        ) AS start_ordinal
    FROM {{ref('5_days_exposed__computed')}}
),
end_dates_30_days_overlap AS
 -- pad the end dates by 30 to allow a grace period for overlapping ranges.
(
    SELECT 
        person_id, 
        ingredient_concept_id, 
        drug_sub_exposure_end_date + 30 AS event_date, 
        1 AS event_type, 
        NULL::bigint AS start_ordinal
    FROM  {{ref('5_days_exposed__computed')}}
),
start_end__unioned AS
(
    SELECT * FROM start_dates
    UNION ALL 
    SELECT * FROM end_dates_30_days_overlap 
),

event_dates__ordered AS
(
    SELECT 
        person_id, 
        ingredient_concept_id, 
        event_date, 
        event_type,
        MAX(start_ordinal) OVER (
            PARTITION BY person_id, ingredient_concept_id
            ORDER BY event_date, event_type ROWS 
            UNBOUNDED PRECEDING
        ) AS start_ordinal,
        -- this pulls the current START down from the prior rows so that the NULLs
        -- from the END DATES will contain a value we can compare with
        ROW_NUMBER() OVER (
            PARTITION BY person_id, ingredient_concept_id
            ORDER BY event_date, event_type
        ) AS overall_ord
        -- this re-numbers the inner UNION so all rows are numbered ordered by the event date
    FROM  start_end__unioned

),

cteEndDates (person_id, ingredient_concept_id, end_date) AS -- the magic
(
    SELECT 
        person_id, 
        ingredient_concept_id, 
        event_date - 30 AS end_date -- unpad the end date
    FROM event_dates__ordered
    WHERE (2 * start_ordinal) - overall_ord = 0
)

SELECT * FROM cteEndDates