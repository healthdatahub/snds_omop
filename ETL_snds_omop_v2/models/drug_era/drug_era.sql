{{ config(materialized='table', 
          schema='drug') }}

WITH
    final_counts_dates AS (
        SELECT

            person_id, 
            ingredient_concept_id AS drug_concept_id, 
            era_end_date AS drug_era_end_date,

            MIN(drug_sub_exposure_start_date) AS drug_era_start_date,  
            SUM(drug_exposure_count) AS drug_exposure_count,
            SUM(days_exposed) AS days_exposed
        
        FROM {{ref('7_drug_era_end_dates')}} 
        GROUP BY person_id, drug_concept_id, drug_era_end_date )



SELECT 
    row_number()over(
            order by person_id
        ) drug_era_id, 
    person_id, 
    drug_concept_id, 
    drug_era_start_date, 
    drug_era_end_date, 
    drug_exposure_count,
    ({{date_diff('drug_era_start_date','drug_era_end_date')}}-days_exposed) as gap_days 
FROM 
    final_counts_dates