{{ config(materialized='table') }}

WITH
cteFinalTarget(row_number, person_id, ingredient_concept_id, drug_sub_exposure_start_date, drug_sub_exposure_end_date, drug_exposure_count, days_exposed) AS
(
    SELECT 
        row_number, 
        person_id, 
        drug_concept_id, 
        drug_sub_exposure_start_date, 
        drug_sub_exposure_end_date, 
        drug_exposure_count, 
        {{date_diff('drug_sub_exposure_start_date','drug_sub_exposure_end_date')}}::integer AS days_exposed
    FROM {{ref('4_sub_exposures__sorted')}}
)

SELECT * FROM cteFinalTarget