{{ config(materialized='table', 
          unique_key = 'condition_era_id',
          schema='omop')}}


SELECT 
    ROW_NUMBER() OVER(
        ORDER BY person_id) AS condition_era_id,
    
    person_id,
    condition_concept_id,
    era_end_date AS condition_era_end_date,
    MIN(condition_start_date) AS condition_era_start_date,
    COUNT(*) AS condition_occurrence_count

FROM {{ ref('ConditionEnds')}}

GROUP BY 2,3,4