{{config(materialized='table', schema='omop', alias='procedure_occurrence', indexes = [{'columns' : ['procedure_occurrence_id'], 'unique' : True}])}}

-- Ce modèle rassemble toutes les lignes de la STEM_TABLE dont le concept standard correspond au domaine Procedure. Il représente la table PROCEDURE_OCCURRENCE

SELECT
    DISTINCT
    id::bigint                         AS procedure_occurrence_id,  
    person_id::bigint                  AS person_id, 
    concept_id::integer                AS procedure_concept_id, 
    start_date::date                   AS procedure_date, 
    start_datetime::timestamp          AS procedure_datetime, 
    type_concept_id::integer           AS procedure_type_concept_id, 
    modifier_concept_id::integer       AS modifier_concept_id,  
    quantity::integer                  AS quantity,   
    provider_id::bigint                AS provider_id, 
    visit_occurrence_id::bigint        AS visit_occurrence_id, 
    visit_detail_id::bigint            AS visit_detail_id,
    source_value::varchar(50)          AS procedure_source_value, 
    source_concept_id::integer         AS procedure_source_concept_id, 
    modifier_source_value::varchar(50) AS modifier_source_value
FROM {{ref('stem_table_omop')}}
WHERE domain_id = 'Procedure'
    