{{config(materialized='table', schema='omop', alias='condition_occurrence',  indexes = [{'columns' : ['condition_occurrence_id'], 'unique' : True}])}}

-- Ce modèle rassemble toutes les lignes de la STEM_TABLE dont le concept standard correspond au domaine Condition. Il représente la table CONDITION_OCCURRENCE

SELECT
    DISTINCT 
    id::bigint                       AS condition_occurrence_id,
    person_id::bigint                AS person_id,
    concept_id::integer              AS condition_concept_id,
    start_date::date                 AS condition_start_date,
    start_datetime::timestamp        AS condition_start_datetime,
    end_date::date                   AS condition_end_date,
    end_datetime::timestamp          AS condition_end_datetime,
    type_concept_id::integer         AS condition_type_concept_id,
    status_concept_id::integer       AS condition_status_concept_id,
    stop_reason::varchar(20)         AS stop_reason,
    provider_id::bigint              AS provider_id,
    visit_occurrence_id::bigint      AS visit_occurrence_id,
    visit_detail_id::bigint          AS visit_detail_id,
    source_value::varchar(50)        AS condition_source_value,
    source_concept_id::integer       AS condition_source_concept_id,
    status_source_value::varchar(50) AS condition_status_source_value
FROM 
    {{ref('stem_table_omop')}}
WHERE domain_id = 'Condition'