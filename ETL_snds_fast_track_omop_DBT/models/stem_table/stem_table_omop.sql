{{config(materialized='table', schema='stem_table', indexes = [{'columns' : ['id'], 'unique' : True}])}}

WITH stem_table AS
(
{{dbt_utils.union_relations(
    relations=[
        ref('condition_occurrence__unioned'), 
        ref('device_exposure__unioned'), 
        ref('drug_exposure__unioned'),
        ref('measurement__unioned'),
        ref('procedure_occurrence__unioned')
    ]
)
}}
)

SELECT 
    {{set_id(var('stem_table_key'))}} AS id,
    *
FROM stem_table 


