{{ config(materialized='view') }}

-- Ce modèle sélectionne les lignes de la table source_to_concept_map relatives à des codes LPP et effectue une joiture sur la table CONCEPT pour récupérer le domaine associé au code SNOMED. 

{{ filter_concepts(source_vocabs=["'LPP'"]) }}