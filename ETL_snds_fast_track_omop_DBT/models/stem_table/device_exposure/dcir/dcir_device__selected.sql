{{ config(materialized='view') }}

-- Ce modèle corrige la date de fin dans le cas d'un achat, et calcule la variable visit_source_value à partir des variables du modèle tip_quantity_dates__grouped. 

SELECT 
    quantity,
    tip_acl_dtd       AS start_date,  
    
    CASE
        WHEN tip_prs_typ = '1' -- achat
            THEN NULL::date -- On enlève la date de fin car égale à la date d'achat 
        ELSE tip_acl_dtf 
    END  end_date, 
    
    tip_prs_ide::text AS source_value, 

    {{ visit_source_value(
            src_base="'dcir19_20'", 
            src_finess_nb='etb_pre_fin', 
            src_visit_nb='dcir_visit_id' ) }}::text AS visit_source_value

FROM {{ ref('tip_quantity_dates__grouped') }} 

WHERE tip_prs_ide::text != '9999999999999'