{{ config(materialized='view') }}

-- Ce modèle sélectionne les variables de la table T_MCOaaDMIP qui alimenteront la table DEVICE_EXPOSURE et calcule le visit_source_value associé à la pose ou l'achat du dispositif médical

{{ device_exposure_pmsi(
    source_value  = 'lpp_cod', 
    quantity      = 'nbr_pos', 
    delai         = 'delai', 
    src_base      = "'mco19_09'", 
    src_finess_nb = 'eta_num', 
    src_visit_nb  = 'rsa_num', 
    src_table     = 'snds.t_mco19_09dmip' ) }}