{{ config(materialized='view') }}

-- Ce modèle unit les modèles stg__t_mcoaa_dmip et stg__t_mcoaafpstc (dispositifs médicaux). Il effectue ensuite une jointure sur le modèle lpp_concepts pour récupérer le concept standard correspondant au code source LPP, il effectue également une jointure avec la table VISIT_OCCURRENCE pour récupérer les informations relatives à la visite associée à la pose du dispositif médical. 

WITH 
    mco_dmip_fpstc__unioned AS(
    {{ dbt_utils.union_relations(
        relations = [
            ref('stg__t_mcoaa_dmip_device'),
            ref('stg__t_mcoaa_fpstc_device') ] ) }} ),
            
    lpp_concept AS(
        SELECT *
        FROM {{ ref('lpp_concepts') }} ),
        
        
    visit_occurrence_pmsi AS(
        SELECT *
        FROM {{ ref('visit_occurrence_pmsi') }}),

        
        
    device_visit_concept__joined AS (
        SELECT
            v.person_id                                            AS person_id,
            v.visit_occurrence_id                                  AS visit_occurrence_id,
            v.provider_id                                          AS provider_id,     
           
        
        
            mdf.quantity                                           AS quantity, 
            (v.visit_start_date  + COALESCE(mdf.delai,0)::integer) AS start_date, -- Date de pose ou achat du dispositif
            mdf.source_value                                        AS source_value, -- Code source LPP
            NULL::date                                             AS end_date,


            COALESCE(lc.source_concept_id,0)                       AS source_concept_id, -- Concept associé au code LPP
            COALESCE(lc.target_concept_id,0)                       AS concept_id, -- Code standard SNOMED
            COALESCE(lc.domain_id, 'Device')                       AS domain_id -- Domaine du code standard SNOMED
 
        
        FROM mco_dmip_fpstc__unioned mdf
        
            JOIN visit_occurrence_pmsi v USING (visit_source_value)
        
            LEFT JOIN lpp_concept  lc USING(source_value)),
        
        
        
        
    quantities__summed AS(
        SELECT 
            domain_id::varchar(20)      AS domain_id,   
            person_id::bigint           AS person_id,
            visit_occurrence_id::bigint AS visit_occurrence_id,
            NULL::bigint                AS visit_detail_id,
            provider_id::bigint         AS provider_id,
            concept_id::integer         AS concept_id,
            source_value::text          AS source_value,
            source_concept_id::integer  AS source_concept_id, -- voir dans fichier des concepts 
            start_date::date            AS start_date,
            NULL::date                  AS end_date,
        
            SUM(quantity)::float        AS quantity  
        
        
        FROM device_visit_concept__joined
        
        GROUP BY 1,2,3,4,5,6,7,8,9,10 )
                
                
                
SELECT * FROM quantities__summed
        
