{{ config(materialized='view') }}

-- Ce modèle joint la table des médicaments cip_ucd au modèle athena_mapping pour obtenir la correspondance entre un code UCD-7 et le code standard correspondant RxNorm. 

SELECT 
    cu.codeucd::text AS source_value, -- Code UCD-7

    ath.concept_code, -- Code CIP-7 
    ath.concept_id_1  AS source_concept_id, -- Concept associé au code CIP-7 dans ATHENA
    ath.concept_id_2  AS concept_id, -- Concept standard RxNorm
    ath.domain_id -- Domain correspondant au concept standard RxNorm

FROM 
    snds.cip_ucd cu -- Table contenant les correspondances CIP-7 <-> UCD-7 <-> CIP-13 <-> UCD-13

LEFT JOIN {{ ref('athena_mapping') }} ath 
    ON TRIM(codecip::text) = concept_code   
    WHERE vocabulary_id = 'BDPM'           
