{{ config(materialized='view') }}

-- Ce modèle unit tous les modèles correspondant aux tables du PMSI qui alimentent la table DRUG_EXPOSURE, il les joint à la table VISIT_OCCURRENCE pour récupérer les identifiants du patient et de la visite et il joint également aux modèles ucd7_cip7 et ucd13_cip7 pour récupérer le code standard RxNorm correspondants aux codes ucd7 et ucd13. Il calcule la date de début de prise en fonction du délai et de la date de début de la visite, et le nombre de jours de prise et il somme les quantités sur tous les autres critères.

WITH 
    drug_pmsi__unioned AS(
        {{ dbt_utils.union_relations(
            relations=[
                ref('stg__t_mcoaafhstc_drug'),
                ref('stg__t_mcoaamed_drug'),
                ref('stg__t_mcoaamedatu_drug'),
                ref('stg__t_mcoaamedthrombo_drug'),
                
                ref('stg__t_hadaamed_drug'),
                ref('stg__t_hadaamedatu_drug'),
                ref('stg__t_hadaamedchl_drug'),
                
                ref('stg__t_ssraamed_drug'),
                ref('stg__t_ssraamedatu_drug') ] ) }} ),

    cip7 AS (
        {{ dbt_utils.union_relations(
            relations=[
                ref('ucd7_cip7'),
                ref('ucd13_cip7') ] ) }} ),       
        
    visit_occurrence_pmsi AS (
        SELECT *
        FROM {{ ref('visit_occurrence_pmsi') }} ),


    join_visit AS (
        SELECT           
            v.person_id                                            AS person_id, 
            v.visit_occurrence_id                                  AS visit_occurrence_id,
            NULL                                                   AS provider_id,
            v.visit_end_date                                       AS end_date,
        
            COALESCE(cip.concept_id,0)                             AS concept_id,
            COALESCE(cip.source_concept_id)                        AS source_concept_id,
            COALESCE(cip.domain_id, 'Drug')                        AS domain_id,
        
            dp.source_value                                        AS source_value,
            dp.quantity                                            AS quantity,

            (v.visit_start_date::date + dp.delai::integer)         AS start_date, 
        
            {{ date_diff(
                start_date = 'v.visit_start_date', 
                end_date   = 'v.visit_end_date') }} + 1 - dp.delai AS days_supply, -- Le nombre de jours de prise est égale au nombre de jours écoulés entre le début de la prise du médicament et la fin de la visite + 1 jour
            
            NULL                                                   AS dose_unit_source_value
        
        
        FROM  drug_pmsi__unioned dp
        
            JOIN visit_occurrence_pmsi v USING(visit_source_value)
            
            LEFT JOIN cip7 cip USING(source_value) ),
            
            

    total_quantity AS (
        SELECT
            person_id,
            visit_occurrence_id, 
            concept_id,
            source_concept_id,
            domain_id,
            provider_id,
            source_value,
            start_date,
            SUM(quantity) AS quantity, -- Total du nombre de doses associées à une même date
            days_supply,
            end_date,
            dose_unit_source_value

        FROM 
            join_visit

        GROUP BY 1,2,3,4,5,6,7,8,10,11,12 )
        
SELECT * FROM total_quantity




