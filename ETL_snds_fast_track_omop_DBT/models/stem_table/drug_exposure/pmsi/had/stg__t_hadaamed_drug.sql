{{ config(materialized='view') }}

-- Ce modèle sélectionne les variables de la table T_HADaaMED qui alimentent la table DRUG_EXPOSURE et construit le visit_source_value de la visite pendant laquelle a eu lieu la prise de médicament.

{{ drug_pmsi(
        src_table     = 'snds.t_had19_09med' , 
        src_base      = "'had19_09'", 
        src_finess_nb = 'eta_num_epmsi', 
        src_visit_nb  = 'rhad_num') }}