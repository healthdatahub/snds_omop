{{ config(materialized='view') }}

--- Ce modèle sélectionne les variables de la table T_MCOaaMEDATU qui alimentent la table DRUG_EXPOSURE et construit le visit_source_value de la visite pendant laquelle a eu lieu la prise de médicament.

{{ drug_pmsi(
        src_table     = 'snds.t_mco19_09medatu', 
        src_base      = "'mco19_09'", 
        src_finess_nb = 'eta_num', 
        src_visit_nb  = 'rsa_num' ) }}