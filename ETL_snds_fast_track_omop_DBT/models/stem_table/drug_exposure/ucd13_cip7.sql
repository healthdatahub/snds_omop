{{ config(materialized='view') }}

-- Ce modèle joint la table des médicaments cip_ucd au modèle athena_mapping pour obtenir la correspondance entre un code UCD-13 et le code standard correspondant RxNorm. 

SELECT 
    cu.codeucd13::text AS source_value, -- Code UCD-13

    ath.concept_code, -- Code CIP-7
    ath.concept_id_1    AS source_concept_id, -- Concept associé au code UCD-13 dans AHTENA
    ath.concept_id_2    AS concept_id, -- Concept standard RxNorm
    ath.domain_id -- Domaine associé au code standard RxNorm

FROM 
    snds.cip_ucd cu

LEFT JOIN {{ ref('athena_mapping') }} ath 
    ON TRIM(codecip::text) = concept_code 
    WHERE vocabulary_id = 'BDPM'