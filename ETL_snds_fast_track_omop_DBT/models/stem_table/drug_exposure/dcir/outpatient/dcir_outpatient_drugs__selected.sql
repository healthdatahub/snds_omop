{{ config(materialized='view') }}

-- Ce modèle selectionne les variables de la table ER_PHA_F qui alimentent la table DRUG_EXPOSURE, il calcule provider_source_value et visit_source_value qui concernent la visite pendant laquelle le médicament a été délivré. 

SELECT 
    pha_prs_c13::text                                        AS source_value, -- Code CIP-13 du médicament
   {{ provider_source_value('psp_spe_cod', 'psp_act_nat') }} AS provider_source_value,
    pre_pre_dtd                                              AS start_date,
    (pre_pre_dtd)::date                                      AS end_date, -- La date de fin de traitement dans le DCIR n'est pas connue. 
    quantity                                                 AS quantity,
    pha_dos_unt_dses                                         AS dose_unit_source_value,
    
    {{ visit_source_value( 
        src_base      = "'dcir19_20'", 
        src_finess_nb = 'etb_pre_fin', 
        src_visit_nb  = 'dcir_visit_id') }} AS visit_source_value
    
    FROM 
        {{ ref('pha_quantity__grouped') }} 