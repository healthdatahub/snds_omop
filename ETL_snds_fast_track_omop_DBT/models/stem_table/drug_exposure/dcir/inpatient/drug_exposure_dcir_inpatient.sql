{{ config(materialized='view') }}

-- Ce modèle joint le modèle dcir_inpatient_drugs__selected avec les tables VISIt_OCCURRENCE et PROVIDER et avec le modèle ucd7_cip7 qui permet d'obtenir le concept standard RxNorm à partir des codes UCD7.


WITH 

    dcir_drug_inpatient__selected AS(
        SELECT *
        FROM {{ ref('dcir_inpatient_drugs__selected') }} ),
        
    cip7 AS (
        SELECT *
        FROM {{ ref('ucd7_cip7') }} ),
        
        
    visit_occurrence_dcir AS(
        SELECT *
        FROM {{ ref('visit_occurrence_dcir') }} ),
        
        
    provider AS(
        SELECT *
        FROM {{ ref('provider') }} ),

    join_visit AS (
        SELECT 
            v.person_id                        AS person_id, 
            v.visit_occurrence_id              AS visit_occurrence_id,
        
            p.provider_id                      AS provider_id, 
            
            COALESCE(cip.concept_id, 0)        AS concept_id,
            COALESCE(cip.source_concept_id, 0) AS source_concept_id,
            COALESCE(cip.domain_id, 'Drug')    AS domain_id,
            

            ddi.source_value                   AS source_value,
            ddi.start_date                     AS start_date,
            ddi.end_date                       AS end_date,
            ddi.quantity                       AS quantity,
            
            1                                  AS days_supply,
            NULL                               AS dose_unit_source_value
            
            
    FROM dcir_drug_inpatient__selected ddi
    
        JOIN visit_occurrence_dcir  v USING(visit_source_value)
        
        LEFT JOIN provider p USING(provider_source_value)
    
        LEFT JOIN cip7     cip USING(source_value) )
                
SELECT * FROM join_visit                
