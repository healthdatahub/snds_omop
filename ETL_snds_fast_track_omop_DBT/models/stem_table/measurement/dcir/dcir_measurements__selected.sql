{{ config(materialized='view') }}

-- Ce modèle construit le visit_source_value associé à la visite pendant laquelle l'acte a été effectué.

SELECT 
    bio_prs_ide                            AS source_value, -- Code NABM de l'acte de biologie
    {{ visit_source_value(
        src_base      = "'dcir19_20'", 
        src_finess_nb = 'etb_pre_fin', 
        src_visit_nb  = 'dcir_visit_id' ) }}  AS visit_source_value,
        quantity                           AS quantity 
        
        FROM {{ ref('bio_quantity__grouped') }} 