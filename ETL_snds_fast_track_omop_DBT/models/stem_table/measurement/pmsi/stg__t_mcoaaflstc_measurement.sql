{{ config(materialized='view') }}

SELECT 

    nabm_cod::text                    AS source_value,
    {{ visit_source_value( 
        src_base = "'mco19_09ace'", 
        src_finess_nb = 'eta_num', 
        src_visit_nb ='seq_num') }}   AS visit_source_value
FROM 
    snds.t_mco19_09flstc 
    