{{ config(materialized='view') }}

-- Ce modèle unit les modèles stg__t_mcoaa_flstc et stg__t_ssraa_flstc (actes de biologie NABM). Il effectue ensuite une jointure sur le modèle nabm_concepts pour récupérer le concept standard correspondant au code source NABM, il effectue également une jointure avec la table VISIT_OCCURRENCE pour récupérer les informations relatives à la visite associée à l'acte. 

WITH 
    pmsi_measurements AS(
        {{ dbt_utils.union_relations(
            relations=[
                ref('stg__t_mcoaaflstc_measurement'),
                ref('stg__t_ssraaflstc_measurement') ] ) }} ),
                
                
    nabm_concepts AS (
        SELECT *
        FROM {{ ref('nabm_concepts') }} ) ,
        
        
    visit_occurrence_pmsi AS(
        SELECT *
        FROM {{ ref('visit_occurrence_pmsi') }} ),
        
        
    pmsi_concepts_visits_measurements__joined AS(

        SELECT 
            v.person_id::bigint                                AS person_id,
            v.visit_occurrence_id::bigint                      AS visit_occurrence_id,
            v.provider_id::bigint                              AS provider_id,
            v.visit_start_date::date                           AS start_date,
            v.visit_start_datetime::timestamp                  AS start_datetime,
            v.visit_end_date::date                             AS end_date,
            v.visit_end_datetime::timestamp                    AS end_datetime,        
        
            pm.source_value::text                              AS source_value,

            COALESCE(nc.source_concept_id, 0)::integer         AS source_concept_id, 
            COALESCE(nc.target_concept_id, 0)::integer         AS concept_id,
            COALESCE(nc.domain_id, 'Measurement')::varchar(20) AS domain_id



        FROM 
            pmsi_measurements pm 

            JOIN visit_occurrence_pmsi v USING(visit_source_value)

            LEFT JOIN nabm_concepts nc USING(source_value))
                    
                    
SELECT * FROM pmsi_concepts_visits_measurements__joined