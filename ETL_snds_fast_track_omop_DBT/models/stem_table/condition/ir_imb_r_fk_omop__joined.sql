{{ config(materialized='view')}}

-- Ce modèle sélectionne les diagnostics d'ALD provenant de la table IR_IMB_R.
-- Il les joint ensuite à la table PERSON pour obtenir le person_id des personnes concernées, à la table DEATH pour indiquer la date de mort comme fin de l'ALD si le patient est décédé, et au modèle athena_mapping pour obtenir le source_concept_id et le concept_id correspondants au diagnostic du SNDS. 

WITH 
ald_patho AS 

(
SELECT
    med_mtf_cod::varchar(100)       AS source_value,
    coalesce(imb_ald_dtd, ins_dte)  AS start_date,  -- Date de début de l'ALD si existante, sinon date d'insertion dans la base                         
    CASE imb_ald_dtf
        WHEN '1600-01-01'::date THEN '2020-12-31'::date
        WHEN '2099-12-31'::date THEN '2020-12-31'::date
    END end_date,    
    imb_etm_nat                     AS condition_status_source_value, 
    num_enq                         AS person_source_value  
FROM
    {{ref('ir_imb_r__fixed_dates')}}
), 

persons AS 
(
    SELECT
        person_id,
        provider_id,
        person_source_value
    FROM 
        {{ref('person')}}
),

join_person_concept AS
(
SELECT 
        person_id                   AS person_id,   
        NULL::bigint                AS visit_occurrence_id,
        provider_id                 AS provider_id,
        start_date                  AS start_date,
        LEAST(end_date, death_date) AS end_date, -- Date de fin de l'ALD si la personne est encore en vie, sinon date de mort. 
        0                           AS status_concept_id,
        condition_status_source_value AS status_source_value,
        a.source_value          AS source_value,
        c.concept_id_1          AS source_concept_id,
        c.concept_id_2          AS concept_id,
        --0                       AS visit_detail_id,
        domain_id               AS domain_id,
        '0'                     AS visit_source_value
        
FROM (  
        ald_patho a
        JOIN persons b USING(person_source_value)
        LEFT JOIN {{ref('death')}} USING(person_id)
        LEFT JOIN  {{ref('athena_mapping')}} c ON a.source_value = REPLACE(c.concept_code, '.', '') -- Les codes français contiennent des points, et non la version internationale --> on les supprime pour réaliser l'alignement. 
    )    
WHERE vocabulary_id = 'CIM10'
)

SELECT * FROM join_person_concept