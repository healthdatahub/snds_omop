{{ config(materialized='view') }}

-- Ce modèle sélectionne les diagnostics principaux des tables t_ssraaD

WITH diag_t_ssraad_visit
AS
(
{{
    condition_source_values(
        src_table = 'snds.t_ssr19_09d', 
        source_values = ['dgn_cod'], 
        src_base = "'ssr'", 
        src_finess_nb = 'eta_num', 
        src_visit_nb = 'rha_num' 
    )
}}
)


SELECT * FROM diag_t_ssraad_visit