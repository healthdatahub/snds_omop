{{ config(materialized='view') }}

-- Ce modèle sélectionne les diagnostics principaux des tables t_ssraaB.

WITH diag_t_ssraab_visit
AS
(
{{
    condition_source_values(
        src_table = 'snds.t_ssr19_09b', 
        source_values = ['fp_pec', 'mor_prp', 'etl_aff'], 
        src_base = "'ssr'", 
        src_finess_nb = 'eta_num', 
        src_visit_nb = 'rha_num' 
    )
}}
)


SELECT * FROM diag_t_ssraab_visit