{{ config(materialzied='view')}}

-- Ce modèle sélectionne les diagnotics principaux des tables UM du MCO FT

WITH diag_c_mco_um_ft_visit
AS
(
{{
    condition_source_values(
        src_table = 'snds.c_mco_um_ft', 
        source_values = ['dgn_pal'], 
        src_base = "'mcoft'", 
        src_finess_nb = 'eta_num', 
        src_visit_nb = 'rsa_num' 
    )
}}
)
SELECT * FROM diag_c_mco_um_ft_visit