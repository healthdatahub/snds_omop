-- Ce modèle sélectionne les diagnostics principaux et reliés des tables t_mcoaaUM

WITH diag_t_mcoaaum
AS
(
{{
    condition_source_values(
        src_table = 'snds.t_mco19_09um', 
        source_values = ['dgn_pal', 'dgn_rel'], 
        src_base = "'mco19_09'", 
        src_finess_nb = 'eta_num', 
        src_visit_nb = 'rsa_num' 
    )
}}
)

SELECT * FROM diag_t_mcoaaum