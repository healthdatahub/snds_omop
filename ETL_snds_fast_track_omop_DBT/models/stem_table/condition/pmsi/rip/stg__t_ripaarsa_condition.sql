{{ config(materialized='view') }}

-- Ce modèle sélectionne les diagnostics principaux des tables t_ripaaRSA

WITH diag_t_ripaarsa_visit
AS
(
{{ condition_source_values(
        src_table = 'snds.t_rip19_09rsa',
        source_values = ['dgn_pal'],
        src_base="'rip19_09'",
        src_finess_nb='eta_num_epmsi',
        src_visit_nb='rip_num'
)
}}
    )


SELECT * FROM diag_t_ripaarsa_visit