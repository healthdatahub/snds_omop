{{ config(materialized='view') }}

-- Ce modèle unit les diagnostics associés provenant de toutes les tables D des champs MCO, HAD, SSR.
-- Il les joint ensuite à la table VISIT_OCCURENCE pour récupérer des informations concernant la visite pendant laquelle un diagnostic a été posé (visit_occurrence_id, person_id, dates de la visite, provider)

{% set condition_status = "'Diagnostic Associé'" %}
{% set condition_status_concept_id = 32908 %} -- Diagnostic secondaire

WITH
diag_ass
AS
(
{{
condition_visit_status(
        src_table = ref('stg__t_mcoaad_condition'),
        src_value = 'ass_dgn',
        status = condition_status,
        status_concept_id = condition_status_concept_id
)
}}
UNION ALL
{{
condition_visit_status(
        src_table=ref('stg__c_mco_d_ft_condition'),
        src_value='ass_dgn',
        status = condition_status,
        status_concept_id = condition_status_concept_id
)
}}
UNION ALL
{{
condition_visit_status(
        src_table=ref('stg__t_ssraad_condition'),
        src_value='dgn_cod',
        status = condition_status,
        status_concept_id = condition_status_concept_id
)
}}
UNION ALL 
{{
condition_visit_status(
        src_table=ref('stg__t_hadaad_condition'),
        src_value='dgn_ass',
        status = condition_status,
        status_concept_id = condition_status_concept_id
)
}}
),

visits AS 
(
    SELECT 
        vo.person_id,
        vo.visit_occurrence_id,
        vo.provider_id,
        vo.visit_start_date,
        vo.visit_end_date,
        vo.visit_source_value
    FROM 
        {{ref('visit_occurrence_pmsi')}} vo 
),

join_visit_concept
AS
(
SELECT
    DISTINCT
        person_id               AS person_id,
        visit_occurrence_id     AS visit_occurrence_id,
        provider_id             AS provider_id,
        visit_start_date        AS start_date,
        visit_end_date          AS end_date,
        status_concept_id       AS status_concept_id,
        status_source_value     AS status_source_value,
        source_value            AS source_value,
        concept_id_1            AS source_concept_id,
        concept_id_2            AS concept_id,
        domain_id               AS domain_id,
        visit_source_value
        
FROM
        diag_ass
        JOIN visits USING(visit_source_value)
        JOIN  {{ref('athena_mapping')}} ON source_value = REPLACE(concept_code, '.', '')
        WHERE vocabulary_id = 'CIM10'
    
)

SELECT * FROM join_visit_concept 