{{ config(materialized='view') }}

-- Ce modèle joint le modèle procedure_dcir_quantity__selected à la table VISIT_OCCURRENCE pour obtenir des informations relatives aux visites pendant lesquelles des procédures ont eu lieu, telles que person_id, visit_occurrence_id, provider_id et les dates de visites. 
-- Il effectue également une jointure avec la table SOURCE_TO_CONCEPT_MAP pour sélectionner le concept_id associé au code source CCAM, le code standard SNOMED correspondant, et le domaine auquel appartient le code standard. 

WITH
visits AS
(
    SELECT
        person_id,
        visit_occurrence_id,
        provider_id,
        visit_start_date,
        visit_end_date,
        visit_source_value
    FROM {{ref('visit_occurrence_dcir')}}
),

join_visit_concept AS
(   
    SELECT
        person_id                AS person_id,
        visit_occurrence_id      AS visit_occurrence_id,
        provider_id              AS provider_id,
        source_value             AS source_value, -- Code CCAM de l'acte
        coalesce(source_concept_id,0)::integer AS source_concept_id, -- concept associé au code CCAM
        coalesce(target_concept_id,0)::integer AS concept_id, -- Code standard SNOMED 
        coalesce(domain_id, 'Procedure')       AS domain_id,  -- Domaine auquel appartient le code standard
        visit_start_date        AS start_date,
        visit_end_date          AS end_date,
        quantity                AS quantity  
    FROM 
        {{ref('procedure_dcir_quantity__selected')}}
        JOIN visits USING(visit_source_value)
        LEFT JOIN {{ref('stg__source_to_concept_map__ccam_csarr')}} USING(source_value)
)



SELECT * FROM join_visit_concept 