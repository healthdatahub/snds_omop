-- Ce model sélectionne les enregistrements de la table source_to_concept_map correspondant à des codes CCAM et CSARR, puis il effectue une jointure sur la table concept pour obtenir le domain_id associé aux concepts standards.

-- La table source_to_concept_map est "découpée" en sous-modèles car la construction de la vue est plus rapide lors de chaque appel. 

{{filter_concepts(source_vocabs=["'CCAM'", "'CSARR'"])}}