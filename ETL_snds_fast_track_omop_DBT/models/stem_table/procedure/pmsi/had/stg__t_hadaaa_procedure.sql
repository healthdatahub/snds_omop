{{ config(materialized='view') }}

-- Ce modèle sélectionne les variables des tables t_HADaaA utiles pour calculer visit_source_value associé à la visite pendant laquelle un actes a été réalisé, ainsi que celles qui alimenteront la table PROCEDURE_OCCURRENCE 

WITH procedure_t_hadaaa AS
(
    {{
    procedure_pmsi_source_value(
        src_table = 'snds.t_had19_09a', 
        source_value = 'ccam_cod', 
        src_base = "'had19_09'", 
        src_finess_nb = 'eta_num_epmsi', 
        src_visit_nb = 'rhad_num',
        phase = 'pha_cod',
        delai = 'del_deb_sseq'  
    )
}}
)

SELECT * FROM procedure_t_hadaaa