-- Ce modèle sélectionne les variables des tables t_RIPaaCCAM utiles pour calculer visit_source_value associé à la visite pendant laquelle un actes a été réalisé, ainsi que celles qui alimenteront la table PROCEDURE_OCCURRENCE 

WITH procedure_t_ripaaccam AS
(
    {{
    procedure_pmsi_source_value(
        src_table = 'snds.t_rip19_09ccam', 
        source_value = 'cdc_act', 
        src_base = "'rip19_09'", 
        src_finess_nb = 'eta_num_epmsi', 
        src_visit_nb = 'rip_num',
        phase = 'pha_act',
        quantity = 'nbr_exe_act',
        delai = 'ent_dat_del' 
    )
}}
)

SELECT * FROM procedure_t_ripaaccam