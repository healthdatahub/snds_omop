-- Ce modèle sélectionne les variables des tables t_ssraaCSARR utiles pour calculer visit_source_value associé à la visite pendant laquelle un actes a été réalisé, ainsi que celles qui alimenteront la table PROCEDURE_OCCURRENCE 

WITH procedure_t_ssraacsarr AS
(
    {{
    procedure_pmsi_source_value(
        src_table = 'snds.t_ssr19_09csarr', 
        source_value = 'csarr_cod', 
        src_base = "'ssr'", 
        src_finess_nb = 'eta_num', 
        src_visit_nb = 'rha_num', 
        phase = None,
        delai = 'ent_dat_del_um' 
    )
}}
)

SELECT * FROM procedure_t_ssraacsarr