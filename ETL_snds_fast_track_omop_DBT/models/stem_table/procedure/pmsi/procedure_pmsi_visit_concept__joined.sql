{{ config(materialzed='view') }}

-- Ce modèle joint le modèle prodecure_pmsi__unioned avec la table VISIT_OCCURRENCE du PMSI pour récupérer des informations relatives au visites associées aux actes : person_id, visit_occurrence_id, provider_id et les dates de visites. Il est également joint à la table SOURCE_TO_CONEPT_MAP pour obtenir les concepts associés aux codes sources (source_concept_id), les concepts standards SNOMED correspondants aux codes sources CCAM et CSARR (concept_id), et le domain du code standard.

WITH visits AS
(
    SELECT 
        person_id,
        visit_occurrence_id,
        provider_id,
        visit_start_date,
        visit_end_date,
        visit_source_value
    FROM {{ ref('visit_occurrence_pmsi')}}
),

join_visit_concept AS
(   
    SELECT
        person_id                 AS person_id,
        visit_occurrence_id       AS visit_occurrence_id,
        provider_id               AS provider_id,
        source_value              AS source_value,-- Code source CCAM ou CSARR issu du SNDS
        coalesce(source_concept_id,0)      AS source_concept_id, -- Concept associé au code source
        coalesce(target_concept_id,0)      AS concept_id, -- Concept standard SNOMED
        coalesce(domain_id, 'Procedure')   AS domain_id, -- Domaine du concept standard
        visit_start_date + delai::integer  AS start_date,
        GREATEST(visit_end_date, visit_start_date + delai::integer) AS end_date,
        quantity                  AS quantity  
    FROM 
        {{ref('procedure_pmsi__unioned') }}
        JOIN visits USING(visit_source_value)
        LEFT JOIN {{ref('stg__source_to_concept_map__ccam_csarr')}} USING(source_value)
)

SELECT * FROM join_visit_concept 