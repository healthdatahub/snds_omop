-- Ce modèle sélectionne les variables des tables t_mcoaaA du FT utiles pour calculer visit_source_value associé à la visite pendant laquelle un actes a été réalisé, ainsi que celles qui alimenteront la partie procedure de la STEM_TABLE. 

WITH procedure_c_mco_a_ft AS
(
    {{
    procedure_pmsi_source_value(
        src_table = 'snds.c_mco_a_ft', 
        source_value = 'cdc_act', 
        src_base = "'mcoft'", 
        src_finess_nb = 'eta_num', 
        src_visit_nb = 'rsa_num',
        phase = 'pha_act',
        quantity = 'nbr_exe_act',
        delai = 'ent_dat_del'
    )
}}
)

SELECT * FROM procedure_c_mco_a_ft