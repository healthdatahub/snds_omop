-- Ce modèle sélectionne les variables des tables t_mcoaaFMSTC (ACE) utiles pour calculer visit_source_value associé à la visite pendant laquelle un actes a été réalisé, ainsi que celles qui alimenteront la partie procedure de la STEM_TABLE. 

WITH procedure_t_mcoaafmstc AS
(
    {{
    procedure_pmsi_source_value(
        src_table = 'snds.t_mco19_09fmstc', 
        source_value = 'ccam_cod', 
        src_base = "'mco19_09ace'", 
        src_finess_nb = 'eta_num', 
        src_visit_nb = 'seq_num',
        phase = 'pha_act', 
        delai = 'del_dat_ent' 
    )
}}
)

SELECT * FROM procedure_t_mcoaafmstc