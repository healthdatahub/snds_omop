{{ config(materialized='view') }}

WITH total_quantity AS

-- Ce modèle effectue les régularisations concernant le nombre d'actes effectués.

(
    SELECT 
        person_id,
        visit_occurrence_id,
        provider_id,
        source_value,
        source_concept_id, 
        concept_id,
        domain_id, 
        start_date,
        end_date,
    
        SUM(quantity)         AS quantity  
    FROM {{ref('procedure_pmsi_visit_concept__joined')}}
    GROUP BY 1,2,3,4,5,6,7,8,9

)

SELECT * FROM total_quantity