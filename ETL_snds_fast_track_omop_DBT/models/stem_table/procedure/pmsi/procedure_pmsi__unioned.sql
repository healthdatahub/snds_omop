{{ config(materialized='view') }}

-- Ce modèle sélectionne les codes, quantités et délais des actes des tables t_mcoaaA, c_mco_A_ft, t_mcoaaFMSTC, t_ssraaCCAM, t_ssraaCSARR, t_ssraaFMSTC, t_ripaaCCAM, t_hadaaA. Il construit aussi la variable visit_source_value correspondant aux identifiants de visites pendant lesquelles les actes ont été exécutés. 

WITH 
pmsi_procedure 
AS
(
{{
procedure_visit(
    src_table = ref('stg__t_mcoaaa_procedure')
    )
}}
UNION ALL
{{
procedure_visit(
    src_table = ref('stg__c_mco_a_ft_procedure')
    )
}}
UNION ALL
{{
procedure_visit(
    src_table = ref('stg__t_mcoaafmstc_procedure')
    )
}}
UNION ALL
{{
procedure_visit(
    src_table = ref('stg__t_ssraaccam_procedure')
    )
}}
UNION ALL
{{
procedure_visit(
    src_table = ref('stg__t_ssraacsarr_procedure')
    )
}}
UNION ALL
{{
procedure_visit(
    src_table = ref('stg__t_ssraafmstc_procedure')
    )
}}
UNION ALL
{{
procedure_visit(
    src_table = ref('stg__t_hadaaa_procedure')
    )
}}
UNION ALL
{{
procedure_visit(
    src_table = ref('stg__t_ripaaccam_procedure')
    )
}}
)

SELECT * FROM pmsi_procedure



