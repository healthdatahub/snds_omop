{{ config(materialized='table', schema='stem', alias = 'stem_procedure', indexes = [{'columns' : var('stem_table_key'), 'unique' : True} ]) }}

-- Ce modèle unit les actes provenant du PMSI et du DCIR, et sélectionne toutes les variables utiles de la STEM_TABLE, partie PROCEDURE. 

WITH 
all_procedures
AS 
(
    dbt_utils.union_relations(relation=[
        ref('procedure_pmsi_quantity__grouped'),
        ref('procedure_dcir_visit_concept__joined')
    ])
)


SELECT
    DISTINCT
    person_id      AS person_id,
    visit_occurrence_id     AS visit_occurrence_id,
    NULL::bigint            AS visit_detail_id,
    provider_id::BIGINT     AS provider_id,
    concept_id              AS concept_id,
    source_value            AS source_value, -- Code CCAM ou CSARR, issu du SNDS
    source_concept_id       AS source_concept_id,   -- Concept associé au code source
    32810                   AS type_concept_id,     
    start_date              AS start_date,
    (start_date||' 00:00:00')::timestamp   AS start_datetime,
    end_date                AS end_date,
    (end_date||' 00:00:00')::timestamp     AS end_datetime,
    domain_id               AS domain_id, -- Domaine du code standard
    NULL                    AS verbatim_end_date,
    NULL                    AS days_supply,
    NULL                    AS dose_unit_source_value,
    NULL                    AS lot_number,
    0                       AS modifier_concept_id,
    0                       AS modifier_source_value,       
    0                       AS operator_concept_id,
    quantity                AS quantity,            
    NULL                    AS range_high,
    NULL                    AS range_low,
    NULL                    AS refills,
    0                       AS route_concept_id,
    NULL                    AS route_source_value,
    NULL                    AS sig,
    NULL                    AS stop_reason,
    NULL                    AS unique_device_id,
    0                       AS unit_concept_id,
    NULL                    AS unit_source_value,
    0                       AS value_as_concept_id,
    NULL                    AS value_as_number,
    NULL                    AS value_as_string,
    NULL                    AS value_source_value,
    0                       AS anatomic_site_concept_id,
    0                       AS disease_status_concept_id,
    NULL                    AS specimen_source_id,
    NULL                    AS anatomic_site_source_value,
    0                       AS status_concept_id,
    NULL                    AS status_source_value,
    0                       AS qualifier_concept_id,
    NULL                    AS qualifier_source_value
FROM
    all_procedures
