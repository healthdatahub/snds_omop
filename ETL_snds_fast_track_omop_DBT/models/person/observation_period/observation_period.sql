{{ config(materialized='table', schema = 'omop', indexes = [{'columns' : ['observation_period_id'], 'unique' : True}]) }}


WITH person_visit_joined AS 
(
    -- Cette CTE calcule les dates de début et de fin de la période d'observation pour chaque patient
    -- La date de début correspond à la date de début de la 1ère visite dans la table VISIT_OCCURRENCE
    -- La date de fin correspond à la date de fin de la dernière visite dans la table VISIT_OCCURRENCE

    SELECT p.person_id,
       GREATEST(MIN(v.visit_start_date), {{"'"~var('min_observation_end_date')~"'"}}::date::date) AS start_date,
       COALESCE(MAX(v.visit_end_date), {{"'"~var('max_observation_end_date')~"'"}}::date) AS end_date,
    FROM {{ref('person')}} p
    LEFT JOIN {{ref('visit_occurrence')}} v USING(person_id)
    GROUP BY 1
)

SELECT 

    {{ set_id(['person_id', 'start_date', 'end_date' ]) }} AS observatiob_period_id,
    person_id::bigint AS person_id,
    start_date::date AS observation_period_start_date,
    end_date::date AS observation_period_end_date,
    44814724 AS period_type_concept_id

FROM person_prs__joined 