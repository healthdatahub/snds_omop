{{ config(materialized='view') }}

-- Ce modèle sélectionne les numéros finess juridiques des établissements du champ RIP contenus dans les tables t_ripaaE


SELECT DISTINCT
    eta_num::varchar(50) AS finess_j
    
FROM snds.t_rip19_09e