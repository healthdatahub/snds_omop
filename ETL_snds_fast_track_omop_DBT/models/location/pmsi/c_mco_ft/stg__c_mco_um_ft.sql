{{ config(materialized='view') }}

SELECT DISTINCT
    eta_num::varchar(50)     AS finess_j, 
    eta_num_geo::varchar(50) AS finess_geo
    
FROM snds.c_mco_um_ft