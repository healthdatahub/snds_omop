{{ config(materialized='view') }}


-- Ce modèle sélectionne les numéros finess juridiques et géographiques des établissements du MCO contenus dans les tables t_mcoaaUM

SELECT DISTINCT
    eta_num::varchar(50)     AS finess_j, 
    eta_num_geo::varchar(50) AS finess_geo
    
FROM snds.t_mco19_09um

