{{ config(materialized='view') }}

-- Ce modèle sélectionne les numéros finess juridiques des établissements du champ SSR contenus dans les tables t_SSRaaE

SELECT DISTINCT
    eta_num::varchar(50) AS finess_j
    
FROM snds.t_ssr19_09e

