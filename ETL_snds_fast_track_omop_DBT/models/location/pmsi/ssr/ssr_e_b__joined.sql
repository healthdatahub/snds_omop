{{ config(materialized='view') }}

-- Ce modèle joint les tables t_SSRaaE et t_SSRaaB pour sélectionner pour chaque établissement :
-- * le finess juridique
-- * le finess géographique (s'il n'existe pas, c'est que les numéros finess juridiques et géographiques sont les mêmes)

{{ join_tables_on_finess(table_e          = ref('stg__t_ssraae'),
                         table_2          = ref('stg__t_ssraab') ) }} 