{{ config(materialized='view') }}


WITH 
    finess_geo_mapped AS(
        SELECT *
        FROM {{ ref('finess_geo__mapped') }} ),
    
    pmsi_location AS(
        SELECT
            adresse                             AS address_1,
            NULL::varchar(50)                   AS address_2,
            lib_acheminement                    AS city,
            code_region                         AS state,
            com_code                            AS zip,
            SUBSTR(com_code, 1, 2)::varchar(20) AS county,
            finess_geo                          AS location_source_value 
            
            
        FROM finess_geo_mapped)
           
SELECT * FROM pmsi_location

