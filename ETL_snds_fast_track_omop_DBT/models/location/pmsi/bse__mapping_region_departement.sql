{{ config(materialized='view') }}

-- Ce model permet de trouver le code INSEE d'une région à partir d'un code commune ou d'un code département

WITH 
    correct_code_commune AS(
        SELECT
            code_region::varchar(2),
            nom_region,
            {{ correct_code_commune(code = 'com_code') }}::varchar(9) AS com_code,
            nom_commune
    
        FROM snds.correspondance_region_departement )
        
        
SELECT
    code_region,
    nom_region,
    com_code,
    {{ numero_departement(code_commune = 'com_code') }}::varchar(3) AS dpt_code,
    nom_commune
    
FROM correct_code_commune



