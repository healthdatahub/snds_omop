{{ config(materialized='view') }}


-- Ce modèle représente la partie de la table LOCATION concernant les patients.
-- Il sélectionne les codes communes associés aux lieux de résidence des patients qui sont ensuite joints au fichier de correspondance pour selectionner le code région à partir du code commune

WITH 
    ir_ben_r AS (
        SELECT * 
        FROM {{ ref('ir_ben_r__deduplicated') }}), 
        
    mapping_region_departement AS(
        SELECT *
        FROM {{ ref('bse__mapping_region_departement') }} ),  
        
    
    ir_ben_r_mapped AS(
        SELECT 
            i.code_commune,
            m.code_region

        FROM ir_ben_r i
        
        LEFT JOIN mapping_region_departement m
            ON i.code_commune = m.com_code),
        
        
    person_location AS(
        SELECT DISTINCT 
            null::varchar(50)                     as address_1,
            null::varchar(50)                     as address_2,
            null::varchar(50)                     as city,
            code_region                           as state,
            code_commune                          as zip,
            SUBSTR(code_commune,1,2)::varchar(20) as county,
            code_commune                          as location_source_value

    
        FROM ir_ben_r_mapped )
        
            
            
            
SELECT * FROM person_location




    
    