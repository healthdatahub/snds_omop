{{ config(materialized='table', 
          schema='omop',
          indexes=[
      {'columns': ['location_id'], 'unique': True}
    ]  ) }}

-- Table LOCATION

WITH 
    location AS ( 
        {{ dbt_utils.union_relations(
            relations=[
                ref('person_location__selected'),
                ref('dcir_location'),
                ref('pmsi_location') ]
            ) }})
        

SELECT
    DISTINCT
    {{ set_id(['location_source_value']) }}::bigint AS location_id,
    address_1::varchar(50)             AS address_1,
    address_2::varchar(50)             AS address_2,
    city::varchar(50)                  AS city,
    state::varchar(2)                  AS state,
    zip::varchar(50)                   AS zip,
    county::varchar(50)                AS county,
    location_source_value::varchar(50) AS location_source_value

FROM
        location