{{ config(materialized='view') }}

-- Ce model sélectionne les numéros finess des pharmacies provenant de la table er_pha_f

SELECT DISTINCT
    etb_pre_fin AS finess8  
FROM {{ ref('pha_prs__joined') }}      
            

