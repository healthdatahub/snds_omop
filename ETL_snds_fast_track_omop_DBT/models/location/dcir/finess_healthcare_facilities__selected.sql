{{ config(materialized='view') }}

-- Ce model sélectionne les numéros finess des établissements de santé provenant de la table er_ete_f. 
-- Ces numéros finess permettent d'identifier un établissement de manière unique, ils possèdent 8 caractères.
-- Il ne garde que les établissements dont les données ne remontent pas dans le PMSI grâce à la macro categorize_facilities()

WITH           
            
    healthcare_facilities_without_pmsis AS(
        SELECT 
            ete_cat_cod,
            etb_cat_rg1,
            etb_exe_fin AS finess8
    
        FROM {{ ref('ete_quantity__grouped') }} 
        {{ categorize_facilities() }}) -- Selectionne uniquement les établissements de santé dont les données ne remontent pas dans le PMSI
    
            
SELECT DISTINCT
    finess8
FROM healthcare_facilities_without_pmsis
