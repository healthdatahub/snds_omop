{{ config(materialized='view') }}

-- Ce modèle représente la partie de la table LOCATION concernant le DCIR
WITH 

    dcir_mapped AS(
        SELECT *
        FROM {{ ref('healthcare_facilities_pharmacies__mapped') }} ),
            
    dcir_location AS(
        SELECT DISTINCT
            adresse                             AS address_1,
            NULL::varchar(50)                      AS address_2,
            lib_acheminement                    AS city,
            code_region                         AS state,
            com_code                            AS zip,
            SUBSTR(com_code, 1, 2)::varchar(20) AS county,
            finess8                             AS location_source_value 
            
            
        FROM dcir_mapped )
            
            
SELECT * FROM dcir_location