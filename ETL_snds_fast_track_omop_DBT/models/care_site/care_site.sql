{{ config(materialized='table', 
          schema='omop',
          indexes=[ {'columns': ['care_site_id'], 'unique':True }]) }}

-- Ce modèle unit les parties PMSI et DCIR de la table CARE_SITE. Il représente la table CARE_SITE

WITH 

    care_sites_pmsi_dcir AS ( 
        {{ dbt_utils.union_relations(
                relations = [
                    ref('dcir_care_site__joined'),
                    ref('pmsi_care_site__joined') ] 
            ) }} ),
            

    care_sites AS(
    
        SELECT
                DISTINCT
                {{set_id(['care_site_source_value'])}}::bigint AS care_site_id,
                location_id::bigint                            AS location_id,
                care_site_source_value::varchar(50)            AS care_site_source_value,
                MAX(care_site_name)::varchar(255)              AS care_site_name, -- Cas où il existerait deux raisons sociales différentes pour un seul numéro finess (finess juridique = finess géographique)
                place_of_service_source_value::varchar(50)     AS place_of_service_source_value,
                place_of_service_concept_id::bigint            AS place_of_service_concept_id
        
        FROM
                care_sites_pmsi_dcir 
        
        GROUP BY 1,2,3,5,6 )



SELECT * FROM care_sites