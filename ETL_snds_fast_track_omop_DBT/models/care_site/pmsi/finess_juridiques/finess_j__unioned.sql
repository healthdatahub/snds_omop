{{ config(materialized='view') }}

-- Ce model sélectionne les finess juridiques et enlève les doublons
WITH 
    finess_j__unioned AS(
        {{ dbt_utils.union_relations(
            relations=[
                    ref('stg__t_mcoaae'),
                    ref('stg__t_hadaae'),
                    ref('stg__t_ssraae'),
                    ref('stg__t_ripaae'),
                    ref('stg__c_mco_e_ft')
            ] ) }} ),
            
    exclude_finess AS(
        SELECT * 
        FROM finess_j__unioned fgu
    
        WHERE NOT EXISTS(
                SELECT *
                FROM snds.finess_exclusion_list fel
                WHERE fel.finess_to_exclude = fgu.finess_j))
            
            
SELECT * FROM exclude_finess
