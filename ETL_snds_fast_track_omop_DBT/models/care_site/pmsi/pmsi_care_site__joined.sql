{{ config(materialized='view') }}

-- Ce model représente la table CARE_SITE pour les établissements du PMSI. 

WITH    
   
    finess AS (
        SELECT *
        FROM {{ ref('finess_geo_finess_j__unioned')  }}),
        
    
    location AS (
        SELECT 
            location_id,
            location_source_value
        FROM {{ ref('location') }}),
        
   
    pmsi_care_site AS(

        SELECT DISTINCT
                loc.location_id::bigint          AS location_id,
                fin.finess::varchar(50)          AS care_site_source_value,
                fin.care_site_name::varchar(255) AS care_site_name,
                NULL::varchar(50)                AS place_of_service_source_value,
                0::integer                       AS place_of_service_concept_id
        
        FROM finess fin
        
        LEFT JOIN location loc
            ON fin.finess = loc.location_source_value)

SELECT * FROM pmsi_care_site 