{{config(materialized='table', indexes=[
      {'columns': ['dcir_visit_id', 'pha_prs_c13'], 'unique': False}
    ])}}
-- Ce model réalise les ragularisations  pour les médicaments 
-- On récupère également l'unité de la dose unitaire du médicament (mg, ml) de la nomenclature IR_PHA_R

WITH 
fix_drug_quantity AS
(
    SELECT 
        a.dcir_visit_id,
        a.pha_prs_c13,
        f.pha_dos_unt_dses, -- unité du médicament 
        a.pre_pre_dtd,
        a.pfs_exe_num,
        a.psp_spe_cod,
        a.psp_act_nat,
        a.etb_pre_fin,
    
        {{ drug_cases() }} 
        -- computes drug quantity with ir_pha_r :  
        -- depends on how the number of doses is filled in the vocabulary (variable pha_unt_nbr_dses)
        
    FROM 
        {{ref('pha_prs__joined')}} a
        LEFT JOIN snds.ir_pha_r f ON a.pha_prs_c13 = f.pha_cip_c13::varchar(50) -- jointure avec IR_PHA_R 
    WHERE a.pha_prs_c13 != '0'
    GROUP BY 1, 2, 3, 4, 5, 6, 7, 8,f.pha_unt_nbr_dses
        
)

SELECT * FROM fix_drug_quantity