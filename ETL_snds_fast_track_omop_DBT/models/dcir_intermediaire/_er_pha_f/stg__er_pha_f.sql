{{config(materialized='view')}}

-- Ce model sert à récupérer les variables d'intérêt d'er_pha_f et à l'union de toutes les années

WITH 
union_er_pha_f_years
AS
(
    SELECT
        pha_prs_c13::varchar(50),
        COALESCE(pha_act_qsn::integer, 0) AS pha_act_qsn,
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
    FROM snds.er_pha_f_2019
    UNION ALL
    SELECT
        pha_prs_c13::varchar(50),
        pha_act_qsn::integer,
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
    FROM snds.er_pha_f_2020
)

SELECT * FROM union_er_pha_f_years WHERE pha_prs_c13 != '0' -- On supprime les lignes pour lesquelles le code cip 13 est inconnu 