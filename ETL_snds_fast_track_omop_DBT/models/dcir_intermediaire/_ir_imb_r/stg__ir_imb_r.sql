{{config(materialized = 'view')}}

-- Ce modèle sélectionne les diagnostics d'ALD dans la table IR_IMB_R, l'identifiant des personnes concernées et les dates associées. 
WITH 
_ir_imb_r AS 
(
    SELECT
        TRIM(med_mtf_cod)::varchar(20)              AS med_mtf_cod, -- Motif médical ou pathologie (code CIM10)
        {{format_date('imb_ald_dtd')}}              AS imb_ald_dtd,                         
        {{format_date('imb_ald_dtf')}}              AS imb_ald_dtf,
        {{format_date('ins_dte')}}                  AS ins_dte, -- date d'insertion
        {{format_date('upd_dte')}}                  AS upd_dte, -- date de MAJ
        imb_etm_nat::varchar(50), -- Motif d'exonération du bénéficiaire
        num_enq::varchar(100)                  
    FROM
        snds.ir_imb_r 
)

SELECT * FROM _ir_imb_r