{{config(materialized = 'view')}}

WITH build_code_commune AS 
(
    SELECT
            num_enq, -- ben_nir_psa
            ben_sex_cod,
            ben_nai_ann,
            ben_nai_moi,
            ben_dte_maj,
            ben_dcd_dte,
            
            CASE
                    WHEN (-- mettre à jour les règles
                                -- Cas général
                            ben_res_dpt <> '209'
                            AND SUBSTR(ben_res_dpt, 1, 2) <> '97'
                            ) THEN SUBSTR(ben_res_dpt, 2, 2) || ben_res_com
                    WHEN (
                                -- Corse
                            ben_res_dpt = '209'
                            ) THEN SUBSTR(ben_res_dpt, 1, 2) || ben_res_com
                    WHEN (
                                -- DOM pour MSA
                            SUBSTR(org_aff_ben, 1, 3) = '02A'
                            AND SUBSTR(ben_res_dpt, 1, 2) = '97'
                            ) THEN SUBSTR(ben_res_dpt, 1, 2) || ben_res_com
                    WHEN (
                                -- DOM pour RSI
                            SUBSTR(org_aff_ben, 1, 3) = '03A'
                            AND SUBSTR(ben_res_dpt, 1, 2) = '97'
                            ) THEN (
                                ben_res_dpt || SUBSTR(ben_res_com, 2, 2) -- ben_res_dpt est sur trois caractères pour les DOM TOM 
                            )
            END code_commune -- scripts CNAM sur la documentation SNDS du HDH 
            
    FROM
            {{ref('stg__ir_ben_r')}}
)

SELECT * FROM build_code_commune