{{config(materialized='table', indexes=[
      {'columns': ['dcir_key_id'], 'type': 'hash'}
    ])}}

-- Ce model sert à joindre la table er_ete_f à er_prs_f et à récupérer les variables de er_prs_f qui seront utilisée dans les tables  
-- OMOP alimentées par ER_ETE_F. 
-- On joint également à la nomenclature ir_cet_v qui permet de repérer les établissements dont les données remontent dans le PMSI.      
    
WITH ete_join_prs
AS
(
    SELECT 
        ete.prs_ppu_sec,
        ete.etb_exe_fin,
        ete.ete_cat_cod,
    
        prs.etb_pre_fin,
        prs.pse_spe_cod,
        prs.pse_act_nat,
        prs.num_enq,
        prs.exe_soi_dtd,
        prs.exe_soi_dtf,
        prs.prs_act_qte, 
        prs.dcir_key_id,
        prs.dcir_visit_id,
        prs.prs_nat_ref,
    
        cet.etb_cat_rg1 -- catégorie de régime qui permet de repérer les établissements du PMSI 
    FROM {{ref('stg__er_ete_f')}} ete
    JOIN {{ ref('bse__er_prs_f')}} prs USING(dcir_key_id)  
    JOIN snds.ir_cet_v cet ON ete.ete_cat_cod::text = cet.etb_cat_cod  
)

SELECT * FROM ete_join_prs

