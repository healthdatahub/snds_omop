{{config(materialized='table', indexes=[
      {'columns': ['dcir_key_id'], 'type': 'hash'}
    ])}}
    
-- Ce modèle joint la table ER_TIP_F à la table ER_PRS_F

WITH 
tip_prs_joined
AS 
(
    SELECT 
        a.tip_prs_ide,
        a.tip_acl_dtd,
        a.tip_acl_dtf,
        a.tip_act_qsn,
        a.tip_prs_typ,
    
        b.exe_soi_dtd,
        b.prs_act_qte, 
        b.pfs_exe_num,
        b.etb_pre_fin,
        b.dcir_key_id,
        b.dcir_visit_id,
        b.prs_pai_mnt, -- Regarder intéret de ces variables de coût
        b.bse_rem_mnt,
        b.cpl_rem_mnt
    FROM 
        {{ref('stg__er_tip_f')}} a
        JOIN {{ref('bse__er_prs_f')}} b USING(dcir_key_id)
)

 
SELECT * FROM tip_prs_joined