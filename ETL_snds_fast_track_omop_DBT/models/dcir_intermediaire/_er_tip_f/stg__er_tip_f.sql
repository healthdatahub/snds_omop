{{config(materialized='view')}}

-- Ce modèle sélectionne les variables pertinentes de la table ER_TIP_F qui alimenteront les tables OMOP.

WITH 
union_er_tip_f_years
AS
(
    SELECT
        tip_prs_ide::varchar(50), -- Code LPP du dispositif médical
        {{ format_date('tip_acl_dtd') }} AS tip_acl_dtd,
        {{ format_date('tip_acl_dtf') }} AS tip_acl_dtf,
        tip_act_qsn::integer, -- Nombre de dispositifs
        tip_prs_typ::varchar(50), -- Type de prestation (Achat, location, réparation, etc.)
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
        
    
    FROM snds.er_tip_f_2019
    UNION ALL
    SELECT
        tip_prs_ide::varchar(50),
        {{ format_date('tip_acl_dtd') }} AS tip_acl_dtd,
        {{ format_date('tip_acl_dtf') }} AS tip_acl_dtf,
        tip_act_qsn::integer,
        tip_prs_typ::varchar(50),
        {{ set_id(var('dcir_key')) }} AS dcir_key_id
        
    
    FROM snds.er_tip_f_2020
)
 
SELECT * FROM union_er_tip_f_years