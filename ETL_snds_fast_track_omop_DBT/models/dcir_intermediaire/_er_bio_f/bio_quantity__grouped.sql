{{config(materialized='table', indexes=[
      {'columns': ['dcir_visit_id', 'bio_prs_ide'], 'unique': False}
    ])}}

-- Ce model sert à réaliser les régularisations dans la table er_bio_f. 
-- On somme les quantités en groupant par identifiant de visit dcir_visit_id. 
-- On supprime ainsi les lignes en double dues à des corrections dans er_prs_f. 

WITH fix_bio_quantity 
 
AS
(
    SELECT 
        bio_prs_ide,
        etb_pre_fin,
        pfs_exe_num,
        num_enq,
        exe_soi_dtd,
        dcir_visit_id,
    
        SUM(prs_act_qte) AS quantity
    FROM 
        {{ref('bio_prs__joined')}}
    GROUP BY 1,2,3,4,5,6 
)


SELECT * from fix_bio_quantity 