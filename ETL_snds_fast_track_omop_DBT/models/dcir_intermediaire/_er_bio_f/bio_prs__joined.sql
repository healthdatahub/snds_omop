{{config(materialized='table', indexes=[
      {'columns': ['dcir_key_id'], 'type': 'hash'}
    ])}}

-- Ce modèle joint la table ER_BIO_F à la table ER_PRS_F   
    
WITH bio_join_prs
AS
(
    SELECT 
        a.bio_prs_ide,
        b.etb_pre_fin,
        b.pfs_exe_num,
        b.num_enq,
        b.exe_soi_dtd,
        b.prs_act_qte, 
        b.dcir_key_id,
        b.dcir_visit_id
    FROM {{ref('stg__er_bio_f')}} a
    JOIN {{ ref('bse__er_prs_f')}} b USING(dcir_key_id)        
)

SELECT * FROM bio_join_prs

