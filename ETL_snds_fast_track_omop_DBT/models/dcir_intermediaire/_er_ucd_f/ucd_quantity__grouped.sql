{{config(materialized='table', indexes=[
      {'columns': ['dcir_visit_id'], 'type': 'hash'}
    ])}}

-- Ce modèle prend en compte les régularisations dans le SNDS au niveau des quantités de prestations et des dates.  

WITH 
fix_ucd_quantity 
AS
(
    SELECT
        ucd_ucd_cod,
        psp_spe_cod,
        psp_act_nat,
        pse_spe_cod,
        etb_pre_fin,
        pre_pre_dtd,
        pfs_exe_num,
        exe_soi_dtd,
        exe_soi_dtf,
        dcir_visit_id,
    
        SUM(ucd_dlv_nbr) AS quantity

    FROM 
        {{ref('ucd_prs__joined')}}
    
    GROUP BY 1, 2,3,4,5,6,7,8,9, 10
)


SELECT * FROM fix_ucd_quantity 