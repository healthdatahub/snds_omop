{{config(materialized='table', indexes=[
      {'columns': ['dcir_key_id'], 'type': 'hash'}
    ])}}

-- Ce modèle joint la table ER_UCD_F à la table ER_PRS_F

WITH 
ucd_prs_joined 
AS
(
    SELECT
        a.ucd_ucd_cod, -- Code UCD-13 du médicament
        a.ucd_dlv_nbr*sign(b.prs_act_qte) AS ucd_dlv_nbr, -- Régularisation du nombre de doses
        b.prs_act_qte,
    
        b.psp_spe_cod,
        b.psp_act_nat,
        b.pse_spe_cod,
        b.etb_pre_fin,
        b.pre_pre_dtd,
        b.pfs_exe_num,
        b.exe_soi_dtd,
        b.exe_soi_dtf,
        b.dcir_key_id,
        b.dcir_visit_id
    FROM 
        {{ref('stg__er_ucd_f')}} a
        JOIN {{ ref('bse__er_prs_f')}} b USING(dcir_key_id) 
)


SELECT * FROM ucd_prs_joined 