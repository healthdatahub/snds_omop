{{ config(materialized='view') }}

-- Ce modèle utilise la macro clean_table_cstc 
-- Il retire les lignes non valides 
-- Adapte le format des dates 
-- Sélectionne les variables qui permettent de calculer visit_source_value 

{{ clean_table_cstc(table_cstc = 'snds.t_mco19_09cstc',
                    base       = "'mco19_09ace'",
                    finess_j   = 'eta_num',
                    visit_nb   = 'seq_num') }}
                 
