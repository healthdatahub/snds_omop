{{ config(materialized='view') }}
        
-- Ce model unit tous les identifiants de visite des produits du PMSI et crée l'identifiant d'une visite dans OMOP : visit_source_value.

WITH
    tables_pmsi__unioned AS(
        {{ dbt_utils.union_relations(
            relations=[
                ref('stg__t_hadaac_visit'),
                ref('stg__t_ssraac_visit'),
                ref('stg__t_mcoaac_visit'),
                ref('stg__t_ripaac_visit'),
                ref('stg__c_mco_c_ft_visit'),
                
                ref('stg__t_mcoaacstc_visit'),
                ref('stg__t_ssraacstc_visit')
            ] ) }} ),
            
            
            
    variables_for_visit__selected AS(        
        SELECT DISTINCT
            num_enq                                             AS person_source_value,
            visit_concept_id,
            exe_soi_dtd                                         AS visit_start_date,
            exe_soi_dtf                                         AS visit_end_date,
            finess_j                                            AS care_site_source_value,
            {{ visit_source_value(src_base = 'base',
                                  src_finess_nb = 'finess_j',
                                  src_visit_nb = 'visit_nb') }} AS visit_source_value


        FROM tables_pmsi__unioned
        WHERE exe_soi_dtd IS NOT NULL) -- On garde uniquement les visites qui ont une date de début remplie
        
        
SELECT * FROM variables_for_visit__selected
    
    
    
    
