{{ config(materialized='table', indexes = [{'columns' : ['visit_occurrence_id'], 'unique' : True} ] ) }}

-- Ce model représente la partie de VISIT_OCCURRENCE correspondant au PMSI.

WITH 
    pmsi_omop AS(
        SELECT 
            person_id,
            care_site_id,
            visit_start_date,
            visit_end_date,
            visit_concept_id,
            visit_source_value
        
        FROM {{ ref('pmsi_omop__joined') }}),
        
        
    visit_pmsi_inpatient AS(
        
        SELECT 
            {{set_id(['visit_source_value'])}}::bigint        AS visit_occurrence_id,
            person_id::bigint                                 AS person_id,
            visit_concept_id::integer                         AS visit_concept_id,
            visit_start_date::date                            AS visit_start_date,
            (visit_start_date::text||' 00:00:00')::timestamp  AS visit_start_datetime,
            visit_end_date::date                              AS visit_end_date,
            (visit_end_date::text||' 00:00:00')::timestamp    AS visit_end_datetime,
            32810                                             AS visit_type_concept_id,
            care_site_id::bigint                              AS care_site_id,
            visit_source_value::text                          AS visit_source_value,
            NULL::bigint                                      AS provider_id,
            0                                                 AS visit_source_concept_id,
            0                                                 AS admitting_source_concept_id,
            NULL::varchar(50)                                 AS admitting_source_value,
            0                                                 AS discharge_to_concept_id,
            NULL::varchar(50)                                 AS discharge_to_source_value,
            NULL::bigint                                      AS preceding_visit_occurrence_id
        
        FROM pmsi_omop)
        
SELECT * FROM visit_pmsi_inpatient