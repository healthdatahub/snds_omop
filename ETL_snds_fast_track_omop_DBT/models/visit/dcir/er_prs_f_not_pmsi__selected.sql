{{config(materialized = 'view')}}

-- Ce modèle sélectionne les visites provenant de la table er_prs_f qui ne sont pas dans la table er_ete_f. Il ne garde que les visites utilisées dans des établissements dont les données ne remontent pas dans le PMSI.

WITH 
relevant_columns_er_prs_f AS 
(
    SELECT 
        num_enq                                  AS person_source_value, 
        {{
            visit_source_value(
                src_base= "'dcir19_20'", 
                src_finess_nb= 'etb_pre_fin', 
                src_visit_nb ='dcir_visit_id'
            )
        }}                                       AS visit_source_value,
        etb_pre_fin                              AS care_site_source_value,
        pse_spe_cod||'_'||pse_act_nat            AS provider_source_value,
        exe_soi_dtd                              AS visit_start_date, 
        COALESCE(exe_soi_dtf, exe_soi_dtd)       AS visit_end_date,
        prs_nat_ref, 
        dcir_key_id

    FROM {{ref('bse__er_prs_f')}}

    WHERE exe_soi_dtd IS NOT NULL 
        
),

relevant_columns_er_ete_f AS
(
    SELECT 
        dcir_key_id
    FROM
        {{ref('ete_prs__joined')}}
),

rows_not_in_er_ete_f AS (
    SELECT 
        DISTINCT
        prs.person_source_value, 
        prs.visit_source_value,
        prs.care_site_source_value,
        prs.provider_source_value,
        prs.visit_start_date, 
        prs.visit_end_date,
    
        ete.prs_nat_ref 
    
    FROM relevant_columns_er_prs_f prs

    LEFT JOIN relevant_columns_er_ete_f ete
        USING(dcir_key_id)

    WHERE 
        ete.dcir_key_id IS NULL -- On sélectionne uniquement les prestations qui sont dans er_prs_f_, mais pas dans er_ete_f
        
)

SELECT * FROM rows_not_in_er_ete_f

