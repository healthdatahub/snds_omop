{{config(materialized = 'view')}}

-- Ce modèle sélectionne les visites provenant de la table er_ete_f. Il ne garde que les visites utilisées dans des établissements dont les données ne remontent pas dans le PMSI.

WITH 
drop_duplicated_data_from_pmsi AS (
    SELECT 
        num_enq                              AS person_source_value, 
        {{
            visit_source_value(
                src_base= "'dcir19_20'", 
                src_finess_nb= 'etb_pre_fin', 
                src_visit_nb ='dcir_visit_id'
            )
        }}                                   AS visit_source_value,
        etb_pre_fin                          AS care_site_source_value,
        pse_spe_cod||'_'||pse_act_nat        AS provider_source_value,
        exe_soi_dtd                          AS visit_start_date, 
        COALESCE(exe_soi_dtf, exe_soi_dtd)   AS visit_end_date,
    
        prs_nat_ref

    FROM 
        {{ref('ete_quantity__grouped')}}
        
    {{ categorize_facilities() }} -- Filtre qui enlève les visites associées à des établissements dont les données remontent dans le PMSI
)

SELECT * 
FROM drop_duplicated_data_from_pmsi 
WHERE care_site_source_value IS NOT NULL 

