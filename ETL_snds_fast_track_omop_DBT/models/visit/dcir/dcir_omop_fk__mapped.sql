{{config(materialized = 'table', indexes = [{'columns' : ['visit_source_value'], 'unique' : True} ])}}

-- Ce modèle joint le modèle prs_ete_not_pmsi__unioned (variables identifiant une visite dans le DCIR) avec les tables tables PERSON, CARE_SITE et PROVIDER pour récuperer respectivement les variables person_id, care_site_id et provider_id.

WITH 
foreign_key_mapping AS
(
    SELECT  
        prs_ete.visit_source_value,
        prs_ete.visit_start_date, 
        prs_ete.visit_end_date,
    
        pers.person_id,
    
        cs.care_site_id,
    
        pro.provider_id,
    
        prs_ete.prs_nat_ref
    FROM 
        {{ ref('prs_ete_not_pmsi__unioned') }} prs_ete

        INNER JOIN {{ref('person')}} pers USING(person_source_value)
        LEFT JOIN {{ref('care_site')}} cs USING(care_site_source_value)
        LEFT JOIN {{ref('provider')}} pro USING(provider_source_value)
        
)

SELECT * FROM foreign_key_mapping