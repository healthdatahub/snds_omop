{{ config(materialized='view', schema='provider') }}

-- Ce modèle sélectionne les différentes sécialités médicales des professionnels de santé executants suivis par "_0" 


WITH 
    mapping_pse_spe_cod AS (
        SELECT 
            conceptId,
            sourceCode 
        FROM snds.mapping_specialties),
    
    ir_spe_v AS (
        SELECT pfs_spe_cod 
        FROM snds.ir_spe_v)


SELECT
    spe.pfs_spe_cod         AS specialty_source_value,
    spe.pfs_spe_cod || '_0' AS provider_source_value,
    
    CASE
        WHEN map.conceptId IS NULL THEN 0
        ELSE map.conceptId
    END  specialty_concept_id
    
    
FROM ir_spe_v spe

    LEFT JOIN mapping_pse_spe_cod map ON map.sourceCode = spe.pfs_spe_cod
    
WHERE spe.pfs_spe_cod NOT IN (0,99)