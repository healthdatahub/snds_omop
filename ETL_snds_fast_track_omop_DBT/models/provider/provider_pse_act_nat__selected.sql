{{ config(materialized='view', schema='provider') }}

-- Ce modèle sélectionne les différentes natures d'activités des professionnels de santé exécutants précédés par "0_"

WITH 
    mapping_pse_act_nat AS (
        SELECT 
            conceptId,
            sourceCode 
        FROM snds.mapping_pse_act_nat),
    
    ir_act_v AS (
        SELECT pfs_act_nat 
        FROM snds.ir_act_v)


SELECT
    act.pfs_act_nat         AS specialty_source_value,
    '0_' || act.pfs_act_nat AS provider_source_value,
    
    CASE
        WHEN map.conceptId IS NULL THEN 0
        ELSE map.conceptId
    END  specialty_concept_id
    
    
FROM ir_act_v act

    LEFT JOIN mapping_pse_act_nat map ON map.sourceCode::text = act.pfs_act_nat
    
WHERE act.pfs_act_nat NOT IN ('0','99')