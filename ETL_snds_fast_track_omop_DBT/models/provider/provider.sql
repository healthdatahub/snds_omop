{{ config(materialized='table', 
          schema='omop',
         indexes=[
      {'columns': ['provider_id'], 'unique': True}
    ]) }}

    -- Ce modèle représente la table PROVIDER

WITH     
    
    provider_dcir_union AS (
        {{ dbt_utils.union_relations(
            relations = [
                ref('provider_pse_spe_cod__selected'),
                ref('provider_pse_act_nat__selected'),
                ref('unknown__selected')]
            ) }} ), 
        
        

    provider AS(
        SELECT DISTINCT
            {{set_id(['provider_source_value'])}}::bigint AS provider_id,
            provider_source_value::varchar(50)            AS provider_source_value,
            0                                             AS specialty_source_concept_id,
            NULL::varchar(255)                            AS provider_name,
            NULL::varchar(20)                             AS npi,
            NULL::varchar(20)                             AS dea,
            specialty_concept_id::integer                 AS specialty_concept_id,
            NULL::bigint                                  AS care_site_id,
            NULL::integer                                 AS year_of_birth,
            0                                             AS gender_concept_id,
            specialty_source_value::varchar(50)           AS specialty_source_value,
            NULL::varchar(50)                             AS gender_source_value,
            0                                             AS gender_source_concept_id
        
        FROM
                provider_dcir_union)
                
                
                
SELECT * FROM provider