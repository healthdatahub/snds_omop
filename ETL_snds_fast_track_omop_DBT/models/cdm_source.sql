{{config(materialized='table', schema='omop')}}

SELECT 
    'Système National des DonnÃ©es de Santé' AS cdm_source_name,
    'SNDS' AS cdm_source_abbreviation,
    'Health Data Hub' AS cdm_holder,
    'https://documentation-snds.health-data-hub.fr/' AS source_description,
    0 AS source_documentation_reference,
    0 AS cdm_etl_reference,
    '2020-01-01'::date AS source_release_date,
    '2022-01-01'::date AS cdm_release_date,
    'v5.3.1' AS cdm_version,
    'v5.0 09-APR-22' AS vocabulary_version
