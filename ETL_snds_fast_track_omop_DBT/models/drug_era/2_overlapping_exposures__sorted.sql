{{ config(materialized='table') }}

WITH 
start_dates AS 
(
    SELECT 
        person_id, 
        ingredient_concept_id, 
        drug_exposure_start_date AS event_date,
        -1 AS event_type,
        ROW_NUMBER() OVER (
            PARTITION BY person_id, ingredient_concept_id
            ORDER BY drug_exposure_start_date
        ) AS start_ordinal
    FROM {{ref('1_drug_exposure_end_dates__normalized')}}

),
end_dates AS 
(
    SELECT 
        person_id, 
        ingredient_concept_id, 
        drug_exposure_end_date AS event_date, 
        1 AS event_type, 
        NULL::bigint AS start_ordinal 
    FROM {{ref('1_drug_exposure_end_dates__normalized')}}
),
all_dates AS 
(
    SELECT * FROM start_dates 
    UNION ALL
    SELECT * FROM end_dates
),

event_dates__sorted AS
(
    SELECT 
        person_id, 
        ingredient_concept_id, 
        event_date, 
        event_type,
        MAX(start_ordinal) OVER (
            PARTITION BY person_id, ingredient_concept_id
            ORDER BY event_date, event_type ROWS unbounded preceding
        ) AS start_ordinal,
        -- this pulls the current START down from the prior rows so that the NULLs
        -- from the END DATES will contain a value we can compare with
        ROW_NUMBER() OVER (
            PARTITION BY person_id, ingredient_concept_id
            ORDER BY event_date, event_type
        ) AS overall_ord
            -- this re-numbers the inner UNION so all rows are numbered ordered by the event date
        FROM all_dates
),

cteSubExposureEndDates (person_id, ingredient_concept_id, end_date) AS --- A preliminary sorting that groups all of the overlapping exposures into one exposure so that we don't double-count non-gap-days
(
    SELECT 
        person_id, 
        ingredient_concept_id,
        event_date AS end_date
    FROM event_dates__sorted
    WHERE (2 * start_ordinal) - overall_ord = 0
)

SELECT * FROM cteSubExposureEndDates
