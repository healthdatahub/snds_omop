{{ config(materialized='table') }}

SELECT 
    c.concept_id AS ingredient_concept_id,
    ca.descendant_concept_id
FROM snds_omop.concept_ancestor ca 
JOIN snds_omop.concept c ON ca.ancestor_concept_id = c.concept_id
WHERE c.vocabulary_id = 'RxNorm' ---8 selects RxNorm from the vocabulary_id
AND c.concept_class_id = 'Ingredient'