{% macro procedure_pmsi_source_value(src_table, source_value, src_base, src_finess_nb, src_visit_nb, phase, delai, quantity=1) %}

-- Cette macro contient du code répété qui ne peut être réduit. Il requête les tables du PMSI qui alimenteront la table PROCEDURE_OCCURRENCE
-- Cette macro fait appel à la macro visit_source_value
-- src_base est un champ du PMSI (MCO, HAD, SSR, RIP)
-- src_table est la table dans laquelle sont informations d'intérêt
-- quantity est la variable qui indique le nombre de procédures dans la table erc_table, par défaut, elle vaut 1
-- delai est le nombre de jours passés entre l'exécution de la procédure et le début de la visite, si cette colonne n'existe pas, delai vaut 0
-- Une procedure peut avoir plusieurs phases
-- src_finess_nb est le numéro finess juridique de l'établissement dans lequel est exécuté la visite dans src_table
-- src_visit_nb est l'identifiant d'une visite dans l'établissement associé au src_finess_nb 

SELECT
    {{source_value}}                AS source_value,
    {{quantity}}                    AS quantity,
    {{src_base}}      AS src_base,
    {{src_finess_nb}} AS src_finess_nb,
    {{src_visit_nb}}  AS src_visit_nb,
    coalesce({{delai}},0)           AS delai
FROM {{src_table}}

{% if phase is not none %}
    WHERE {{phase}} IN (0,1) -- Si la procédure possède plusieurs phases, on garde seulement la 1ère (phase = 1) pour éviter les doublons. Si la procédure ne possède qu'une seule phase, (phase = 0)
{% endif %}

{% endmacro %}




{% macro procedure_visit(src_table) %}

-- Cette macro construit le visit_source_value associé à la visite pendant laquelle une procédure a eu lieu.

SELECT 
    DISTINCT 
    source_value AS source_value, -- Code source, issu du SNDS
    quantity, -- Nombre d'actes
    delai, -- Délai par rapport au début de la visite
    {{visit_source_value('src_base', 'src_finess_nb', 'src_visit_nb')}} AS visit_source_value 
FROM 
    {{src_table}}
    
{% endmacro %}