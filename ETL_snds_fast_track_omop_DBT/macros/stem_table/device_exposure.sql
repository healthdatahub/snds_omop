{% macro device_exposure_pmsi(source_value, quantity, delai, src_base, src_finess_nb, src_visit_nb, src_table) %}

-- Cette macro sélectionne la variable de la table src_table qui contient le code LPP des dispositifs médicaux (source_value)
-- quantity est la variable qui indique le nombre de dispositifs médicaux posés ou achetés, si la colonne n'existe pas, indiquer quantity = 1
-- delai est le nombre de jours passés entre le début de la visite et la pose ou l'achat d'un dispositif médical, si la colonne n'existe pas, indiquer delai = 0
-- src_base est un produit du PMSI (MCO, HAD, SSR, RIP)
-- src_table est la table dans laquelle on cherche les dispositifs médicaux
-- src_finess_nb est la variable qui contient le numéro finess juridique des établissements où sont posés ou achetés les dispositifs médicaux dans la table src_table
-- src_visit_nb est l'identifiant d'une visite dans l'établissement associé au src_finess_nb

SELECT
    {{ source_value }}::text            AS source_value, -- Code LPP du dispositif  
    {{ delai }}                         AS delai, -- Nombre de jours écoulés depuis le début de la visite
    {{ quantity }}                      AS quantity, -- Nombre de dispositifs

    {{ visit_source_value(
        src_base      = src_base, 
        src_finess_nb = src_finess_nb, 
        src_visit_nb  = src_visit_nb) }} AS visit_source_value -- Identifiant d'une visite dans le PMSI

FROM {{ src_table }}    

                          
{% endmacro %}