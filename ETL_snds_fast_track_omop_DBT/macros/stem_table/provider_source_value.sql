{% macro provider_source_value( ps_spe_cod, ps_act_nat) %}

-- Cette macro concatène les variables ps_spe_cod et ps_act_nat pour former l'identifiant d'un professionnel de santé provider_source_value

{{ ps_spe_cod }}::text ||'_'|| {{ ps_act_nat }}::text 

{% endmacro %}