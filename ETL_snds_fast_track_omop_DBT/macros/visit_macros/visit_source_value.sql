{% macro visit_source_value( src_base, src_finess_nb, src_visit_nb) %}

-- Cette macro calcule l'identifiant unique d'une visite dans le SNDS. Il s'agit de la concaténation des variables suivantes :
-- src_base est un champ du PMSI (MCO, HAD, SSr, RIP)
-- src_finess_nb est la variable qui contient le numéro finess juridique identifiant un centre de soin dans src_table
-- src_visit_nb est l'identifiant unique d'une visite dans src_table

{{ src_base }} ||'_'|| COALESCE({{src_finess_nb }}, '0') ||'_'|| {{ src_visit_nb }}

{% endmacro %}