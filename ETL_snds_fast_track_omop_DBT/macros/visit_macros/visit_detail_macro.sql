{%macro visit_detail_pmsi(src_table, src_base, src_finess_nb, src_visit_nb, src_detail_nb, nb_jrs, um_typ) %}

-- Cette macro contient du code répétitif qui ne peut pas être réduit. Il requête les différentes tables du PMSI qui alimentent la table OMOP VISIT_DETAIL
-- Elle appelle la macro visit_source_value
-- src_base est un champ du PMSI (MCO, HAD, SSR, RIP)*
-- src_table est la table dans laquelle sont les informations
-- um_typ est la variable qui contient le code de l'unité médicale dans la src_table
-- nbr_jrs est le nombre de jours passés dans l'unité médicale
-- src_finess_nb est la variable qui contient le numéro finess juridique du centre de soin
-- src_visit_nb est l'identifiant unique d'une visite dans la src_table
-- src_detail_nb est l'identifiant de la visite détailéle dans la src_table

SELECT 
    {{ visit_source_value(src_base, src_finess_nb, src_visit_nb) }} AS visit_source_value,
    {{ visit_source_value(src_base, src_finess_nb, src_visit_nb) }} ||'_'|| {{ src_detail_nb }}::text AS visit_detail_source_value,
    {{ src_detail_nb }}::text AS detail_num,
    coalesce({{ nb_jrs }}, 0)        AS jours,
    {{ um_typ }}        AS um_typ
    
FROM {{src_table}} t1
WHERE {{um_typ}} IS NOT NULL



{% endmacro %}