{% macro visit_detail_dates(start_date, date_time) %}

-- Cette macro calcule la date de début ou la date de fin d'une visite détaillée, en fonction de la date de début de la visite, la durée de la visite détaillée (jours) et le délai entre le début de la visite et la fin de la visite détaillée
-- start_date et date_time sont des booléens 


{% if start_date %}

        {% if date_time %}

                ((visit_start_date::date + (delai::integer - jours::integer))||' 00:00:00')::timestamp 

        {% else %}

                (visit_start_date::date + (delai::integer - jours::integer))

        {% endif %}

{% else %}

        {% if date_time %}

                ((visit_start_date::date + delai::integer)||' 00:00:00')::timestamp

        {% else %}


                (visit_start_date::date + delai::integer)::date

        {%endif%}


{% endif %}

{% endmacro %}