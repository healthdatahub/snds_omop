{% macro numero_departement(code_commune) %}


-- Cett macro donne le numéro du département correspondant à un code commune sur 2 caractères
-- Pour les DOM, il s'agit des 2 premiers caractères de code_commune
-- Pour les autres, il s'agit de 0XX où XX sont les 2 premiers caractères du code commune

CASE
    WHEN SUBSTR({{ code_commune}}, 1, 2) = '97'
        THEN SUBSTR({{ code_commune}}, 1, 3)
    ELSE 
        '0' || SUBSTR({{ code_commune}}, 1, 2)
END


{% endmacro %}